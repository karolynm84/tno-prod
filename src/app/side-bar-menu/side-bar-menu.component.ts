import { Component, OnInit } from '@angular/core';
import { StorageService, AuthenticationService } from "../core";
import { Router } from "@angular/router";
import { User } from '../modelos/modelo-user';
import * as AminLTE from 'admin-lte';
//import 'jquery';
import * as $ from 'jquery';
import { TopHeaderComponent } from '../top-header/top-header.component';
//declare var $: any;
import { DataSharingService } from '../shared-services/data-sharing.service';

@Component({
  selector: 'app-side-bar-menu',
  templateUrl: './side-bar-menu.component.html',
  styleUrls: ['./side-bar-menu.component.css'],
  providers: [ StorageService, AuthenticationService ]
})
export class SideBarMenuComponent implements OnInit {
  public user: User;
  public title: string;
  public id: number;
  public topHeader: TopHeaderComponent = null;
  public role: string;
  public hayTurno : boolean = false;
  
  
  constructor(
    private storageService: StorageService,
    private authenticationService: AuthenticationService,
    public router:Router,
    private dataSharingService: DataSharingService
  ) { 
    if ( this.storageService.getCurrentUser()!=null){
      this.user = this.storageService.getCurrentUser();
      this.id = this.storageService.getCurrentUser().id;  
      this.role = this.user.role;
    }
  }

  ngOnInit() {   }

  ngAfterInit() {
    

   /* console.log(this.storageService.getCurrentTurno(this.id.toString()));
    if (this.storageService.getCurrentTurno(this.id.toString())==null) {
      this.hayTurno = false;
      console.log("No hay turno");
    }  
    else {
      if (this.storageService.getCurrentTurno(this.id.toString()).empleado_id.toString()!=this.id.toString()){
        this.hayTurno = false;
        console.log("Este usuario no tiene turno en marcha");
      }
      else{
        this.hayTurno = true;
        console.log("Sí hay turno en marcha");
      }
    }*/

  }

  public logout(): void{
    var oldToken = this.storageService.getCurrentToken();
    this.storageService.logout();
    //this.storageService.removeCurrentSession();
    //this.router.navigate(['/login']);
    this.authenticationService.logout(oldToken).subscribe(
        response => {
          if(response) {
            this.dataSharingService.isUserLoggedIn.next(false);
           console.log("cerrada sesion");
          }
        },
        error=> {
          console.log(error);
        }
    );
  }

  public setTitle( title:string ) {
    this.title = title;
  }

  public editarPerfil() : void {
    console.log("---editar perfil---");
    var id = this.storageService.getCurrentUser().id;
    this.router.navigate([{outlets: {homesesion: ['detalleusuario', id]}}]);
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaSolicitudesPautasComponent } from './lista-solicitudes-pautas.component';

describe('ListaSolicitudesPautasComponent', () => {
  let component: ListaSolicitudesPautasComponent;
  let fixture: ComponentFixture<ListaSolicitudesPautasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaSolicitudesPautasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaSolicitudesPautasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

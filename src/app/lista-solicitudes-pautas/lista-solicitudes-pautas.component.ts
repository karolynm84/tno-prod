import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { StorageService } from '../core';
import { User } from '../modelos/modelo-user';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { SolicitudPauta } from '../modelos/modelo-solicitud-pauta';
import { SolicitudPautasService } from '../http-services/solicitud-pauta.service';

@Component({
  selector: 'app-lista-solicitudes-pautas',
  templateUrl: './lista-solicitudes-pautas.component.html',
  styleUrls: ['./lista-solicitudes-pautas.component.css'],
  providers: [SolicitudPautasService ]
})
export class ListaSolicitudesPautasComponent implements OnInit {
  solicitudes: SolicitudPauta[] = [];
  errMsg: any;
  clientid: number;
  loading = false;
  p: number = 1;
  currenUser: User;
  id_: string;
  role_:string;

   /**
   * SolicitudesListComponent constructor.
   */
  constructor(
    public solicitudPautasService: SolicitudPautasService,
    public toastr: ToastrService,
    public router: Router,
    public storageService: StorageService,
    public sideBar: SideBarMenuComponent
  ) { }

/**
 * Gets list of solicitudes from data base.
 */
  getSolicitudes() {
    console.info("--getSolicitudes()---");
    this.loading = true;
    this.currenUser = this.storageService.getCurrentUser();
    console.log(this.currenUser);
    this.role_ = this.currenUser.role;
    this.id_ = this.currenUser.id.toString();
    this.solicitudPautasService.getSolicitudesPautas(this.storageService.getCurrentToken())
      .subscribe(solicitudes => {  
        this.loading = false;      
        this.solicitudes = solicitudes;

        var id = this.currenUser.id.toString();
        if (this.currenUser.role!='Administrador'){
          var solicitudes = Array.from(solicitudes);
          this.solicitudes = solicitudes.filter(s=> s.user_id == id);
        }
             
      },error=>{
        this.loading = false;
        console.error(error);             
        this.toastr.info('Algo salió mal.');     
    }
    );    
  }

 /**
  * Initialization of component.
  */
  ngOnInit(): void {
    this.sideBar.setTitle('Lista de Solicitudes de Pautas de Grabación');
    this.getSolicitudes();
  }

  setClient(clientid: number) {
    this.clientid = clientid;
  }

  /**
   * Deletes a client from solicitudes list.
   * @param id client id.
   */
  delete(id: string) {
    console.info("---delete()---");
    this.solicitudPautasService.deleteSolicitudPauta(id, this.storageService.getCurrentToken()).subscribe(
      (res) => {
        this.toastr.success('¡Solicitud de Pauta eliminado!');
        this.solicitudes.forEach((t, i) => {
          if (t.id === id) { 
            this.solicitudes.splice(i, 1); 
          }
        });
      }
    );
  }

  public detalleSolicitud(id: string): void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['detallesp', id]}}]);
  }

  public actualizarSolicitud(id: string): void {
    console.log("Este es el ID " + id);
    this.router.navigate(['sesion', {outlets: {homesesion: ['actualizarsp', id]}}]);
  }
}


export interface formRegistroUsuario {
    name: string;
    lastname: string;
    email: string;
    password: string;
    password_confirmation: string;
    birthdate: string;
    instagram : string;
    twitter: string;
    facebook: string;
    enabled: boolean;
    role:string;
}
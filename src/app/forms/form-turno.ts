export interface formTurno {
    hora_entrada: string;
    hora_salida: string;
    fecha: string;
    area_id: number;
    empleado_id: number;
    actividad: string;
}
export interface formSolicitudEntrevista {
    id: string;
    user_id: string;
    programa_id:string;
    nombre_entrevistado:string;
    conoce_tno:boolean;
    ultima_entrevista:Date;
    comentarios:string;
    facebook:string;
    twitter:string;
    instagram:string;
    snapchat:string;
    tema_entrevista:string;
    programa_especifico:boolean;
    status: string;
}
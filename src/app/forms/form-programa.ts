export interface formPrograma {
    id: string;
    pro_nombre: string;
    pro_sinopsis: string;
    pro_tipo: string;
    pro_clasificacion: string;
    pro_email: string;
    pro_hora_inicio: string;
    pro_hora_fin: string;
    pro_fecha_origen: string;
    pro_productor: string;
    pro_dia_semana: string;
    pro_locutor_1: string;
    pro_locutor_2: string;
    pro_locutor_3: string;
    pro_locutor_4: string;
    pro_twitter: string;
    pro_facebook: string;
    pro_instagram : string;
    pro_snapchat : string;
    pro_corte: string;
    habilitado: boolean;
}



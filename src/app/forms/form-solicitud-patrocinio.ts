export interface formSolicitudPatrocinio {
    id:string;
    fecha: string;
    lugar: string;
    nombre_responsable: string;
    nombre_productora: string;
    objetivos_evento: string;
    tipo_evento: string;
    user_id: string;
    status: string;
}
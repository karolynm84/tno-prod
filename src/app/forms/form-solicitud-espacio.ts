export interface formSolicitudEspacios {
    id: number;
    demo: boolean;
    user_id: number;
    demo_link: string;
    corte: string;
    seg_objetivo: string;
    dias: string;
    experiencia_medios: boolean;
    experiencia_medios_online: boolean;
    vinculo_otros_medios: boolean;
    comentarios: string;
    tipo_experiencia: string;
    conductor_1: string;
}
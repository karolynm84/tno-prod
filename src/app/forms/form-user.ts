export interface formUser {
    name: string;
    lastname: string;
    email: string;
    password?: string;
    password_confirmation?: string;
    role: string;
    us_id: string;
    c_locucion: string;
    telephone: string;
    birthdate: string;
    instagram : string;
    twitter: string;
    facebook: string;
    pni: string;
    bio: string;
    enabled: boolean;
}
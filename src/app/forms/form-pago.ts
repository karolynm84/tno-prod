export interface formPago {
    referencia: string;
    id: string;
    programa_id: string;
    status: string;
    hora_transaccion: string;
    fecha_transaccion: string;
    concepto: string;
    banco_origen: string;
    banco_destino: string;
    user_id: string;
    monto: number;
    programa_especifico: boolean;
    efectivo: boolean;
}
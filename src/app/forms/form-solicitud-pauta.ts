export interface formSolicitudPauta {
   id: string;
   fecha: string;
   programa_id: string;
   user_id: string;    
   status: string;
   hora_inicio: string;
   hora_fin: string;
}
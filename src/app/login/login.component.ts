import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { loginForm } from '../forms/form-login';
import { NGXLogger } from 'ngx-logger';
import { Login } from '../modelos/login.model';
import { FormsValidationService } from '../shared-components/forms-validation.service';
import { LoginObject } from '../modelos/modelo-login-object';
import { Sesion } from '../modelos/modelo-sesion';
import { AuthenticationService, StorageService } from '../core';
import { DataSharingService } from '../shared-services/data-sharing.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css', '../app.component.scss'],
  providers: [FormsValidationService, NGXLogger, StorageService, AuthenticationService]
})

export class LoginComponent implements OnInit {    
    loginForm: FormGroup;
    userLogin: Login;
    email: string;     
    sub: any;
    submitted: Boolean = false;
    loading = false;
    error: {
      code: number, message: string
    } = null;   

  constructor (
    public router:Router,
    private authenticationService: AuthenticationService,
    private storageService: StorageService,
    public formBuilder: FormBuilder,
    public validation: FormsValidationService,
    public route: ActivatedRoute,
    public toastr: ToastrService,
    private dataSharingService: DataSharingService ) {
      console.log("--constructor login");
    }

  ngOnInit() {      
    this.loginForm = this.formBuilder.group({
        email: ['', Validators.compose([Validators.required, this.validation.emailValidator, Validators.maxLength(40)])],
        password:  ['', Validators.compose([Validators.required,  Validators.minLength(6),  Validators.maxLength(16)])]
    });  
  }

  /**
   * When user press submit button.
   */
  onSubmit({ value, valid }: { value: loginForm, valid: boolean }) { 
    console.log("---onSubmit()---");
    this.submitted = true;
    this.error = null;
    if(this.loginForm.valid) {
      console.info("--form is valid--");
      this.loading = true;
      this.authenticationService
      .login(new LoginObject(this.loginForm.value))
      .subscribe( 
        data => {
          this.loading = false;
          this.toastr.success("Bienvenido");
          this.correctLogin(data);
        },
        error => {
          this.loading = false;
          console.error("Error");
          console.log(error);
          this.toastr.error("Usuario o clave inválido.");
          this.storageService.removeCurrentSession();
          this.dataSharingService.isUserLoggedIn.next(false);
          this.router.navigate(['/login']);
        }
      )
    }
  }
  
  correctLogin(data: Sesion){
    console.log("---correctLogin---");
    this.storageService.setCurrentSession(data);
    this.dataSharingService.isUserLoggedIn.next(true);
    this.router.navigate(['/sesion']);
  }
}

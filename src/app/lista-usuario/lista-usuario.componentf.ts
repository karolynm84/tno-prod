import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../http-services/user.service';
import { User } from '../modelos/modelo-user';
import { StorageService } from '../core';

@Component({
  selector: 'app-lista-usuario',
  templateUrl: './lista-usuario-table.html',
  styleUrls: ['./lista-usuario.component.css'],
  providers: [UserService, StorageService]
})
export class ListaUsuarioComponentf implements OnInit {
  users: User[] = [];
  errMsg: any;
  clientid: number;
  loading = false;
  p: number = 1;
  role_: string;

  /**
   * ClientsListComponent constructor.
   */
  constructor(
    public servicioUser: UserService,
    public toastr: ToastrService,
    public router: Router,
    private storageService: StorageService
  ) { }

/**
 * Gets list of users from data base.
 */
  getusers() {
    console.info("--getusers()---");
    this.loading = true; 
    this.role_ = this.storageService.getCurrentUser().role;       
    this.servicioUser
    .getUsers(this.storageService.getCurrentToken())
      .subscribe(User => {  
        this.loading = false;      
        this.users = User;
      },error=>{
        this.loading = false;
        console.error(error);             
        this.toastr.info('Algo salió mal.');     
      }
    );    
  }

 /**
  * Initialization of component.
  */
  ngOnInit(): void {
    this.getusers();
  }

  setClient(clientid: number) {
    this.clientid = clientid;
  }

  /**
   * Deletes a client from users list.
   * @param id client id.
   */
  delete(id: number) {
    console.info("---delete()---");
    this.servicioUser.deleteUser(id, this.storageService.getCurrentToken()).subscribe(
      (res) => {
        this.toastr.success('Usuario eliminado!');
        this.users.forEach((t, i) => {
          if (t.id === id) { this.users.splice(i, 1); }
        });
      }
    );
  }
}

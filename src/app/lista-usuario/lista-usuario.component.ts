import { Component, OnInit, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { User } from '../modelos/modelo-user';
import { UserService } from '../http-services/user.service';
import { ToastrService } from 'ngx-toastr';
import { Router, PRIMARY_OUTLET } from '@angular/router';
import { StorageService } from '../core';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
 
@Component({
    selector: 'shipment-list',
    templateUrl: './lista-usuario-table.html',
    styleUrls: ['./lista-usuario.component.css'],
    providers: [UserService, StorageService]
})

export class ListaUsuarioComponent implements OnInit {
  users: User[] = [];
  errMsg: any;
  clientid: number;
  loading = false;
  p: number = 1;
  role_: string;

    constructor(
        private el: ElementRef, // You need this in order to be able to "grab" the table element form the DOM
        public servicioUser: UserService,
        public toastr: ToastrService,
        public router: Router,
        private storageService: StorageService,
        public sideBar: SideBarMenuComponent
    ) { }
 
    public ngOnInit() {
      this.sideBar.setTitle('Lista de Usuarios');
      this.getusers();
    }

    
/**
 * Gets list of users from data base.
 */
  getusers() {
    console.info("--getusers()---");
    this.loading = true; 
    this.role_ = this.storageService.getCurrentUser().role; 
    this.servicioUser
    .getUsers(this.storageService.getCurrentToken())
      .subscribe(User => {  
        this.loading = false;      
        this.users = User;
        

      },error=>{
        this.loading = false;
        console.error(error);             
        this.toastr.info('Algo salió mal.');     
      }
    );    
  }

  setClient(clientid: number) {
    this.clientid = clientid;
  }

  /**
   * Deletes a client from users list.
   * @param id client id.
   */
  delete(id: number) {
    console.info("---delete()---");
    this.servicioUser.deleteUser(id, this.storageService.getCurrentToken()).subscribe(
      (res) => {
        this.toastr.success('Usuario eliminado!');
        this.users.forEach((t, i) => {
          if (t.id === id) { this.users.splice(i, 1); }
        });
      }
    );
  }

  public actualizarUsuario(id: string): void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['actualizarusuario', id]}}]);
  }
  public detalleUsuario(id: string): void {
    console.log("ID " + id);
    this.router.navigate(['sesion', {outlets: {homesesion: ['detalleusuario', id]}}]);
  }
}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleSolicitudTarifasComponent } from './detalle-solicitud-tarifas.component';

describe('DetalleSolicitudTarifasComponent', () => {
  let component: DetalleSolicitudTarifasComponent;
  let fixture: ComponentFixture<DetalleSolicitudTarifasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleSolicitudTarifasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleSolicitudTarifasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

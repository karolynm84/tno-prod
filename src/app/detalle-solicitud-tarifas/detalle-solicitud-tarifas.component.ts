import { Component, OnInit } from '@angular/core';
import { StorageService } from '../core';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { SolicitudTarifasService } from '../http-services/solicitud-tarifas.service';
import { SolicitudTarifa } from '../modelos/modelo-solicitud-tarifa';
import { UserService } from 'app/http-services/user.service';
import { User } from 'app/modelos/modelo-user';

@Component({
  selector: 'app-detalle-solicitud-tarifas',
  templateUrl: './detalle-solicitud-tarifas.component.html',
  styleUrls: ['./detalle-solicitud-tarifas.component.css','../app.component.scss'],
  providers:[SolicitudTarifasService, StorageService, UserService]
})

export class DetalleSolicitudTarifasComponent implements OnInit {
  solicitud: SolicitudTarifa;
  loading = false;
  sub:any;
  role: string;
  user: User = new User();

  constructor(
    private storageService: StorageService,
    public route: ActivatedRoute,
    public router:Router,
    public solicituTarifaService: SolicitudTarifasService,
    private sideBar: SideBarMenuComponent,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.sideBar.setTitle('Detalle de Solicitud de Tarifa');
    this.loading = true;
    this.role = this.storageService.getCurrentUser().role;
    console.log(this.route.url);
    this.sub = this.route.params.subscribe(params => {     
      const solicitud = params['id'];
      this.solicituTarifaService
        .getSolicitudTarifa(solicitud, this.storageService.getCurrentToken())
        .subscribe(c => {
          this.loading = false;
          this.solicitud = c;           
          this.userService.getUser(this.solicitud.user_id, this.storageService.getCurrentToken())
          .subscribe( 
            user =>{
            this.user = user;
            console.log(this.user);
          });
        });
    }, error => {
      this.loading = false;
      console.log(error);
    }
  );
  }

  public volver() : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['solicitudestarifas']}}]);
  }

  public update(id: string) : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['actualizarst', id]}}]);
  }

}



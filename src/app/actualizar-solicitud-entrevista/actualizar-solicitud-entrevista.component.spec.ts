import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarSolicitudEntrevistaComponent } from './actualizar-solicitud-entrevista.component';

describe('ActualizarSolicitudEntrevistaComponent', () => {
  let component: ActualizarSolicitudEntrevistaComponent;
  let fixture: ComponentFixture<ActualizarSolicitudEntrevistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarSolicitudEntrevistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarSolicitudEntrevistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

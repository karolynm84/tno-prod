import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarSolicitudPatrocinioComponent } from './agregar-solicitud-patrocinio.component';

describe('AgregarSolicitudPatrocinioComponent', () => {
  let component: AgregarSolicitudPatrocinioComponent;
  let fixture: ComponentFixture<AgregarSolicitudPatrocinioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarSolicitudPatrocinioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarSolicitudPatrocinioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

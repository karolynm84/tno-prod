import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProgramaService } from '../http-services/programa.service';
import { Programa } from '../modelos/modelo-programa';
import { ToastrService } from 'ngx-toastr';
import { FormsValidationService } from '../shared-components/forms-validation.service';
import { StorageService } from '../core';
import { DataFormatService } from '../shared-services/data-format.service';
import { User } from '../modelos/modelo-user';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { SolicitudPatrocinioService } from '../http-services/solicitud-patrocinio.service';
import { SolicitudPatrocinio } from '../modelos/modelo-solicitud-patrocinio';
import { formSolicitudPatrocinio } from '../forms/form-solicitud-patrocinio';

@Component({
  selector: 'app-agregar-solicitud-patrocinio',
  templateUrl: './agregar-solicitud-patrocinio.component.html',
  styleUrls: ['./agregar-solicitud-patrocinio.component.css', '../app.component.scss'],
  providers:[ FormsValidationService, ProgramaService, 
              StorageService, DataFormatService, SolicitudPatrocinioService ]
})
export class AgregarSolicitudPatrocinioComponent {
  statusList = [
    'pendiente',
    'aprobada',
    'eliminada'
  ]

  form_agregar_solicitud: FormGroup;
  patrocinio_nuevo: SolicitudPatrocinio;
  errMsg: any;
  loading = false;
  currentUser: User;    
  role:string;

  constructor(
        public router:Router,
        public formBuilder: FormBuilder,
        public programaService: ProgramaService,
        public toastr: ToastrService,
        public validation: FormsValidationService,
        public storageService: StorageService,
        public dataFormatService: DataFormatService,
        public sideBar: SideBarMenuComponent,
        private solicitudPatrocinioService: SolicitudPatrocinioService
    ) {
        this.sideBar.setTitle('Solicitud de Patrocinio de Eventos');
        this.currentUser = this.storageService.getCurrentUser();
        this.role = this.currentUser.role;     
        this.form_agregar_solicitud = this.formBuilder.group({
          nombre_responsable: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],
          nombre_productora: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],      
          tipo_evento: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],        
          fecha:['',  Validators.compose([Validators.required])],
          lugar: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],
          objetivos_evento: ['', Validators.compose([Validators.required, Validators.maxLength(500)])],
          user_id:[null,]
        })
    }

   onSubmit({ value,  valid }: { value: formSolicitudPatrocinio, valid: boolean }) { 
       if(!this.form_agregar_solicitud.valid){
            this.toastr.error('Problema en el formulario!', 'Error!');
        }else{ 
            this.patrocinio_nuevo = new SolicitudPatrocinio();
            this.patrocinio_nuevo.fecha = value.fecha;
            this.patrocinio_nuevo.lugar = value.lugar;
            this.patrocinio_nuevo.nombre_responsable = value.nombre_responsable;
            this.patrocinio_nuevo.nombre_productora = value.nombre_productora;
            this.patrocinio_nuevo.tipo_evento = value.tipo_evento;
            this.patrocinio_nuevo.objetivos_evento = value.objetivos_evento;
            this.patrocinio_nuevo.user_id = this.currentUser.id.toString();  
            this.patrocinio_nuevo.status = 'pendiente';  

            console.log(this.patrocinio_nuevo);
            this.loading = true;
            this.solicitudPatrocinioService
            .createSolicitudPatrocinios(this.patrocinio_nuevo, this.storageService.getCurrentToken())
            .subscribe(
                (res) => {                  
                  console.log(res);
                  this.solicitudPatrocinioService
                  .sendEmailSolicitud()
                  .subscribe(
                      (resEmail) => {
                        this.loading = false;
                        console.log(resEmail);
                        this.toastr.success('Solicitud enviada con éxito. Gracias por su tiempo.');
                        if (this.currentUser.role!='Administrador')
                          this.toastr.warning('Se le contactará con la información.');                        
                        this.volver();   
                      },errorEmail=>{
                        this.loading = false;
                        console.log(errorEmail);
                        this.toastr.error('Algo salió mal.'); 
                      }
                  );                              
                },error=>{
                    this.loading = false;     
                    console.log(error);        
                    this.toastr.error('Algo salió mal.');     
                }
            );
        }
    }    

    public volver() : void {
        this.router.navigate(['sesion', {outlets: {homesesion: ['solicitudespatrocinios']}}]);
      }
}



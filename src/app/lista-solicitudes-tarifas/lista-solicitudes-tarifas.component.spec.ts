import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaSolicitudesTarifasComponent } from './lista-solicitudes-tarifas.component';

describe('ListaSolicitudesTarifasComponent', () => {
  let component: ListaSolicitudesTarifasComponent;
  let fixture: ComponentFixture<ListaSolicitudesTarifasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaSolicitudesTarifasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaSolicitudesTarifasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

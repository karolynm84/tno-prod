import { Component, OnInit } from '@angular/core';
import { NgxGalleryOptions, NgxGalleryImageSize, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';

@Component({
    selector:"app-banners",
    templateUrl: './banners.component.html',
    styleUrls: ['./banners.component.css'],
})
export class BannersComponent implements OnInit {
    innerWidth: number;
    galleryOptions: NgxGalleryOptions[];
    galleryImages: NgxGalleryImage[];

    ngOnInit(): void {

       this.innerWidth = window.innerWidth;

       this.galleryOptions = [
        {
            width: '100%',
            height: '235px',
            imageAutoPlay: true,
            imageAutoPlayPauseOnHover: true,
            imageSize: NgxGalleryImageSize.Contain,
            imageAnimation: NgxGalleryAnimation.Fade,
            imageAutoPlayInterval: 4000,
            thumbnails: false,
            preview: false
        },
        {
            breakpoint: 3000,
            height: '280px',
        },
        {
            breakpoint: 1900,
            height: '270px',
        },
        {
            breakpoint: 1600,
            height: '260px',
        },
        {
            breakpoint: 1366,
            height: '200px',
        },
        {
            breakpoint: 1200,
            height: '165px',
        },
        {
            breakpoint: 1100,
            height: '155px',
            width: '100%',
        },
        {
            breakpoint: 1024,
            height: '145px',
            width: '100%',
            imageArrowsAutoHide:true
        },
        {
            breakpoint: 900,
            width: '100%',
            height: '135px',
            imageArrowsAutoHide:true,
        },
        // max-width 
        {
            breakpoint: 830,
            width: '100%',
            height: '125px',
            imageArrowsAutoHide:true,
        },
        {
            breakpoint: 768,
            width: '100%',
            height: '120px',
            imageArrowsAutoHide:true,
        },
        {
            breakpoint: 731,
            width: '731px',
            height: '120px',
            imageArrowsAutoHide:true,
        },
         // max-width 
         {
            breakpoint: 640,
            width: '640px',
            height: '380px',
            imageSize: NgxGalleryImageSize.Cover,
            imageArrowsAutoHide:true,
        },
         {
            breakpoint: 568,
            width: '568px',
            height: '360px',
            imageArrowsAutoHide:true,
        },
        {
            breakpoint: 414,
            width: '414px',
            height: '250px',
            imageArrowsAutoHide:true
        },
        {
            breakpoint: 375,
            width: '375px',
            height: '230px',
            imageArrowsAutoHide:true
        },       
        {
            breakpoint: 360,
            width: '360px',
            height: '220px',
            imageArrowsAutoHide:true
        },
        {
            breakpoint: 320,
            width: '320px',
            height: '200px',
            imageArrowsAutoHide:true
        }
    ];


       if (this.innerWidth <=640) {
            this.galleryImages = [
               /* {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_genteextraordinaria-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_genteextraordinaria-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_genteextraordinaria-small.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_conexionastral-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_conexionastral-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_conexionastral-small.jpg'
                }, */
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_respiralibreprograma-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_respiralibreprograma-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_respiralibreprograma-small.jpg'
                }, 
               /* {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_bebavandenberg-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_bebavandenberg-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_bebavandenberg-small.jpg'
                },*/
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_efectojess-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_efectojess-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_efectojess-small.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_piensaengrande-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_piensaengrande-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_piensaengrande-small.jpg'
                },
                /*{
                    small: 'assets/images/banners/radio_online_venezuela_fondo_angelesdelcacao-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_angelesdelcacao-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_angelesdelcacao-small.jpg'
                },   */               
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_diloconderecho-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_diloconderecho-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_diloconderecho-small.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_yahoraescuando-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_yahoraescuando-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_yahoraescuando-small.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_exportacionesglobales-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_exportacionesglobales-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_exportacionesglobales-small.jpg'
                },   
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_entrevistastonycarrasco-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_entrevistastonycarrasco-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_entrevistastonycarrasco-small.jpg'
                },  
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_franquicias-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_franquicias-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_franquicias-small.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_drmoyacontigo-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_drmoyacontigo-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_drmoyacontigo-small.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_diafragma-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_diafragma-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_diafragma-small.jpg'
                },               
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_magiadelcine-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_magiadelcine-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_magiadelcine-small.jpg'
                },   
              /*  {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_aquelarre-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_aquelarre-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_aquelarre-small.jpg'
                },*/
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_conseguridad-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_conseguridad-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_conseguridad-small.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_vermasalla-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_vermasalla-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_vermasalla-small.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_buenasvibras-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_buenasvibras-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_buenasvibras-small.jpg'
                },
              /*  {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_atodorock-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_atodorock-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_atodorock-small.jpg'
                },*/
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_horadelexito-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_horadelexito-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_horadelexito-small.jpg',
                },
              /*  {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_unraticoyya-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_unraticoyya-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_unraticoyya-small.jpg'
                },*/
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_millennialschic-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_millennialschic-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_millennialschic-small.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_telonvip-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_telonvip-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_telonvip-small.jpg',
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_metaforasanadoras-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_metaforasanadoras-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_metaforasanadoras-small.jpg',
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_cabinaexpresarte-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_cabinaexpresarte-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_cabinaexpresarte-small.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_entretodos-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_entretodos-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_entretodos-small.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_hostseven-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_hostseven-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_hostseven-small.jpg'
                },                            
                /*{
                    small: 'assets/images/banners/radio_online_venezuela_fondo_snobgourmet-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_snobgourmet-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_snobgourmet-small.jpg',
                },*/
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_niideaniyo-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_niideaniyo-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_niideaniyo-small.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_nocheradiante-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_nocheradiante-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_nocheradiante-small.jpg'
                },                 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_dequepuedes-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_dequepuedes-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_dequepuedes-small.jpg'
                },   
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_tumomentofitness-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_tumomentofitness-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_tumomentofitness-small.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_historiasqueinspiran-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_historiasqueinspiran-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_historiasqueinspiran-small.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_suenaaespectaculos-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_suenaaespectaculos-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_suenaaespectaculos-small.jpg'
                }, 
               /* {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_horaperfecta-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_horaperfecta-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_horaperfecta-small.jpg'
                },  */
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_culturaprogresiva-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_culturaprogresiva-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_culturaprogresiva-small.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_driblingyrebote-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_driblingyrebote-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_driblingyrebote-small.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_economiadigital-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_economiadigital-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_economiadigital-small.jpg'
                }           
            ];
       }
        else {

            this.galleryImages = [
                   /* {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_genteextraordinaria-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_genteextraordinaria.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_genteextraordinaria.jpg'
                },
            {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_conexionastral-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_conexionastral.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_conexionastral.jpg'
                }, */
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_respiralibreprograma-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_respiralibreprograma.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_respiralibreprograma.jpg'
                }, 
               /* {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_bebavandenberg-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_bebavandenberg.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_bebavandenberg.jpg'
                }, */
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_efectojess-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_efectojess.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_efectojess.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_piensaengrande-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_piensaengrande.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_piensaengrande.jpg'
                }, 
               /* {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_angelesdelcacao-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_angelesdelcacao.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_angelesdelcacao.jpg'
                },  */                
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_diloconderecho-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_diloconderecho.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_diloconderecho.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_yahoraescuando-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_yahoraescuando.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_yahoraescuando.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_exportacionesglobales-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_exportacionesglobales.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_exportacionesglobales.jpg'
                },  
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_entrevistastonycarrasco-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_entrevistastonycarrasco.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_entrevistastonycarrasco.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_franquicias-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_franquicias.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_franquicias.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_drmoyacontigo-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_drmoyacontigo.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_drmoyacontigo.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_diafragma-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_diafragma.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_diafragma.jpg'
                },
                /*{
                    small: 'assets/images/banners/radio_online_venezuela_fondo_creeentiycrea-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_creeentiycea.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_creeentiycea.jpg'
                }, */
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_magiadelcine-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_magiadelcine.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_magiadelcine.jpg'
                },   
              /*  {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_aquelarre-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_aquelarre.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_aquelarre.jpg'
                }, */
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_conseguridad-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_conseguridad.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_conseguridad.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_vermasalla-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_vermasalla.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_vermasalla.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_buenasvibras-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_buenasvibras.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_buenasvibras.jpg'
                },  
              /*  {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_atodorock-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_atodorock.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_atodorock.jpg'
                },  */
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_horadelexito-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_horadelexito.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_horadelexito.jpg'
                }, 
               /* {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_unraticoyya-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_unraticoyya.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_unraticoyya.jpg'
                },    */   
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_millennialschic-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_millennialschic.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_millennialschic.jpg'
                },           
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_telonvip-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_telonvip.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_telonvip.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_metaforasanadoras-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_metaforasanadoras.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_metaforasanadoras.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_cabinaexpresarte-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_cabinaexpresarte.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_cabinaexpresarte.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_entretodos-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_entretodos.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_entretodos.jpg'
                },                                                            
                /*{
                    small: 'assets/images/banners/radio_online_venezuela_fondo_snobgourmet-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_snobgourmet.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_snobgourmet.jpg'
                },*/
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_hostseven-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_hostseven.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_hostseven.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_niideaniyo-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_niideayo.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_niideayo.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_nocheradiante-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_nocheradiante.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_nocheradiante.jpg'
                },    
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_dequepuedes-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_dequepuedes.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_dequepuedes.jpg'
                },
                /*{
                    small: 'assets/images/banners/radio_online_venezuela_fondo_mujerfeliz.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_mujerfeliz.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_mujerfeliz.jpg'
                },*/
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_tumomentofitness.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_tumomentofitness.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_tumomentofitness.jpg'
                },   
                /*{
                    small: 'assets/images/banners/radio_online_venezuela_fondo_alsondelosbailadores.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_alsondelosbailadores.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_alsondelosbailadores.jpg'
                }, */
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_historiasqueinspiran.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_historiasqueinspiran.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_historiasqueinspiran.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_suenaaespectaculos-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_suenaaespectaculos.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_suenaaespectaculos.jpg'
                },                
                /*{
                    small: 'assets/images/banners/radio_online_venezuela_fondo_horaperfecta-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_horaperfecta.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_horaperfecta.jpg'
                }, */  
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_culturaprogresiva-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_culturaprogresiva.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_culturaprogresiva.jpg'
                },  
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_driblingyrebote-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_driblingyrebote.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_driblingyrebote.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_economiadigital-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_economiadigital.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_economiadigital.jpg'
                },              
            ];
       }
    }
}
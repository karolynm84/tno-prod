import {NgModule, Optional, SkipSelf} from '@angular/core';
import {MockBackend} from "@angular/http/testing";
import {BaseRequestOptions} from "@angular/http";
import { StorageService } from '.';
import { AuthorizatedGuard } from '../seguridad/guardian';
import { AuthenticationService } from '.';

@NgModule({
  declarations: [  ],
  imports: [],
  providers: [
    StorageService,
    AuthorizatedGuard,
    MockBackend,
    BaseRequestOptions,
    AuthenticationService,
    StorageService
  ],
  bootstrap: []
})
export class CoreModule {
  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}
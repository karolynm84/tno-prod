import { Injectable } from '@angular/core';

@Injectable( )
export class SharedDefinitions {

  constructor( ) { }

  bancosList = [
    'Banco Activo',
    'Banesco',
    'Banco Venezolano de Crédito',
    'Banco Mercantil'
  ]

  public getBancosList(): string[] {
      return this.bancosList;
  }
}
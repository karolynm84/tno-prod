import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaSolicitudesEntrevistaComponent } from './lista-solicitudes-entrevista.component';

describe('ListaSolicitudesEntrevistaComponent', () => {
  let component: ListaSolicitudesEntrevistaComponent;
  let fixture: ComponentFixture<ListaSolicitudesEntrevistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaSolicitudesEntrevistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaSolicitudesEntrevistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

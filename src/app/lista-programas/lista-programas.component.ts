import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ProgramaService } from '../http-services/programa.service';
import { Programa } from '../modelos/modelo-programa';
import { StorageService } from '../core';
import { User } from '../modelos/modelo-user';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';

@Component({
  selector: 'app-lista-programas',
  templateUrl: './lista-programas.component.html',
  styleUrls: ['./lista-programas.component.scss'],
  providers: [ProgramaService, SideBarMenuComponent ]
})

/**
 * Programas list component controller.
 */
export class ListaProgramasComponent implements OnInit {
  programas: Programa[] = [];
  programasAutorizados: Programa[] = [];
  autorizado:boolean;
  errMsg: any;
  clientid: number;
  loading = false;
  locutores = '';
  p: number = 1;
  currenUser: User;
  id_: string;
  role_:string;

  /**
   * ClientsListComponent constructor.
   */
  constructor(
    public servicioPrograma: ProgramaService,
    public toastr: ToastrService,
    public router: Router,
    public storageService: StorageService,
    public sideBar: SideBarMenuComponent
  ) { }

/**
 * Gets list of programas from data base.
 */
  getProgramas() {
    console.info("--getProgramas()---");
    this.loading = true;
    this.currenUser = this.storageService.getCurrentUser();
    //console.log(this.currenUser);
    this.role_ = this.currenUser.role;
    this.id_ = this.currenUser.id.toString();
    this.servicioPrograma.getProgramas()
      .subscribe(programas => {  
        this.loading = false;      
        this.programas = programas;
        /** Obtiene la lista de programas que el usuario está
         *  autorizado para modificar en caso de que no sea administrador.
         */
        this.servicioPrograma
        .getProgramasAutorizados(this.id_, this.storageService.getCurrentToken())
        .subscribe(programasAutorizados => {
          if (programasAutorizados != null) {
            var itemsArray = Array.from(programasAutorizados);
            this.programasAutorizados = programasAutorizados;          
            for (let i=0; i<this.programas.length; i++) {
              this.autorizado = itemsArray.includes(this.programas[i]);
              this.programas[i].autorizado = this.autorizado;
            }           
          }
        },
        error=> {
          console.log(error);
          console.log("Error en programas autorizados");
        });       
      },error=>{
        this.loading = false;
        console.error(error);             
        this.toastr.info('Algo salió mal.');     
    }
    );    
  }

 /**
  * Initialization of component.
  */
  ngOnInit(): void {
    this.sideBar.setTitle('Lista de Programas');
    this.getProgramas();
  }

  setClient(clientid: number) {
    this.clientid = clientid;
  }

  /**
   * Deletes a client from programas list.
   * @param id client id.
   */
  delete(id: string) {
    console.info("---delete()---");
    this.servicioPrograma.deletePrograma(id, this.storageService.getCurrentToken()).subscribe(
      (res) => {
        this.toastr.success('Programa eliminado!');
        this.programas.forEach((t, i) => {
          if (t.id === id) { 
            this.programas.splice(i, 1); 
          }
        });
      }
    );
  }

  public actualizarPrograma(id: string): void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['actualizarprograma', id]}}]);
  }
  public detallePrograma(id: string): void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['detalleprograma', id]}}]);
  }
}

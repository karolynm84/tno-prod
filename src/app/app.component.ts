import { Component } from '@angular/core';
import { AuthenticationService, ProgramaService } from './core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ AuthenticationService, ProgramaService]
})
export class AppComponent {
  title = 'TNO Radio';

  constructor () { }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarNotificacionComponent } from './agregar-notificacion.component';

describe('AgregarNotificacionComponent', () => {
  let component: AgregarNotificacionComponent;
  let fixture: ComponentFixture<AgregarNotificacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarNotificacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarNotificacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Pago } from '../modelos/modelo-pago';
import { StorageService } from '../core';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { PagoService } from '../http-services/pago.service';

@Component({
  selector: 'app-detalle-pago',
  templateUrl: './detalle-pago.component.html',
  styleUrls: ['./detalle-pago.component.css','../app.component.scss'],
  providers:[PagoService, StorageService]
})

export class DetallePagoComponent implements OnInit {
  pago: Pago;
  loading = false;
  private sub: any;
  role: string;

  constructor(
    private storageService: StorageService,
    public route: ActivatedRoute,
    public router:Router,
    public pagoService: PagoService,
    private sideBar: SideBarMenuComponent
  ) { }

  ngOnInit() {
    this.sideBar.setTitle('Detalle de Pago');
    this.loading = true;
    this.role = this.storageService.getCurrentUser().role;
    //console.log(this.route.url);
    this.sub = this.route.params.subscribe(params => {     
      const pago = params['id'];
      this.pagoService
        .getPago(pago, this.storageService.getCurrentToken())
        .subscribe(c => {
          console.log(c);
          this.loading = false;
          this.pago = c;
        });
    }, error => {
      this.loading = false;
      console.log(error);
    }
  );
  }

  public volver() : void {
    this.router.navigate([['sesion', {outlets: {homesesion: ['pagos']}}]]);
  }

  public update(id: string) : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['actualizarpago', id]}}]);
  }

}


import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';

import { NgModule } from '@angular/core';

import * as $ from "jquery";

import { AlertModule } from 'ngx-bootstrap';

import { SidebarModule } from 'ng-sidebar';

import { CollapseModule } from 'ngx-bootstrap/collapse';

import { NgxGalleryModule } from 'ngx-gallery';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';

import { HttpClientModule } from '@angular/common/http';

import { NgxPaginationModule } from 'ngx-pagination'; 

//import { environment } from '../environments/environment';

import { LocationStrategy, PathLocationStrategy, HashLocationStrategy } from '@angular/common';

import { NgxPageScrollModule} from 'ngx-page-scroll';


//import { NgTableComponent, NgTableFilteringDirective, NgTablePagingDirective, NgTableSortingDirective } from 'ng2-table/ng2-table';

import 'hammerjs';
 
import { AppComponent } from './app.component';
import { TopHeaderComponent } from './top-header/top-header.component';
import { PubliHeaderComponent } from './publi-header/publi-header.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { PlayerComponent } from './player/player.component';
import { PlayerSideComponent } from './player-side/player-side.component';
import { PageContainerComponent } from './page-container/page-container.component';
import { MenuOpenedService } from './menu-opened.service';
import { RedesComponent } from './redes/redes.component';
import { BannersComponent } from './banners/banners.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { AgregarProgramaComponent } from './agregar-programa/agregar-programa.component';
import { MapToKeysPipe } from './shared-components/map-to-keys-pipe';
import { ListaProgramasComponent } from './lista-programas/lista-programas.component';
import { ListaUsuarioComponent } from './lista-usuario/lista-usuario.component';
import { DetalleUsuarioComponent } from './detalle-usuario/detalle-usuario.component';
import { ActualizarUsuarioComponent } from './actualizar-usuario/actualizar-usuario.component';
import { homesesionComponent } from './home-sesion/home-sesion.component';
import { StorageService, ProgramaService } from './core';
import { ResgistroUsuarioComponent } from './agregar-usuario/resgistro-usuario.component';
import { DataSharingService } from './shared-services/data-sharing.service';
import { LoadingModule } from 'ngx-loading';
import { BottomComponent } from './bottom/bottom.component';
import { DetalleProgramaComponent } from './detalle-programa/detalle-programa.component';
import { ActualizarProgramaComponent } from './actualizar-programa/actualizar-programa.component';
import { SideBarMenuComponent } from './side-bar-menu/side-bar-menu.component';
import { ListaSolicitudesEntrevistaComponent } from './lista-solicitudes-entrevista/lista-solicitudes-entrevista.component';
import { DetalleSolicitudEntrevistaComponent } from './detalle-solicitud-entrevista/detalle-solicitud-entrevista.component';
import { ActualizarSolicitudEntrevistaComponent } from './actualizar-solicitud-entrevista/actualizar-solicitud-entrevista.component';
import { ActualizarSolicitudPautaComponent } from './actualizar-solicitud-pauta/actualizar-solicitud-pauta.component';
import { ActualizarSolicitudPatrocinioComponent } from './actualizar-solicitud-patrocinio/actualizar-solicitud-patrocinio.component';
import { ActualizarPagoComponent } from './actualizar-pago/actualizar-pago.component';
import { ActualizarTurnoComponent } from './actualizar-turno/actualizar-turno.component';
import { ActualizarSolicitudTarifasComponent } from './actualizar-solicitud-tarifas/actualizar-solicitud-tarifas.component';
import { DetalleSolicitudPatrocinioComponent } from './detalle-solicitud-patrocinio/detalle-solicitud-patrocinio.component';
import { DetalleSolicitudTarifasComponent } from './detalle-solicitud-tarifas/detalle-solicitud-tarifas.component';
import { DetalleTurnoComponent } from './detalle-turno/detalle-turno.component';
import { DetallePagoComponent } from './detalle-pago/detalle-pago.component';
import { AgregarSolicitudPautaComponent } from './agregar-solicitud-pauta/agregar-solicitud-pauta.component';
import { AgregarSolicitudPatrocinioComponent } from './agregar-solicitud-patrocinio/agregar-solicitud-patrocinio.component';
import { AgregarPagoComponent } from './agregar-pago/agregar-pago.component';
import { AgregarTurnoComponent } from './agregar-turno/agregar-turno.component';
import { AgregarSolicitudEntrevistaComponent } from './agregar-solicitud-entrevista/agregar-solicitud-entrevista.component';
import { AgregarNotificacionComponent } from './agregar-notificacion/agregar-notificacion.component';
import { ListaSolicitudesPatrocinioComponent } from './lista-solicitudes-patrocinio/lista-solicitudes-patrocinio.component';
import { ListaPagosComponent } from './lista-pagos/lista-pagos.component';
import { ListaSolicitudesPautasComponent } from './lista-solicitudes-pautas/lista-solicitudes-pautas.component';
import { ListaNotificacionesComponent } from './lista-notificaciones/lista-notificaciones.component';
import { ListaSolicitudesTarifasComponent } from './lista-solicitudes-tarifas/lista-solicitudes-tarifas.component';
import { ListaTurnosComponent } from './lista-turnos/lista-turnos.component';
import { AgregarSolicitudTarifasComponent } from './agregar-solicitud-tarifas/agregar-solicitud-tarifas.component';
import { DetalleSolicitudPautaComponent } from './detalle-solicitud-pauta/detalle-solicitud-pauta.component';
import { ListaUsuarioComponentf } from './lista-usuario/lista-usuario.componentf';
import { AgregarSalidaComponent } from './agregar-salida/agregar-salida.component';
import { AgregarSolicitudEspacioComponent } from './agregar-solicitud-espacio/agregar-solicitud-espacio.component';
import { ActualizarSolicitudEspacioComponent } from './actualizar-solicitud-espacio/actualizar-solicitud-espacio.component';
import { ListaSolicitudesEspaciosComponent } from './lista-solicitudes-espacios/lista-solicitudes-espacios.component';
import { DetalleSolicitudEspacioComponent } from './detalle-solicitud-espacio/detalle-solicitud-espacio.component';
import { ProgramacionComponent, TimeFormat } from './programacion/programacion.component';
import { HomeSiteComponent } from './home-site/home-site.component';
import { ProgramacionWeekComponent } from './programacion-week/programacion-week.component';
import { SidePublicidadComponent } from './side-publicidad/side-publicidad.component';

@NgModule({
  declarations: [
    AppComponent,
    TopHeaderComponent,
    PubliHeaderComponent,
    SideBarComponent,
    PlayerComponent,
    PlayerSideComponent,
    PageContainerComponent,
    RedesComponent,
    BannersComponent,
    FooterComponent,
    LoginComponent,
    ProgramacionComponent,
    AgregarProgramaComponent,
    MapToKeysPipe,
    ListaProgramasComponent,
    ResgistroUsuarioComponent,
    ListaUsuarioComponent,
    DetalleUsuarioComponent,
    ActualizarUsuarioComponent,
    homesesionComponent,
    BottomComponent,
    DetalleProgramaComponent,
    ActualizarProgramaComponent,
    SideBarMenuComponent,
    ListaSolicitudesEntrevistaComponent,
    DetalleSolicitudEntrevistaComponent,
    ActualizarSolicitudEntrevistaComponent,
    ActualizarSolicitudPautaComponent,
    ActualizarSolicitudPatrocinioComponent,
    ActualizarPagoComponent,
    ActualizarTurnoComponent,
    ActualizarSolicitudTarifasComponent,
    DetalleSolicitudPatrocinioComponent,
    DetalleSolicitudTarifasComponent,
    DetalleTurnoComponent,
    DetallePagoComponent,
    AgregarSolicitudPautaComponent,
    AgregarSolicitudPatrocinioComponent,
    AgregarPagoComponent,
    AgregarTurnoComponent,
    AgregarSolicitudEntrevistaComponent,
    AgregarNotificacionComponent,
    ListaSolicitudesPatrocinioComponent,
    ListaPagosComponent,
    ListaSolicitudesPautasComponent,
    ListaNotificacionesComponent,
    ListaSolicitudesTarifasComponent,
    ListaTurnosComponent,
    AgregarSolicitudTarifasComponent,
    DetalleSolicitudPautaComponent,
    ListaUsuarioComponentf,
    AgregarSalidaComponent,
    AgregarSolicitudEspacioComponent,
    ActualizarSolicitudEspacioComponent,
    ListaSolicitudesEspaciosComponent,
    DetalleSolicitudEspacioComponent,
    ProgramacionWeekComponent,
    HomeSiteComponent,
    TimeFormat,
    SidePublicidadComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgxGalleryModule,
    AlertModule.forRoot(),
    SidebarModule.forRoot(),
    CollapseModule.forRoot(),
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    }),
    LoggerModule.forRoot({serverLoggingUrl: '/api/logs', level: NgxLoggerLevel.DEBUG, serverLogLevel: NgxLoggerLevel.ERROR}),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    LoadingModule,
    NgxPageScrollModule,
    NgxPaginationModule

  ],
  providers: [MenuOpenedService, 
              { provide: LocationStrategy, useClass: PathLocationStrategy }, 
              StorageService,
              DataSharingService,
              ProgramaService 
            ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
 }
}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '../../../node_modules/@angular/forms';
import { Router, ActivatedRoute } from '../../../node_modules/@angular/router';
import { ToastrService } from '../../../node_modules/ngx-toastr';
import { FormsValidationService } from '../shared-components/forms-validation.service';
import { DataFormatService } from '../shared-services/data-format.service';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { User } from '../modelos/modelo-user';
import { Pago } from '../modelos/modelo-pago';
import { PagoService } from '../http-services/pago.service';
import { formPago } from '../forms/form-pago';
import { Programa } from '../modelos/modelo-programa';
import { StorageService, ProgramaService } from '../core';

@Component({
  selector: 'app-actualizar-pago',
  templateUrl: './actualizar-pago.component.html',
  styleUrls: ['./actualizar-pago.component.css', '../app.component.scss'],
  providers: [ FormsValidationService,  PagoService,
    StorageService, DataFormatService, ProgramaService ]
})
export class ActualizarPagoComponent implements OnInit {

  statusList = [
    'Pendiente',
    'Conciliado',
    'Eliminado',
    'Devuelto'
  ]

  form_agregar_pago: FormGroup;
  form_locutores: FormGroup
  pago_nuevo: Pago;
  errMsg: any;
  isChecked: boolean = false;
  loading = false;
  formPago: formPago;
  sub: any;
  currentUser: User;
  programa_especifico_: boolean = false;
  programas: Programa[] = [];
  role:string;
  status_:string;
  
  constructor (
    public router: Router,
        public formBuilder: FormBuilder,
        public pagoService: PagoService,
        public toastr: ToastrService,
        public validation: FormsValidationService,
        public storageService: StorageService,
        public route: ActivatedRoute,
        public dataFromatService: DataFormatService,
        public sideBar: SideBarMenuComponent,
        public programaService: ProgramaService
      ) {}

  ngOnInit() {
      this.sideBar.setTitle('Actualizar Pago');
      this.loadProgramas();     
      this.currentUser = this.storageService.getCurrentUser();
      this.role = this.currentUser.role;
      this.loading = true;   
      this.form_agregar_pago = this.formBuilder.group({
        id: [null],
        referencia: ['', Validators.compose([Validators.required, Validators.maxLength(20), this.validation.notNullValidator])],
        user_id: [null],
        programa_id:[null],
        fecha_transaccion: ['',  Validators.compose([Validators.required])],
        hora_transaccion: ['', Validators.compose([Validators.required, this.validation.notNullValidator])],
        concepto: ['', Validators.compose([Validators.maxLength(200), Validators.required, this.validation.notNullValidator])],
        banco_origen:['', Validators.compose([Validators.maxLength(60), Validators.required, this.validation.notNullValidator])],
        status:[''],
        banco_destino:['', Validators.compose([Validators.maxLength(60), Validators.required, this.validation.notNullValidator])],
        programa_especifico: [false],
        monto: ['', Validators.compose([Validators.maxLength(20), Validators.required, this.validation.notNullValidator])]
    });
    this.loading = true;
    this.sub = this.route.params.subscribe(params => {     
      const pago = params['id'];
      this.pagoService
        .getPago(pago, this.storageService.getCurrentToken())
        .subscribe(c => {
          this.loading = false;
          this.pago_nuevo = c;
          console.log(this.pago_nuevo);

          let form = this.formPago;
          this.status_ = this.pago_nuevo.status;

          if (this.pago_nuevo.programa_id != null) {
            this.programa_especifico_ = true;
          }            
          else {
            this.programa_especifico_ = false;
          }
          form = {
            "id" : this.pago_nuevo.id,
            "concepto" : this.pago_nuevo.concepto,
            "referencia" : this.pago_nuevo.referencia,
            "programa_id" : this.pago_nuevo.programa_id,
            "fecha_transaccion" : this.pago_nuevo.fecha_transaccion,
            "hora_transaccion" : this.pago_nuevo.hora_transaccion,
            "banco_origen" : this.pago_nuevo.banco_origen,
            "banco_destino" : this.pago_nuevo.banco_destino,
            "user_id" : this.currentUser.id.toString(),
            "status" : this.pago_nuevo.status,
            "monto" : this.pago_nuevo.monto,            
            "programa_especifico" : this.programa_especifico_,
            "efectivo" : this.pago_nuevo.efectivo
          }         
         this.form_agregar_pago.setValue(form);   
        });
    }, error => {
      this.loading = false;
      console.log(error);
    }
  );
  }

  /**
   * When user press submit button.
   */
  onSubmit({ value, valid }: { value: formPago, valid: boolean }) { 
    if(!this.form_agregar_pago.valid){
        this.toastr.error('Debe llenar todos los campos obligatorios!', 'Error!');
    }else {
      this.loading = true;            
      this.pagoService
      .updatePago(value, this.storageService.getCurrentToken())
      .subscribe(
          (res) => {
            this.loading = false;  
            console.log(res);    
            this.toastr.success('Pago actualizado con éxito!');
            this.volver();                               
          },error=>{
            this.loading = false;           
            console.log(error);             
            this.toastr.error('Algo salió mal. Comunicarse con soporte.'); 
          }
       );          
    } 
  }

  public volver() : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['pagos']}}]);
  }

  /**
  * Loads programas into a SelectList.
  */
  public loadProgramas() {
  console.info("---loadProgramas()---");
  this.loading = true;
  this.programaService.getProgramas().subscribe
  (programas => { 
    this.loading = false;
      this.programas = programas;
  });
  } 

  /**
   * Changes.
   */
  public changeProgramaEspecifico() {
      console.info("---changeProgramaEspecífico()---");
      if (this.programa_especifico_==true) {
          this.programa_especifico_=false;
          this.form_agregar_pago.get('programa_id').disable();
      }
      else {
          this.programa_especifico_=true;
          this.loadProgramas();
          this.form_agregar_pago.get('programa_id').enable();
      }
  }  
}
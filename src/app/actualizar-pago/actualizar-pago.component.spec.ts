import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarPagoComponent } from './actualizar-pago.component';

describe('ActualizarPagoComponent', () => {
  let component: ActualizarPagoComponent;
  let fixture: ComponentFixture<ActualizarPagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarPagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

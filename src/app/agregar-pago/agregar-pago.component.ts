import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { formPago } from '../forms/form-pago';
import { PagoService } from '../http-services/pago.service';
import { Pago } from '../modelos/modelo-pago';
import { ToastrService } from 'ngx-toastr';
import { FormsValidationService } from '../shared-components/forms-validation.service';
import { StorageService, ProgramaService } from '../core';
import { DataFormatService } from '../shared-services/data-format.service';
import { User } from '../modelos/modelo-user';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { Programa } from '../modelos/modelo-programa';
import { SharedDefinitions } from '../shared-data/shared-definitions';

@Component({
  selector: 'app-agregar-pago',
  templateUrl: './agregar-pago.component.html',
  styleUrls: ['./agregar-pago.component.css', '../app.component.scss'],
  providers:[ FormsValidationService, PagoService, SharedDefinitions,
              StorageService, DataFormatService, ProgramaService]
})

export class AgregarPagoComponent {       
    form_agregar_pago: FormGroup;
    pago_nuevo: Pago;
    errMsg: any;
    loading = false;
    currentUser: User;    
    programa_especifico_: boolean = false;
    efectivo: boolean = false;
    programas: Programa[] = [];
   

  constructor(
        public router:Router,
        public formBuilder: FormBuilder,
        public pagoService: PagoService,
        public toastr: ToastrService,
        public validation: FormsValidationService,
        public storageService: StorageService,
        public dataFormatService: DataFormatService,
        public sideBar: SideBarMenuComponent,
        public programaService: ProgramaService,
        public sharedDef: SharedDefinitions
    ) {
        console.log(this.sharedDef.bancosList);
        this.sideBar.setTitle('Registro de Pago');
        this.loadProgramas();
        this.currentUser = this.storageService.getCurrentUser();         
        this.form_agregar_pago = this.formBuilder.group({
            referencia: ['', Validators.compose([Validators.required, Validators.maxLength(20), this.validation.notNullValidator])],
            user_id: [null],
            programa_id:[null],
            fecha_transaccion: ['', Validators.compose([Validators.required, this.validation.notNullValidator])],
            hora_transaccion: ['', Validators.compose([Validators.required, this.validation.notNullValidator])],
            concepto: ['', Validators.compose([Validators.maxLength(200), Validators.required, this.validation.notNullValidator])],
            banco_origen:['', Validators.compose([Validators.maxLength(60)])],
            banco_destino:['', Validators.compose([Validators.maxLength(60)])],
            programa_especifico: [false],
            efectivo: [false],
            monto: ['', Validators.compose([Validators.maxLength(20), Validators.required, this.validation.notNullValidator])]
        }
    );
        this.form_agregar_pago.get('programa_id').disable();
    }

   onSubmit({ value,  valid }: { value: formPago, valid: boolean }) { 
       if(!this.form_agregar_pago.valid){
            this.toastr.error('Problema en el formulario!', 'Error!');
        }else{ 
            console.log(value.efectivo);
            this.pago_nuevo = new Pago();
            this.pago_nuevo.user_id = this.currentUser.id.toString();
            this.pago_nuevo.referencia = value.referencia;
            this.pago_nuevo.fecha_transaccion = value.fecha_transaccion;
            this.pago_nuevo.hora_transaccion = value.hora_transaccion;
            this.pago_nuevo.concepto = value.concepto;
            this.pago_nuevo.banco_origen = value.banco_origen;
            this.pago_nuevo.banco_destino = value.banco_destino;
            this.pago_nuevo.programa_id = value.programa_id;
            this.pago_nuevo.efectivo = this.efectivo;
            this.pago_nuevo.monto = value.monto;
            this.pago_nuevo.status = 'Pendiente';

            console.log(this.pago_nuevo);
            this.loading = true;
            this.pagoService
            .createPago(this.pago_nuevo, this.storageService.getCurrentToken())
            .subscribe(
                (res) => {                  
                  console.log(res);
                  this.toastr.success('Pago registrado con éxito. Gracias por su tiempo.');
              this.pagoService
              .sendEmailPago()
                  .subscribe(
                      (resEmail) => {
                        this.loading = false;
                        console.log(resEmail);
                        this.toastr.success('Aviso de pago enviado con éxito. Gracias por su tiempo.');
                        if (this.currentUser.role!='Administrador')
                          this.toastr.warning('Enviado para revisión y será acreditado luego de su aprobación.');                        
                          this.volver();   
                      },errorEmail=>{
                        this.loading = false;
                        console.log(errorEmail);
                        this.toastr.error('Algo salió mal con el aviso.'); 
                        this.volver();
                      }
                  );                              
                },error=>{
                    this.loading = false;     
                    console.log(error);        
                    this.toastr.error('Algo salió mal.');     
                }
            );
        }
    }    


    public volver() : void {
        this.router.navigate(['sesion', {outlets: {homesesion: ['pagos']}}]);
      }

  /**
   * Loads programas into a SelectList.
   */
  public loadProgramas() {
    console.info("---loadProgramas()---");
    this.loading = true;
    this.programaService.getProgramas().subscribe
    (programas => { 
      this.loading = false;
        this.programas = programas;
    });
  } 

    /**
     * Changes if the payment is due to a production.
     */
    public changeProgramaEspecifico() {
        console.info("---changeProgramaEspecífico()---");
        if (this.programa_especifico_==true) {
            this.programa_especifico_=false;
            this.form_agregar_pago.get('programa_id').disable();
        }
        else {
            this.programa_especifico_=true;            
            this.form_agregar_pago.get('programa_id').enable();
        }
    }  

    /**
     * Cahnges if it was cash or transfer.
     */
    public changeEfectivo() {
        console.info("---changeProgramaEspecífico()---");
        if (this.efectivo==true) {
            this.efectivo=false;
            this.form_agregar_pago.get('banco_origen').enable();
            this.form_agregar_pago.get('banco_destino').enable();
        }
        else {
            this.efectivo=true;     
            this.form_agregar_pago.get('banco_origen').disable();
            this.form_agregar_pago.get('banco_destino').disable();     
        }
    }  

}

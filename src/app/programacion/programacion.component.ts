import { Component, OnInit } from '@angular/core';
import { StorageService } from 'app/core';
import { Router } from '@angular/router';
import { Programa } from 'app/modelos/modelo-programa';

@Component({
  selector: 'app-programacion',
  templateUrl: './programacion.component.html',
  styleUrls: ['./programacion.component.css'],
  
})
export class ProgramacionComponent implements OnInit {
  programas: Programa[] = [];
  programasHabilitados: Programa[] = [];
  errMsg: any;
  loaded = false;
  locutores = '';
  timeformat: TimeFormat = new TimeFormat();
  loading = false;

  /**
   * ProgramaciónComponent constructor.
   */
  constructor(    
    public router: Router,
    public storageService: StorageService,
    private dataSharingService: DataSharingService
  ) { }

  ngOnInit() {
    console.log("--ProgramacionComponent Init---");
    this.loading = true;
    this.dataSharingService.getProgramas().subscribe(
      programas => {
      this.loading = false;
      this.programas = programas;
      this.loaded = true;
      this.programas.sort((a,b) => a.pro_hora_inicio.localeCompare(b.pro_hora_inicio));
      this.programas.forEach(s => {
        s.pro_hora_inicio = this.timeformat.transform(s.pro_hora_inicio);
      },
      error => {
        this.loading=false;
        console.log(error);
      }
      );

    this.programasHabilitados = this.programas.filter(programa=>{
      console.log(programa.habilitado);
      if (programa.habilitado==true)
      {
        //console.log(programa.habilitado);
        return programa;
      }
    });
    this.dataSharingService.setData(this.programasHabilitados); 
  });
  }
}

import { Pipe, PipeTransform } from '@angular/core';
import { DataSharingService } from 'app/shared-services/data-sharing.service';
@Pipe({name: 'convertFrom24To12Format'})
export class TimeFormat implements PipeTransform {
    transform(time: any): any {
        let hour = (time.split(':'))[0]
        let min = (time.split(':'))[1]
        let part = hour > 12 ? 'pm' : 'am';
        min = (min+'').length == 1 ? `0${min}` : min;
        hour = hour > 12 ? hour - 12 : hour;
        hour = (hour+'').length == 1 ? `0${hour}` : hour;
        return `${hour}:${min} ${part}`
      }
  }

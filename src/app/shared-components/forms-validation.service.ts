import { Injectable } from '@angular/core';
import { FormControl, ValidatorFn, FormArray, Form, AbstractControl } from '@angular/forms';

@Injectable( )
export class FormsValidationService {

  constructor( ) { }

  emailValidator(control: FormControl): {[key:string]: any} {
        const emailRegexp =/^[a-z0-9_\+-]+(\.[a-z0-9_\+-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*\.([a-z]{2,4})$/;
        if (control.value && !emailRegexp.test(control.value)) {
            return { invalidEmail: true };
        }
    }   

    cellphoneValidator(control: FormControl): {[key:string]: any} {
        const cellphone = /^\+\d{1,4} \d{1,3}[-]\d{4,12}$/;
        if (control.value && !cellphone.test(control.value)) {
        return { invalidcellphone: true };
        }
    }

    notNullValidator(control: FormControl): { [key: string]: any } {
        const checkIt = control.value;
        if (checkIt.toString().trim() == '') {
            return { invalid: true };
        }
    }

     /**
     * Se debe seleccionar banco origen y destino si
     * el pago no es realizado en efectivo.
     */
    bancoSelected(control: FormControl, efectivo:boolean): { [key: string]: any } {
        const checkIt = control.value;
        if (checkIt.toString().trim() == '' && efectivo==false) {
            return { invalid: true };
        }
    }

    justLettersValidator(control: FormControl): {[key:string]: any} {
        const justLettersRegexp = /^[a-zA-Z\s]*$/;
        if (control.value && !justLettersRegexp.test(control.value)) {
            return { invalidString: true };
        }
    }

    justNumbersValidator(control: FormControl): {[key:string]: any} {
        const justLettersRegexp = /^[0-9\s]*$/;
        if (control.value && !justLettersRegexp.test(control.value)) {
            return { invalidString: true };
        }
    }

    tooManyWordsValidator(control: FormControl): {[key:string]: any} {
        if(control.value.split(" ").length > 3) {
            return { tooManyWords: true };
        }
    }
   
    dateLessThan(dateField1: string, dateField2: string, validatorField: { [key: string]: boolean }): ValidatorFn {
        console.log("entro");
        return (c: AbstractControl): { [key: string]: boolean } | null => {
            const date1 = c.get(dateField1).value;
            const date2 = c.get(dateField2).value;
            if ((date1 !== null && date2 !== null) && date1 > date2) {
                return validatorField;
            }
            return null;
        };
    }


    isValidDate(control: FormControl): {[key:string]: any} {
        // First check for the pattern
        var date = new Date().getTime();
        var birth_date = new Date (control.value).getTime();
        var dateYear = new Date().getFullYear();

        //const regex_date = /^\d{4}\-\d{1,2}\-\d{1,2}$/;
        
        const regex_date = /^\d{1,2}\-\d{1,2}\-\d{4}$/;
        
        if(control.value && !regex_date.test(control.value))
        {
            console.log("Invalid date");
            return { invalidDateFormat: true};
        }

        // Parse the date parts to integers
        var parts   = control.value.split("-");
        var year     = parseInt(parts[2], 10);
        var month   = parseInt(parts[1], 10);
        var day    = parseInt(parts[0], 10);

        // Check the ranges of month and year
        if(control.value && (year < 1000 || year > 3000 || month == 0 || month > 12))
        {
            console.log("Invalid date");
            return { invalidDateYearRange: true};
        }

        if (birth_date > date) {
            console.log("Invalid date");
            return { invalidFutureDate: true};
        }
        
        if (dateYear - year < 18) {
            console.log("Invalid date");
            return { invalidUnderAge: true};
        }

        var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

        // Adjust for leap years
        if(control.value && (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)))
        {
            monthLength[1] = 29;
        }

        // Check the range of the day 
        if (control.value && (day < 0 || day >= monthLength[month - 1])) {
            console.log("Invalid date");
            return { invalidDateDayRange: true};
        }
    }

    isValidBirthDate(control: FormControl): {[key:string]: any} {
        // First check for the pattern
        var date = new Date().getTime();
        var birth_date = new Date (control.value).getTime();
        var dateYear = new Date().getFullYear();

        const regex_date = /^\d{4}\-\d{1,2}\-\d{1,2}$/;
        
        if(control.value && !regex_date.test(control.value))
        {
            return { invalidDateFormat: true};
        }

        // Parse the date parts to integers
        var parts   = control.value.split("-");
        var day     = parseInt(parts[2], 10);
        var month   = parseInt(parts[1], 10);
        var year    = parseInt(parts[0], 10);

        // Check the ranges of month and year
        if(control.value && (year < 1000 || year > 3000 || month == 0 || month > 12))
        {
            return { invalidDateYearRange: true};
        }

        if (birth_date > date) {
            return { invalidFutureDate: true};
        }
        
        if (dateYear - year < 18) {
            return { invalidUnderAge: true};
        }

        var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

        // Adjust for leap years
        if(control.value && (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)))
        {
            monthLength[1] = 29;
        }

        // Check the range of the day 
        if (control.value && (day < 0 || day >= monthLength[month - 1])) {
            return { invalidDateDayRange: true};
        }
    }

    countWordsValidator(control: FormControl): {[key:string]: any} {
        if (control.value.split(" ").length < 2) {
            return { countWords: true };
        }
        if ((control.value.split(" ").length < 3) && (control.value.substr(control.value.length-1) === " ")){
            return { countWords: true };
        }
        if(control.value.split(" ").length > 6) {
            return { countWords: true };
        }
    }

    minSelectedCheckboxes(min = 1) {
        const validator: ValidatorFn = (formArray: FormArray) => {
          const totalSelected = formArray.controls
            // get a list of checkbox values (boolean)
            .map(control => control.value)
            // total up the number of checked checkboxes
            .reduce((prev, next) => next ? prev + next : prev, 0);
      
          // if the total is not greater than the minimum, return the error message
          return totalSelected >= min ? null : { required: true };
        };
      
        return validator;
    }

   
}


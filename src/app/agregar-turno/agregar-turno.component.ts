
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, ValidationErrors } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { AreaService } from '../http-services/area.service';
import { TurnoService } from '../http-services/turno.service';
import { Area } from '../modelos/modelo-area';
import { ToastrService } from 'ngx-toastr';
import { FormsValidationService } from '../shared-components/forms-validation.service';
import { StorageService } from '../core';
import { DataFormatService } from '../shared-services/data-format.service';
import { User } from '../modelos/modelo-user';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { Turno } from '../modelos/modelo-turno';
import { formTurno } from '../forms/form-turno';

@Component({
  selector: 'app-agregar-turno',
  templateUrl: './agregar-turno.component.html',
  styleUrls: ['./agregar-turno.component.css', '../app.component.scss'],
  providers:[ FormsValidationService, AreaService, 
              StorageService, DataFormatService, TurnoService]
})
export class AgregarTurnoComponent {
    form_agregar_turno: FormGroup;
    turno_nuevo: Turno;
    area_nueva: Area;
    errMsg: any;
    loading = false;
    currentUser: User;    
    areas: Area[] = [];
    areasLibres: Area[] = [];
    today: number = Date.now();
    areaId: number;
    fecha: Date = new Date(Date.now()); 
    pipe: DatePipe = new DatePipe('en-US'); // Use your own locale

  constructor(
        public router:Router,
        public formBuilder: FormBuilder,
        public areaService: AreaService,
        public toastr: ToastrService,
        public validation: FormsValidationService,
        public storageService: StorageService,
        public dataFormatService: DataFormatService,
        public sideBar: SideBarMenuComponent,
        public turnoService: TurnoService
    ) {
      this.loadAreas();
        this.sideBar.setTitle('Marcar Entrada');   
        this.currentUser = this.storageService.getCurrentUser();         
        this.form_agregar_turno = this.formBuilder.group({            
            empleado_id: [null],
            id:[null],
            area_id: ['', Validators.compose([Validators.required, this.validation.notNullValidator])],
            actividad: ['', Validators.compose([Validators.maxLength(200), Validators.required, this.validation.notNullValidator])],            
            status: [false],
        }
    );
    }

   onSubmit({ value,  valid }: { value: formTurno, valid: boolean }) { 
       if(!this.form_agregar_turno.valid){
            this.toastr.error('Problema en el formulario!', 'Error!');
        }else{ 
            let time = new Date (Date.now());
            this.turno_nuevo = new Turno();
            this.turno_nuevo.empleado_id = this.currentUser.id;
            this.turno_nuevo.area_id = value.area_id;
            this.turno_nuevo.fecha = this.pipe.transform(this.fecha,'yyyy-MM-dd');
            this.turno_nuevo.hora_entrada =  this.pipe.transform(this.fecha,'HH:mm');
            this.turno_nuevo.hora_salida = this.pipe.transform(this.fecha,'HH:mm');
            this.turno_nuevo.actividad = value.actividad;    
            this.loading = true;
           
            this.turnoService
            .createTurno(this.turno_nuevo, this.storageService.getCurrentToken())
            .subscribe(
                res => {      
                  this.loading = false; 
                  this.turno_nuevo = res;
                  this.storageService.setCurrentTurno(this.turno_nuevo);
                  this.sideBar.hayTurno = true;
                  this.router.navigate(['/sesion']);                                   
                  this.toastr.success('Inicio de jornada agregado');                      
                },error=>{
                    this.loading = false;     
                    console.log(error);        
                    this.toastr.error('Algo salió mal registrando la entrada.');     
                }
            );
        }
    }    

    public volver() : void {
        this.router.navigate(['sesion', {outlets: {homesesion: ['listaturnos']}}]);
      }

  /**
   * Loads areas into a SelectList.
   */
  public loadAreas() {
      console.info("---loadAreas()---");
      this.loading = true;
      this.areaService.getAreas().subscribe(
      areas => { 
      this.loading = false;
      this.areas = areas;  
      this.areasLibres = this.areas.filter(s=>s.status == false);
    });
  } 
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ProgramaService, StorageService, AuthenticationService } from '../core';
import { UserService } from '../http-services/user.service';
import { User } from '../modelos/modelo-user';
import { RoleService } from '../http-services/role.service';
import { FormsValidationService } from '../shared-components/forms-validation.service';
import { formUser } from '../forms/form-user';
import { Role } from '../modelos/modelo-rol';
import { Sesion } from '../modelos/modelo-sesion';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-actualizar-usuario',
  templateUrl: './actualizar-usuario.component.html',
  styleUrls: ['./actualizar-usuario.component.css', '../app.component.scss'],
  providers: [UserService, ProgramaService, 
    RoleService, FormsValidationService, 
    StorageService, AuthenticationService]
})
export class ActualizarUsuarioComponent implements OnInit {  
  form_registro_usuario: FormGroup;
  roles: Role[];
  data: Sesion = new Sesion();
  currentUser: User;
  updateUser: User;
  loading = false;
  userEnabled: boolean = false;
  sub:any;
  role_:string;
  currentRole: string;
  date_: Date; 
  pipe: DatePipe = new DatePipe('en-US'); // Use your own locale

  constructor(
    public router:Router,
    public formBuilder: FormBuilder,
    public validation: FormsValidationService,
    public userService: UserService,
    public roleService: RoleService,     
    public programaService: ProgramaService,
    public toastr: ToastrService,
    private storageService: StorageService,
    public route: ActivatedRoute,
    public sideBar: SideBarMenuComponent  
) {}

ngOnInit() {   
  this.sideBar.setTitle('Actualizar Usuario');
  this.loadRoles(); 
  this.currentUser = this.storageService.getCurrentUser();  
  this.currentRole = this.currentUser.role;
  this.form_registro_usuario = this.formBuilder.group({
    id: [null,],
    name: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator, this.validation.justLettersValidator, this.validation.tooManyWordsValidator])],
    bio: ['', Validators.compose([Validators.maxLength(500)])],
    us_id: ['', Validators.compose([Validators.maxLength(20)])],
    lastname: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],
    telephone: ['', Validators.compose([this.validation.cellphoneValidator, Validators.maxLength(15)])],
    email: ['', Validators.compose([Validators.required, this.validation.emailValidator,Validators.maxLength(40)])],
    instagram: ['', Validators.compose([Validators.maxLength(60)])],
    twitter:  ['', Validators.compose([Validators.maxLength(60)])],
    facebook:  ['', Validators.compose([Validators.maxLength(60)])],
    pni: ['', Validators.compose([Validators.maxLength(60)])],
    c_locucion: ['', Validators.compose([Validators.maxLength(60)])],
    birthdate:['',  Validators.compose([ this.validation.isValidBirthDate])],
    role: ['', Validators.required],
    enabled: [false,],
  });

  this.loading=true;  
  this.sub = this.route.params.subscribe(params => {   
  const userId = params['id'];
  this.userService
  .getUser(userId, this.storageService.getCurrentToken())
  .subscribe (c => {
    this.loading=false;
    this.updateUser = c;
    //console.log("update user");
    //console.log(this.updateUser);
    let form = this.updateUser; 
    this.date_ = new Date(this.updateUser.birthdate);
    //console.log( this.date_);
    this.role_ = this.updateUser.role;
    //console.log(this.role_);
    form = {
      "id": this.updateUser.id,
      "name": this.updateUser.name,
      "lastname": this.updateUser.lastname,
      "email": this.updateUser.email,
      "telephone": this.updateUser.telephone,
      "role": this.updateUser.role,
      "birthdate": this.updateUser.birthdate, 
      "pni": this.updateUser.pni,
      "c_locucion": this.updateUser.c_locucion,
      "us_id" : this.updateUser.us_id,
      "instagram": this.updateUser.instagram,
      "facebook": this.updateUser.facebook,
      "twitter": this.updateUser.twitter,
      "bio": this.updateUser.bio,
      "enabled" : this.updateUser.enabled          
    };
    this.form_registro_usuario.setValue(form);   
    this.userEnabled = this.updateUser.enabled;
    this.role_ = this.updateUser.role;
    },error => {
      this.loading=false;
      console.error(error);
    }
  );
  //this.loadRoles();

  });
    
  }

 /**
   * When user press submit button.
   */
  onSubmit({ value, valid }: { value: formUser, valid: boolean }) { 
    if(!this.form_registro_usuario.valid){
        this.toastr.error('Debe llenar todos los campos obligatorios!', 'Error!');
    }else {
      this.loading = true;      
      value.enabled = this.userEnabled;
      //console.log(value);
      this.userService
      .updateUser(value, this.storageService.getCurrentToken())
      .subscribe(
          (res) => {
            this.loading = false;  
            //console.log(res);    
            this.toastr.success('Usuario actualizado con éxito!');    
            this.router.navigate(['sesion', {outlets: {homesesion: ['usuarios']}}]);        
          },error=>{
            this.loading = false;           
            console.log(error);             
            this.toastr.error('Algo salió mal. Comunicarse con soporte.'); 
          }
       );          
    } 
  }

   
  /**
   * Loads Roles into a SelectList.
   */
  public loadRoles() {
    console.info("---loadRoles()---");
    this.roleService.getRoles().subscribe
      (roles => { 
        this.roles = roles;
    });
  }

  onCheckChange(isChecked: boolean) {
    if(isChecked) {
        this.userEnabled = true;
    } else {
      this.userEnabled = false;
    }
    console.log(this.userEnabled);
  }

  public volver() : void {
    if (this.currentRole == 'Adminstrador'){
      this.router.navigate(['sesion', {outlets: {homesesion: ['usuarios']}}]);
    }
    else {
      this.router.navigate(['/sesion']);
    }
  }

}


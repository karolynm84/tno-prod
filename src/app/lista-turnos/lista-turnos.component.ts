import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TurnoService } from '../http-services/turno.service';
import { Turno } from '../modelos/modelo-turno';
import { StorageService } from '../core';
import { User } from '../modelos/modelo-user';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';

@Component({
  selector: 'app-lista-turnos',
  templateUrl: './lista-turnos.component.html',
  styleUrls: ['./lista-turnos.component.css'],
  providers: [TurnoService]
})

/**
 * Turnos list component controller.
 */
export class ListaTurnosComponent implements OnInit {
  turnos: Turno[] = [];
  turnosAutorizados: Turno[] = [];
  autorizado:boolean;
  errMsg: any;
  clientid: number;
  loading = false;
  p: number = 1;
  currenUser: User;
  id_: string;
  role_:string;
  listaTurnosCompletos:any[];

  /**
   * ClientsListComponent constructor.
   */
  constructor(
    public servicioTurno: TurnoService,
    public toastr: ToastrService,
    public router: Router,
    public storageService: StorageService,
    public sideBar: SideBarMenuComponent
  ) { }

/**
 * Gets list of turnos from data base.
 */
  getTurnos() {
    console.info("--getTurnos()---");
    this.loading = true;
    this.currenUser = this.storageService.getCurrentUser();
    this.role_ = this.currenUser.role;
    this.id_ = this.currenUser.id.toString();
    this.servicioTurno.getTurnosCompletos(this.storageService.getCurrentToken())
      .subscribe(turnosCompletos => {  
        this.loading = false;      
        this.turnos = turnosCompletos;
        //console.log(turnosCompletos);

        /** Obtiene la lista de turnos que el usuario está
         *  autorizado para modificar en caso de que no sea administrador.
         */
        var id = this.currenUser.id;
        if (this.currenUser.role!='Administrador'){
          console.log('No es admin.');
          var turnosOperador = this.turnos.filter(function (turno) {
            return turno.empleado_id === id;
          });
          this.turnos = turnosOperador;          
        }  
          
      },error=>{
        this.loading = false;
        console.error(error);             
        this.toastr.info('Algo salió mal.');     
    }
    );    
  }

 /**
  * Initialization of component.
  */
  ngOnInit(): void {
    this.sideBar.setTitle('Lista de Turnos');
    this.getTurnos();
  }

  setClient(clientid: number) {
    this.clientid = clientid;
  }

  /**
   * Deletes a client from turnos list.
   * @param id client id.
   */
  delete(id: string) {
    console.info("---delete()---");
    this.servicioTurno.deleteTurno(id, this.storageService.getCurrentToken()).subscribe(
      (res) => {
        this.toastr.success('Turno eliminado!');
        this.turnos.forEach((t, i) => {
          if (t.id === id) { 
            this.turnos.splice(i, 1); 
          }
        });
      }
    );
  }

  public actualizarTurno(id: string): void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['actualizarturno', id]}}]);
  }
  public detalleTurno(id: string): void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['detalleturno', id]}}]);
  }
}

import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProgramaService } from '../http-services/programa.service';
import { Programa } from '../modelos/modelo-programa';
import { ToastrService } from 'ngx-toastr';
import { FormsValidationService } from '../shared-components/forms-validation.service';
import { StorageService } from '../core';
import { DataFormatService } from '../shared-services/data-format.service';
import { User } from '../modelos/modelo-user';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { SolicitudPautasService } from '../http-services/solicitud-pauta.service';
import { SolicitudPauta } from '../modelos/modelo-solicitud-pauta';
import { formSolicitudPauta } from '../forms/form-solicitud-pauta';

@Component({
  selector: 'app-agregar-solicitud-pauta',
  templateUrl: './agregar-solicitud-pauta.component.html',
  styleUrls: ['./agregar-solicitud-pauta.component.css', '../app.component.scss'],
  providers:[ FormsValidationService, ProgramaService, 
              StorageService, DataFormatService, SolicitudPautasService ]
})
export class AgregarSolicitudPautaComponent {
  statusList = [
    'pendiente',
    'enviada',
    'eliminada'
  ]

  form_agregar_solicitud: FormGroup;
  pauta_nueva: SolicitudPauta;
  errMsg: any;
  loading = false;
  currentUser: User;    
  programas: Programa[] = [];
  role:string;

  constructor(
        public router:Router,
        public formBuilder: FormBuilder,
        public programaService: ProgramaService,
        public toastr: ToastrService,
        public validation: FormsValidationService,
        public storageService: StorageService,
        public dataFormatService: DataFormatService,
        public sideBar: SideBarMenuComponent,
        private solicitudPautaService: SolicitudPautasService
    ) {
        this.sideBar.setTitle('Solicitud de Pauta de Grabación');
        this.currentUser = this.storageService.getCurrentUser();
        this.role = this.currentUser.role;
        this.loadProgramas();        
        this.form_agregar_solicitud = this.formBuilder.group({
            programa_id: ['', ],
            status: ['Pendiente', Validators.compose([Validators.maxLength(60)])],
            hora_inicio: ['', Validators.compose([Validators.required, Validators.maxLength(50), this.validation.notNullValidator])],
            hora_fin: ['', Validators.compose([Validators.required, Validators.maxLength(50), this.validation.notNullValidator])],
            user_id: [null],
            fecha:['',  Validators.compose([Validators.required])],
        }, {validator: Validators.compose([
            this.validation.dateLessThan('hora_inicio', 'hora_fin', { 'hora_inicio': true })
        ])}
    );
    }

   onSubmit({ value,  valid }: { value: formSolicitudPauta, valid: boolean }) { 
       if(!this.form_agregar_solicitud.valid){
            this.toastr.error('Problema en el formulario!', 'Error!');
        }else{ 
            this.pauta_nueva = new SolicitudPauta();
            this.pauta_nueva.fecha = value.fecha;
            this.pauta_nueva.hora_fin = value.hora_fin;
            this.pauta_nueva.hora_inicio = value.hora_inicio;
            this.pauta_nueva.programa_id = value.programa_id;
            this.pauta_nueva.status = value.status;
            this.pauta_nueva.user_id = this.currentUser.id.toString();    

            console.log(this.pauta_nueva);
            this.loading = true;
            this.solicitudPautaService
            .createSolicitudPautas(this.pauta_nueva, this.storageService.getCurrentToken())
            .subscribe(
                (res) => {                  
                  console.log(res);
                  this.solicitudPautaService
                  .sendEmailSolicitud()
                  .subscribe(
                      (resEmail) => {
                        this.loading = false;
                        console.log(resEmail);
                        this.toastr.success('Solicitud enviada con éxito. Gracias por su tiempo.');
                        if (this.currentUser.role!='Administrador')
                          this.toastr.warning('Se le contactará con la información.');                        
                        this.volver();   
                      },errorEmail=>{
                        this.loading = false;
                        console.log(errorEmail);
                        this.toastr.error('Algo salió mal.'); 
                      }
                  );                              
                },error=>{
                    this.loading = false;     
                    console.log(error);        
                    this.toastr.error('Algo salió mal.');     
                }
            );
        }
    }    

    public volver() : void {
        this.router.navigate(['sesion', {outlets: {homesesion: ['solicitudespautas']}}]);
      }

 /**
   * Loads Cortes into a SelectList.
   */
  public loadProgramas() {
    console.info("---loadProgramas()---");
    this.programaService.getProgramas().subscribe
    (programas => { 
        this.programas = programas;
    });
}


}



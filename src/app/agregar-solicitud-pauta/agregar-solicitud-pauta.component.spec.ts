import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarSolicitudPautaComponent } from './agregar-solicitud-pauta.component';

describe('AgregarSolicitudPautaComponent', () => {
  let component: AgregarSolicitudPautaComponent;
  let fixture: ComponentFixture<AgregarSolicitudPautaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarSolicitudPautaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarSolicitudPautaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarSolicitudEntrevistaComponent } from './agregar-solicitud-entrevista.component';

describe('AgregarSolicitudEntrevistaComponent', () => {
  let component: AgregarSolicitudEntrevistaComponent;
  let fixture: ComponentFixture<AgregarSolicitudEntrevistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarSolicitudEntrevistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarSolicitudEntrevistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

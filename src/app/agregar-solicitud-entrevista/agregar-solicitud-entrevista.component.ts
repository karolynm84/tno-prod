import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { formSolicitudEntrevista } from '../forms/form-solicitud-entrevista';
import { SolicitudEntrevistaService } from '../http-services/solicitud-entrevista.service';
import { SolicitudEntrevista } from '../modelos/modelo-solicitud-entrevista';
import { ToastrService } from 'ngx-toastr';
import { FormsValidationService } from '../shared-components/forms-validation.service';
import { StorageService, ProgramaService } from '../core';
import { DataFormatService } from '../shared-services/data-format.service';
import { User } from '../modelos/modelo-user';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { Programa } from '../modelos/modelo-programa';

@Component({
  selector: 'app-agregar-programa',
  templateUrl: './agregar-solicitud-entrevista.component.html',
  styleUrls: ['./agregar-solicitud-entrevista.component.css', '../app.component.scss'],
  providers:[ FormsValidationService, SolicitudEntrevistaService, 
              StorageService, DataFormatService, ProgramaService]
})

export class AgregarSolicitudEntrevistaComponent {       
    form_agregar_solicitud: FormGroup;
    form_locutores: FormGroup
    solicitud_nueva: SolicitudEntrevista;
    errMsg: any;
    loading = false;
    currentUser: User;    
    conoceTNO_: boolean = false;
    programa_especifico_: boolean = false;
    programas: Programa[] = [];

  constructor(
        public router:Router,
        public formBuilder: FormBuilder,
        public solicitudEntrevistaService: SolicitudEntrevistaService,
        public toastr: ToastrService,
        public validation: FormsValidationService,
        public storageService: StorageService,
        public dataFormatService: DataFormatService,
        public sideBar: SideBarMenuComponent,
        public programaService: ProgramaService
    ) {
        this.sideBar.setTitle('Solicitud de Entrevista');
        this.currentUser = this.storageService.getCurrentUser();         
        this.form_agregar_solicitud = this.formBuilder.group({
            nombre_entrevistado: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],
            tema_entrevista: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],
            ultima_entrevista:[''],
            facebook: ['', Validators.compose([ Validators.maxLength(60)])],
            instagram: ['', Validators.compose([Validators.maxLength(60)])],
            twitter: ['', Validators.compose([Validators.maxLength(60)])],
            snapchat: ['', Validators.compose([Validators.maxLength(60)])],
            conoce_tno: [false],
            programa_especifico: [false],
            comentarios: ['', Validators.compose([ Validators.maxLength(500)])],
            programa_id: ['', Validators.compose([])]
        }
    );
        this.form_agregar_solicitud.get('programa_id').disable();
        this.form_agregar_solicitud.get('ultima_entrevista').disable();
    }

   onSubmit({ value,  valid }: { value: formSolicitudEntrevista, valid: boolean }) { 
       if(!this.form_agregar_solicitud.valid){
            this.toastr.error('Problema en el formulario!', 'Error!');
        }else{ 
            this.solicitud_nueva = new SolicitudEntrevista();
            this.solicitud_nueva.user_id = this.currentUser.id.toString();
            this.solicitud_nueva.nombre_entrevistado = value.nombre_entrevistado;
            this.solicitud_nueva.ultima_entrevista = value.ultima_entrevista;
            this.solicitud_nueva.facebook = value.facebook;
            this.solicitud_nueva.twitter = value.twitter;
            this.solicitud_nueva.snapchat = value.snapchat;
            this.solicitud_nueva.instagram = value.instagram;
            this.solicitud_nueva.comentarios = value.comentarios;
            this.solicitud_nueva.programa_id = value.programa_id;
            this.solicitud_nueva.programa_especifico = this.programa_especifico_;
            this.solicitud_nueva.conoce_tno = this.conoceTNO_;
            this.solicitud_nueva.tema_entrevista = value.tema_entrevista;
            this.solicitud_nueva.status = 'Pendiente';

            console.log(this.solicitud_nueva);
            this.loading = true;
            this.solicitudEntrevistaService
            .createSolicitudEntrevista(this.solicitud_nueva, this.storageService.getCurrentToken())
            .subscribe(
                (res) => {                  
                  console.log(res);
              this.solicitudEntrevistaService
              .sendEmailSolicitud()
                  .subscribe(
                      (resEmail) => {
                        this.loading = false;
                        console.log(resEmail);
                        this.toastr.success('Solicitud de Entrevista enviada con éxito. Gracias por su tiempo.');
                        if (this.currentUser.role!='Administrador')
                          this.toastr.warning('Enviado para revisión y será publicado luego de su aprobación.');                        
                          this.volver();   
                      },errorEmail=>{
                        this.loading = false;
                        console.log(errorEmail);
                        this.toastr.error('Algo salió mal.'); 
                      }
                  );                              
                },error=>{
                    this.loading = false;     
                    console.log(error);        
                    this.toastr.error('Algo salió mal.');     
                }
            );
        }
    }    


    public volver() : void {
        this.router.navigate(['sesion', {outlets: {homesesion: ['listaprogramas']}}]);
      }

    /**
     * Gets the value of is_agent.
     */
    public conoceTNO() : boolean {
        return this.conoceTNO_;
    }

    /**
     * Captura el check conoce TNO Radio y habilita o deshabilita
     * el campo fecha_ultima_entrevista.
     */
    public changeConoceTNO() {
    console.info("---changeConoceTNO()---");
        if (this.conoceTNO_==true) {
            this.conoceTNO_=false;
            this.form_agregar_solicitud.get('ultima_entrevista').disable();
        }
        else {
            this.conoceTNO_=true;
            this.form_agregar_solicitud.get('ultima_entrevista').enable();
        }
    }

/**
   * Loads programas into a SelectList.
   */
  public loadProgramas() {
    console.info("---loadProgramas()---");
    this.loading = true;
    this.programaService.getProgramas().subscribe
    (programas => { 
      this.loading = false;
        this.programas = programas;
    });
  } 

    /**
     * Changes.
     */
    public changeProgramaEspecifico() {
        console.info("---changeProgramaEspecífico()---");
        if (this.programa_especifico_==true) {
            this.programa_especifico_=false;
            this.form_agregar_solicitud.get('programa_id').disable();
        }
        else {
            this.programa_especifico_=true;
            this.loadProgramas();
            this.form_agregar_solicitud.get('programa_id').enable();
        }
    }  

}

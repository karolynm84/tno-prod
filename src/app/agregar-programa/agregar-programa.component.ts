import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { formPrograma } from '../forms/form-programa';
import { ProgramaService } from '../http-services/programa.service';
import { Programa } from '../modelos/modelo-programa';
import { ToastrService } from 'ngx-toastr';
import { FormsValidationService } from '../shared-components/forms-validation.service';
import { StorageService } from '../core';
import { DataFormatService } from '../shared-services/data-format.service';
import { User } from '../modelos/modelo-user';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { CorteProgramaService } from '../http-services/corte-programa.service';
import { CortePrograma } from '../modelos/modelo-corte-programa';

@Component({
  selector: 'app-agregar-programa',
  templateUrl: './agregar-programa.component.html',
  styleUrls: ['./agregar-programa.component.css', '../app.component.scss'],
  providers:[ FormsValidationService, ProgramaService, 
              StorageService, DataFormatService, CorteProgramaService ]
})

export class AgregarProgramaComponent {   
    hora = [
        '06:00',
        '07:00',
        '08:00',
        '09:00',
        '10:00',
        '11:00',
        '12:00',
        '13:00',
        '14:00',
        '15:00',
        '16:00',
        '17:00',
        '18:00',
        '19:00',
        '20:00',
        '21:00',
        '22:00',
        '23:00',
    ];

    dias = [ 
        'lunes', 
        'martes', 
        'miércoles', 
        'jueves', 
        'viernes',  
        'sábado',  
        'domingo'
    ];

    clasificaciones = [ 
        'Todo Público', 
        'Supervisado', 
        'Adulto'
    ];

    tipos = [
        'Cultural y Educativo',
        'Informativo',
        'Mixto',
        'Recreacional o Deportivo',
        'Opinión'
    ];
        
    form_agregar_programa: FormGroup;
    form_locutores: FormGroup
    programa_nuevo: Programa;
    errMsg: any;
    isChecked: boolean = false;
    diasCheck: string[] = [];
    loading = false;
    currentUser: User;    
    enabled: boolean = false;
    cortes: CortePrograma[] = [];

  constructor(
        public router:Router,
        public formBuilder: FormBuilder,
        public programaService: ProgramaService,
        public toastr: ToastrService,
        public validation: FormsValidationService,
        public storageService: StorageService,
        public dataFormatService: DataFormatService,
        public sideBar: SideBarMenuComponent,
        private corteService: CorteProgramaService
    ) {
        this.sideBar.setTitle('Registro de Programa');
        this.currentUser = this.storageService.getCurrentUser();
        this.loadCortes();        
        this.form_agregar_programa = this.formBuilder.group({
            pro_nombre: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],
            pro_sinopsis: ['', Validators.compose([Validators.required, Validators.maxLength(500), this.validation.notNullValidator])],
            pro_tipo: ['', Validators.compose([Validators.required, Validators.maxLength(50), this.validation.notNullValidator])],
            pro_clasificacion: ['', Validators.compose([Validators.required, Validators.maxLength(50), this.validation.notNullValidator])],
            pro_hora_inicio: ['', Validators.compose([Validators.required, Validators.maxLength(50), this.validation.notNullValidator])],
            pro_hora_fin: ['', Validators.compose([Validators.required, Validators.maxLength(50), this.validation.notNullValidator])],
            pro_email: ['', Validators.compose([Validators.required, this.validation.emailValidator, Validators.maxLength(40)])],
            pro_dia_semana: [new FormArray([])/*,  this.validation.minSelectedCheckboxes(1)*/],
            pro_fecha_origen:['',  Validators.compose([Validators.required])],
            pro_locutor_1: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],
            pro_locutor_2: ['', Validators.compose([Validators.maxLength(60)])],
            pro_locutor_3: ['', Validators.compose([Validators.maxLength(60)])],
            pro_locutor_4: ['', Validators.compose([Validators.maxLength(60)])],
            pro_facebook: ['', Validators.compose([Validators.maxLength(60)])],
            pro_twitter: ['', Validators.compose([ Validators.maxLength(60)])],
            pro_instagram: ['', Validators.compose([ Validators.maxLength(60)])],
            pro_snapchat: ['', Validators.compose([ Validators.maxLength(60)])],
            pro_productor: ['', Validators.compose([Validators.required, Validators.maxLength(60)])],
            habilitado: [false],
            pro_corte: ['', Validators.compose([Validators.required, this.validation.notNullValidator])]
        }, 
        {
            validator: Validators.compose([
            this.validation.dateLessThan('pro_hora_inicio', 'pro_hora_fin', { 'pro_hora_inicio': true })
        ])}
    );
//console.log(this.form_agregar_programa.hasError('dateLessThan'));
    }

   onSubmit({ value,  valid }: { value: formPrograma, valid: boolean }) { 
       if(!this.form_agregar_programa.valid){
            this.toastr.error('Problema en el formulario!', 'Error!');
        }else{ 
            this.programa_nuevo = new Programa();
            this.programa_nuevo.pro_email = value.pro_email;
            this.programa_nuevo.pro_nombre = value.pro_nombre;
            this.programa_nuevo.pro_sinopsis = value.pro_sinopsis;
            this.programa_nuevo.pro_fecha_origen = value.pro_fecha_origen;
            this.programa_nuevo.pro_productor = value.pro_productor;
            this.programa_nuevo.pro_hora_inicio = value.pro_hora_inicio;
            this.programa_nuevo.pro_hora_fin = value.pro_hora_fin;
            this.programa_nuevo.pro_locutor_1 = value.pro_locutor_1;
            this.programa_nuevo.pro_locutor_2 = value.pro_locutor_2;
            this.programa_nuevo.pro_locutor_3 = value.pro_locutor_3;
            this.programa_nuevo.pro_locutor_4 = value.pro_locutor_4;
            this.programa_nuevo.pro_dia_semana = this.diasCheck.toString();
            this.programa_nuevo.fk_user_last_modifier = this.currentUser.id;
            this.programa_nuevo.user_id = this.currentUser.id;
            this.programa_nuevo.pro_twitter = value.pro_twitter;
            this.programa_nuevo.pro_facebook = value.pro_facebook;
            this.programa_nuevo.pro_instagram = value.pro_instagram;
            this.programa_nuevo.pro_snapchat = value.pro_snapchat;            
            this.programa_nuevo.pro_corte = value.pro_corte;
            this.programa_nuevo.pro_tipo = value.pro_tipo;
            this.programa_nuevo.pro_clasificacion = value.pro_clasificacion;

            if (this.currentUser.role == 'Administrador') {
                this.programa_nuevo.habilitado = this.enabled;
            }else {
                this.programa_nuevo.habilitado = false;
            }

            console.log(this.programa_nuevo);
            this.loading = true;
            this.programaService
            .createPrograma(this.programa_nuevo, this.storageService.getCurrentToken())
            .subscribe(
                (res) => {                  
                  console.log(res);
                  this.programaService
                  .sendEmailAprobación(this.storageService.getCurrentToken())
                  .subscribe(
                      (resEmail) => {
                        this.loading = false;
                        console.log(resEmail);
                        this.toastr.success('Programa creado con éxito. Gracias por su tiempo.');
                        if (this.currentUser.role!='Administrador')
                          this.toastr.warning('Enviado para revisión y será publicado luego de su aprobación.');                        
                          this.volver();   
                      },errorEmail=>{
                        this.loading = false;
                        console.log(errorEmail);
                        this.toastr.error('Algo salió mal con el email.');
                        this.volver();    
                      }
                  );                              
                },error=>{
                    this.loading = false;     
                    console.log(error);        
                    this.toastr.error('Algo salió mal, no se guardó.');     
                }
            );
        }
    }    

    /**
     * Vigila los días de la semana que son seleccionados en el formulario
     * y actualiza la lista de días.
     */
    onCheckChange(dia: string, isChecked: boolean) {
        if(isChecked) {
            this.diasCheck.push(dia);
        } else {
            this.diasCheck = this.diasCheck.filter(item => item !== dia);
        }
        //console.log(JSON.stringify(this.diasCheck));
    }

    public volver() : void {
        this.router.navigate(['sesion', {outlets: {homesesion: ['listaprogramas']}}]);
      }

    /**
     * Changes the agent enabled.
     */
    public changeHabilidado() {
    console.info("AgregarProgramas---changeHabilitado()---");
        if (this.enabled==true) {
            this.enabled=false;
            this.form_agregar_programa.get('habilitado').disable();
        }
        else {
            this.enabled=true;
            this.form_agregar_programa.get('habilitado').enable();
        }
    }

    /**
   * Loads Cortes into a SelectList.
   */
    public loadCortes() {
        console.info("AgregarPrograma---loadCortes()---");
        this.corteService.getCortes().subscribe
        (cortes => { 
            this.cortes = cortes;
        });
    }
}



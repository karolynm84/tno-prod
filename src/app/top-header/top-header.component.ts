import { Component, OnInit } from '@angular/core';

import {Router} from "@angular/router";
import { StorageService, AuthenticationService } from '../core';
import { DataSharingService } from '../shared-services/data-sharing.service';

@Component({
  selector: 'app-top-header',
  templateUrl: './top-header.component.html',
  styleUrls: ['./top-header.component.css'],
  providers: [StorageService, AuthenticationService]
})
export class TopHeaderComponent implements OnInit {
  options: string[];
  selectedOption: string;
  isUserLoggedIn: boolean = false;
  private localStorageService;

  a = [ 62029, 35261, 21299, 21862, 49219, 16360, 25339, 57740, 14011, 63124, 58114, 13523, 8572, 48738, 59832, 47831, 
    33352, 48870, 51186, 19923, 26119, 28904, 56724, 31426, 25033, 42369, 46822, 37823, 17112, 644, 14419, 13031, 42579, 13687, 28002,
     50160, 14321, 43810, 4252, 3268, 53366, 40472, 12406, 12503, 15340, 34172, 41309, 25057, 3794, 12433, 24977, 64256, 29073, 14723, 
     32315, 3424, 25243, 43095, 51646, 25917, 22705, 45203, 12743, 24779, 14159, 45355, 1828, 32876, 33441, 39677, 580, 25373, 36961, 25169, 
     17577, 33315, 33382, 43073, 24135, 31066, 44139, 13331, 14411, 13389, 28778, 48600, 26261, 46029, 13946, 32551, 39171, 14419, 63646, 7973, 
     27256, 12763, 18091, 23928, 25073, 20199, 12553, 53837, 39442, 32910, 60994, 26802, 25583, 63221, 33007, 47190, 12967, 37161, 37785, 2257, 
     26826, 41089, 474, 28100, 26251, 11488, 38417, 29497, 16330, 22968, 63852, 37948, 41858, 30890, 48456, 26896, 35896, 25561, 33835, 
     40227, 34154, 25867, 13309, 30415, 53555, 24766, 412, 50164, 13463, 25876, 44982, 18754, 37149, 4302, 17948, 6330, 39625, 12749, 56366, 
     12791, 55940, 13757, 14419, 1520, 24967, 57491, 31459, 56042, 62812, 28162, 44183, 13633, 14081, 60459, 25899, 40205, 42066, 57686, 41164, 
     58960, 22084, 29532, 428, 17430, 301, 41569, 24923, 15860, 33793, 39945, 63412, 12907 ];

     b = [];

  constructor(
    private router: Router,
    private storageService: StorageService,
    private authenticationService: AuthenticationService,
    private dataSharingService: DataSharingService
  ) {
      this.localStorageService = localStorage;
      this.dataSharingService.isUserLoggedIn.subscribe( value => {
      this.isUserLoggedIn = value;
      console.log("--constructor header--");
  });
   }

  ngOnInit() {
   
    console.log("--oninit--");
    if (this.localStorageService.getItem('currentUser')=='null' || this.localStorageService.getItem('currentUser')==null){
      console.info("---not logged---");     
      this.isUserLoggedIn=false;
    }
    else {
      console.log("---logged---");
       this.dataSharingService.isUserLoggedIn.next(true);
      this.isUserLoggedIn=true;
    }
  }

  public setBit = (num) => {
    return num >> 8;
  }

  public micuenta():void {
    console.log("---micuenta()---");
    this.router.navigate(['/sesion']);
  }

  public home():void {
    console.log("---home()---");
    this.router.navigate(['/']);
  }

  public logout(): void {
    console.log("---logOut()---");
    //this.storageService.removeCurrentSession();
    //this.router.navigate(['/login']);
    this.authenticationService.logout(this.storageService.getCurrentToken()).subscribe(
        response => {
          if(response) {
            this.dataSharingService.isUserLoggedIn.next(false);
            this.storageService.logout();          
          }
        },
        error=> {
          console.log(error);
        }
    );
  }

  public login(): void {
    this.dataSharingService.isUserLoggedIn.next(true);
    this.router.navigate(['/login']);    
  }

  public editarPerfil() : void {
    var id = this.storageService.getCurrentUser().id;
    this.router.navigate(['/sesion/usuario/actualizar/'+id]);
  }

  isPrime(number) {

    var d = number - 1;

    // If n is less than 2 or not an integer then we can say not a prime number.
    if (number < 2) {
        alert('If n is less than 2 or not an integer then we can say not a prime number.');
        return false
    }

    if (number != Math.round(number)) {
        alert('If n is less than 2 or not an integer then we can say not a prime number.');
        return false
    }

    while (d > 1) {
        if ((number % d) == 0) {
            return false;
        }
        d--;
    }
    return true;
}
   
}

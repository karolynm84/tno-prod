import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '../../../node_modules/@angular/forms';
import { Router, ActivatedRoute } from '../../../node_modules/@angular/router';
import { StorageService } from '../core';
import { ToastrService } from '../../../node_modules/ngx-toastr';
import { FormsValidationService } from '../shared-components/forms-validation.service';
import { DataFormatService } from '../shared-services/data-format.service';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { User } from '../modelos/modelo-user';
import { SolicitudPatrocinio } from '../modelos/modelo-solicitud-patrocinio';
import { SolicitudPatrocinioService } from '../http-services/solicitud-patrocinio.service';
import { formSolicitudPatrocinio } from '../forms/form-solicitud-patrocinio';

@Component({
  selector: 'app-actualizar-solicitud-patrocinio',
  templateUrl: './actualizar-solicitud-patrocinio.component.html',
  styleUrls: ['./actualizar-solicitud-patrocinio.component.css', '../app.component.scss'],
  providers: [ FormsValidationService,  SolicitudPatrocinioService,
    StorageService, DataFormatService ]
})
export class ActualizarSolicitudPatrocinioComponent implements OnInit {

  statusList = [
    'pendiente',
    'aprobada',
    'eliminada'
  ]

  form_agregar_solicitud: FormGroup;
  form_locutores: FormGroup
  solicitud_nueva: SolicitudPatrocinio;
  errMsg: any;
  isChecked: boolean = false;
  diasCheck: string[] = [];
  loading = false;
  formSolicitudPatrocinio: formSolicitudPatrocinio;
  sub: any;
  currentUser: User;
  role:string;
  
  constructor (
    public router: Router,
        public formBuilder: FormBuilder,
        public solicitudPatrocinioService: SolicitudPatrocinioService,
        public toastr: ToastrService,
        public validation: FormsValidationService,
        public storageService: StorageService,
        public route: ActivatedRoute,
        public dataFromatService: DataFormatService,
        public sideBar: SideBarMenuComponent
      ) {}

  ngOnInit() {
      this.sideBar.setTitle('Actualizar Solicitud de Patrocinios');
      this.currentUser = this.storageService.getCurrentUser();
      this.role = this.currentUser.role;
      this.loading = true;   
      this.form_agregar_solicitud = this.formBuilder.group({
        nombre_responsable: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],
        nombre_productora: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],      
        status: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],      
        tipo_evento: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],        
        fecha:['',  Validators.compose([Validators.required])],
        lugar: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],
        objetivos_evento: ['', Validators.compose([Validators.required, Validators.maxLength(500)])],
        user_id:[null,],
        id:[null,]
      })
    this.loading = true;
    this.sub = this.route.params.subscribe(params => {     
      const solicitud = params['id'];
      this.solicitudPatrocinioService
        .getSolicitudPatrocinio(solicitud, this.storageService.getCurrentToken())
        .subscribe(c => {
          this.loading = false;
          this.solicitud_nueva = c;
          let form = this.formSolicitudPatrocinio;            
          form = {
            "nombre_responsable" : this.solicitud_nueva.nombre_responsable,
            "nombre_productora" : this.solicitud_nueva.nombre_productora,
            "id" : this.solicitud_nueva.id,
            "fecha" : this.solicitud_nueva.fecha,
            "objetivos_evento" : this.solicitud_nueva.objetivos_evento,
            "tipo_evento" : this.solicitud_nueva.tipo_evento,
            "lugar" : this.solicitud_nueva.lugar,
            "user_id" : this.currentUser.id.toString(),
            "status" : this.solicitud_nueva.status
          }         
         this.form_agregar_solicitud.setValue(form);        
        });
    }, error => {
      this.loading = false;
      console.log(error);
    }
  );
  }

  /**
   * When user press submit button.
   */
  onSubmit({ value, valid }: { value: formSolicitudPatrocinio, valid: boolean }) { 
    if(!this.form_agregar_solicitud.valid){
        this.toastr.error('Debe llenar todos los campos obligatorios!', 'Error!');
    }else {
      this.loading = true;            
      this.solicitudPatrocinioService
      .updateSolicitudPatrocinio(value, this.storageService.getCurrentToken())
      .subscribe(
          (res) => {
            this.loading = false;  
            console.log(res);    
            this.toastr.success('Solicitud de Patrocinio actualizada con éxito!');
            this.volver();                               
          },error=>{
            this.loading = false;           
            console.log(error);             
            this.toastr.error('Algo salió mal. Comunicarse con soporte.'); 
          }
       );          
    } 
  }

  public volver() : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['solicitudespatrocinio']}}]);
  }
}

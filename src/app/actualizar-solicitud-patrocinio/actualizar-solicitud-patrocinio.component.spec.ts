import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarSolicitudPatrocinioComponent } from './actualizar-solicitud-patrocinio.component';

describe('ActualizarSolicitudPatrocinioComponent', () => {
  let component: ActualizarSolicitudPatrocinioComponent;
  let fixture: ComponentFixture<ActualizarSolicitudPatrocinioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarSolicitudPatrocinioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarSolicitudPatrocinioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

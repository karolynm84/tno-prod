import { Component, OnInit } from '@angular/core';
import { User } from '../modelos/modelo-user';
import { FormsValidationService } from '../shared-components/forms-validation.service';
import { AreaService } from '../http-services/area.service';
import { StorageService } from '../core';
import { DataFormatService } from '../shared-services/data-format.service';
import { TurnoService } from '../http-services/turno.service';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { Turno } from '../modelos/modelo-turno';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-agregar-salida',
  templateUrl: './agregar-salida.component.html',
  styleUrls: ['./agregar-salida.component.css'],
  providers:[ FormsValidationService, AreaService, 
    StorageService, DataFormatService, TurnoService]
})
export class AgregarSalidaComponent implements OnInit {

  currentUser: User;  
  turnos: Turno[] = [];
  turnosAutorizados: Turno[] = [];  
  loading = false;
  turno : Turno;
  fecha: Date = new Date(Date.now()); 
  pipe: DatePipe = new DatePipe('en-US'); // Use your own locale
  constructor(
    public storageService: StorageService,
    public sideBar: SideBarMenuComponent,
    public turnoService: TurnoService,
    public areaService: AreaService,
    public toastr: ToastrService,
    public router:Router
  ) { }

  ngOnInit() {
    this.sideBar.setTitle('Marcar Salida');            
    let turno: Turno = <Turno>this.storageService
    .getCurrentTurno(this.storageService
      .getCurrentUser()
      .id
      .toString());
    turno.hora_salida =  this.pipe.transform(this.fecha,'HH:mm');
    this.loading = true;
    this.turnoService
        .updateTurno(turno, this.storageService.getCurrentToken())
        .subscribe(c => {
          this.loading = false;
          this.turno = c;
          this.storageService.setCurrentTurno(this.turno);
          this.router.navigate(['/sesion']);    
          this.sideBar.hayTurno = false;                               
          this.toastr.success('Fin de jornada agregado');
    }, error => {
      this.loading = false;
      console.log(error);
      if (error.status!=200) {
        this.toastr.error('No se pudo marcar la salida');
      }
      else {
        this.storageService.setCurrentTurno(turno);
        this.router.navigate(['/sesion']);    
        this.sideBar.hayTurno = false;                               
        this.toastr.success('Fin de jornada agregado');
      }      
    });

  }

}

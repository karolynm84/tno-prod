import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { formSolicitudEspacios } from '../forms/form-solicitud-espacio';
import { SolicitudEspacioService } from '../http-services/solicitud-espacios.service';
import { SolicitudEspacio } from '../modelos/modelo-solicitud-espacio';
import { ToastrService } from 'ngx-toastr';
import { FormsValidationService } from '../shared-components/forms-validation.service';
import { StorageService } from '../core';
import { DataFormatService } from '../shared-services/data-format.service';
import { User } from '../modelos/modelo-user';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { CorteProgramaService } from '../http-services/corte-programa.service';
import { CortePrograma } from '../modelos/modelo-corte-programa';

@Component({
  selector: 'app-agregar-programa',
  templateUrl: './agregar-solicitud-espacio.component.html',
  styleUrls: ['./agregar-solicitud-espacio.component.css', '../app.component.scss'],
  providers:[ FormsValidationService, SolicitudEspacioService, 
              StorageService, DataFormatService, CorteProgramaService]
})

export class AgregarSolicitudEspacioComponent {       
    form_agregar_solicitud: FormGroup;
    solicitud_nueva: SolicitudEspacio;
    errMsg: any;
    loading = false;
    currentUser: User;    
    tieneDemo: boolean = false;
    cortes: CortePrograma[] = [];
    diasCheck: string[] = [];
    mediosTrad: boolean = false;
    mediosOnline: boolean = false;
    vinculo: boolean= false;
    demo: boolean = false;

    dias = [ 
      'lunes', 
      'martes', 
      'miércoles', 
      'jueves', 
      'viernes',  
      'sábado',  
      'domingo'
  ];

  constructor(
        public router:Router,
        public formBuilder: FormBuilder,
        public solicitudEspacioService: SolicitudEspacioService,
        public toastr: ToastrService,
        public validation: FormsValidationService,
        public storageService: StorageService,
        public dataFormatService: DataFormatService,
        public sideBar: SideBarMenuComponent,
        public corteService: CorteProgramaService
    ) {
        this.sideBar.setTitle('Solicitud de Espacio');
        this.currentUser = this.storageService.getCurrentUser();     
        this.loadCortes();    
        this.form_agregar_solicitud = this.formBuilder.group({
            demo: [false],
            user_id: ['',],
            demo_link:['',],
            corte: ['',Validators.compose([Validators.required, this.validation.notNullValidator])],
            seg_objetivo: ['', Validators.compose([Validators.required, this.validation.notNullValidator])],
            dias:  [new FormArray([])/*,  this.validation.minSelectedCheckboxes(1)*/],
            experiencia_medios: [false],
            experiencia_medios_online: [false],
            vinculo_otros_medios: [false],
            tipo_experiencia: ['',  Validators.compose([Validators.required, this.validation.notNullValidator])],
            conductor_1: ['',  Validators.compose([Validators.required, this.validation.notNullValidator])]
        }
    );
        this.form_agregar_solicitud.get('demo_link').disable();
    }

   onSubmit({ value,  valid }: { value: formSolicitudEspacios, valid: boolean }) { 
       if(!this.form_agregar_solicitud.valid){
            this.toastr.error('Problema en el formulario!', 'Error!');
        }else{ 
            this.solicitud_nueva = new SolicitudEspacio();
            this.solicitud_nueva.user_id = this.currentUser.id;
            this.solicitud_nueva.demo =  this.tieneDemo;
            this.solicitud_nueva.demo_link = value.demo_link;
            this.solicitud_nueva.corte = value.corte;
            this.solicitud_nueva.seg_objetivo = value.seg_objetivo;
            this.solicitud_nueva.dias = this.diasCheck.toString();
            this.solicitud_nueva.experiencia_medios = this.mediosTrad;
            this.solicitud_nueva.comentarios = value.comentarios;
            this.solicitud_nueva.tipo_experiencia = value.tipo_experiencia;
            this.solicitud_nueva.conductor_1 = value.conductor_1;
            this.solicitud_nueva.experiencia_medios_online = this.mediosOnline;
            this.solicitud_nueva.vinculo_otros_medios = this.vinculo;

            console.log(this.solicitud_nueva);
            this.loading = true;
            this.solicitudEspacioService
            .createSolicitudEspacios(this.solicitud_nueva, this.storageService.getCurrentToken())
            .subscribe(
                (res) => {                  
                  console.log(res);
              this.solicitudEspacioService
              .sendEmailSolicitud()
                  .subscribe(
                      (resEmail) => {
                        this.loading = false;
                        console.log(resEmail);
                        this.toastr.success('Solicitud de Espacio enviada con éxito. Gracias por su tiempo.');
                        if (this.currentUser.role!='Administrador')
                          this.toastr.warning('Enviado para revisión y será publicado luego de su aprobación.');                        
                          this.volver();   
                      },errorEmail=>{
                        this.loading = false;
                        console.log(errorEmail);
                        this.toastr.error('Algo salió mal.'); 
                      }
                  );                              
                },error=>{
                    this.loading = false;     
                    console.log(error);        
                    this.toastr.error('Algo salió mal.');     
                }
            );
        }
    }    


    public volver() : void {
        this.router.navigate(['sesion', {outlets: {homesesion: ['listasolicitudespacio']}}]);
      }


    /**
     * Captura el check conoce TNO Radio y habilita o deshabilita
     * el campo fecha_ultima_entrevista.
     */
    public changeDemo() {
    console.info("---changeDemo()---");
        if (this.tieneDemo==true) {
            this.tieneDemo=false;
            this.form_agregar_solicitud.get('demo_link').disable();
        }
        else {
            this.tieneDemo=true;
            this.form_agregar_solicitud.get('demo_link').enable();
        }
    }


   /**
   * Loads Cortes into a SelectList.
   */
  public loadCortes() {
    console.info("---loadCortes()---");
    this.corteService.getCortes().subscribe
    (cortes => { 
        this.cortes = cortes;
    });
  }

  onCheckDias(dia: string, isChecked: boolean) {
    if(isChecked) {
        this.diasCheck.push(dia);
    } else {
        this.diasCheck = this.diasCheck.filter(item => item !== dia);
    }
    console.log(JSON.stringify(this.diasCheck));
}

onCheckVinculo(isChecked: boolean) {
  if(isChecked) {
      this.vinculo = true;
  } else {
    this.vinculo = false;
  }
}

onCheckMediosTrad (isChecked: boolean) {
  if(isChecked) {
      this.mediosTrad = true;
  } else {
    this.mediosTrad = false;
  }
}

onCheckMediosOnline(isChecked: boolean) {
  if(isChecked) {
      this.mediosOnline = true;
  } else {
    this.mediosOnline = false;
  }
}

   

}

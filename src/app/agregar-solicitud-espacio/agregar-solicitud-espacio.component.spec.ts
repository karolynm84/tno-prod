import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarSolicitudEspacioComponent } from './agregar-solicitud-espacio.component';

describe('AgregarSolicitudEspacioComponent', () => {
  let component: AgregarSolicitudEspacioComponent;
  let fixture: ComponentFixture<AgregarSolicitudEspacioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarSolicitudEspacioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarSolicitudEspacioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

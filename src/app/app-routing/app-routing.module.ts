import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule  } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { ProgramacionComponent } from '../programacion/programacion.component';
import { PageContainerComponent } from '../page-container/page-container.component';
import { AgregarProgramaComponent } from '../agregar-programa/agregar-programa.component';
import { ListaProgramasComponent } from '../lista-programas/lista-programas.component';
import { homesesionComponent } from '../home-sesion/home-sesion.component';
import { AuthorizatedGuard } from '../seguridad/guardian';
import { ResgistroUsuarioComponent } from '../agregar-usuario/resgistro-usuario.component';
import { DetalleUsuarioComponent } from '../detalle-usuario/detalle-usuario.component';
import { ActualizarUsuarioComponent } from '../actualizar-usuario/actualizar-usuario.component';
import { ActualizarProgramaComponent } from '../actualizar-programa/actualizar-programa.component';
import { ListaUsuarioComponent } from '../lista-usuario/lista-usuario.component';
import { DetalleProgramaComponent } from '../detalle-programa/detalle-programa.component';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { AgregarSolicitudTarifasComponent } from '../agregar-solicitud-tarifas/agregar-solicitud-tarifas.component';
import { ListaSolicitudesTarifasComponent } from '../lista-solicitudes-tarifas/lista-solicitudes-tarifas.component';
import { ActualizarSolicitudTarifasComponent } from '../actualizar-solicitud-tarifas/actualizar-solicitud-tarifas.component';
import { AgregarSolicitudPautaComponent } from '../agregar-solicitud-pauta/agregar-solicitud-pauta.component';
import { ListaSolicitudesPautasComponent } from '../lista-solicitudes-pautas/lista-solicitudes-pautas.component';
import { ActualizarSolicitudPautaComponent } from '../actualizar-solicitud-pauta/actualizar-solicitud-pauta.component';
import { ActualizarPagoComponent } from '../actualizar-pago/actualizar-pago.component';
import { ListaPagosComponent } from '../lista-pagos/lista-pagos.component';
import { AgregarPagoComponent } from '../agregar-pago/agregar-pago.component';
import { DetalleSolicitudTarifasComponent } from '../detalle-solicitud-tarifas/detalle-solicitud-tarifas.component';
import { DetalleSolicitudPatrocinioComponent } from '../detalle-solicitud-patrocinio/detalle-solicitud-patrocinio.component';
import { DetalleSolicitudPautaComponent } from '../detalle-solicitud-pauta/detalle-solicitud-pauta.component';
import { AgregarSolicitudPatrocinioComponent } from '../agregar-solicitud-patrocinio/agregar-solicitud-patrocinio.component';
import { ListaSolicitudesPatrocinioComponent } from '../lista-solicitudes-patrocinio/lista-solicitudes-patrocinio.component';
import { ActualizarSolicitudPatrocinioComponent } from '../actualizar-solicitud-patrocinio/actualizar-solicitud-patrocinio.component';
import { AgregarSolicitudEntrevistaComponent } from '../agregar-solicitud-entrevista/agregar-solicitud-entrevista.component';
import { ListaSolicitudesEntrevistaComponent } from '../lista-solicitudes-entrevista/lista-solicitudes-entrevista.component';
import { ActualizarSolicitudEntrevistaComponent } from '../actualizar-solicitud-entrevista/actualizar-solicitud-entrevista.component';
import { DetalleSolicitudEntrevistaComponent } from '../detalle-solicitud-entrevista/detalle-solicitud-entrevista.component';
import { DetallePagoComponent } from '../detalle-pago/detalle-pago.component';
import { DetalleTurnoComponent } from '../detalle-turno/detalle-turno.component';
import { AgregarTurnoComponent } from '../agregar-turno/agregar-turno.component';
import { ActualizarTurnoComponent } from '../actualizar-turno/actualizar-turno.component';
import { ListaTurnosComponent } from '../lista-turnos/lista-turnos.component';
import { AgregarSalidaComponent } from '../agregar-salida/agregar-salida.component';
import { AgregarSolicitudEspacioComponent } from '../agregar-solicitud-espacio/agregar-solicitud-espacio.component';
import { ActualizarSolicitudEspacioComponent } from '../actualizar-solicitud-espacio/actualizar-solicitud-espacio.component';
import { DetalleSolicitudEspacioComponent } from '../detalle-solicitud-espacio/detalle-solicitud-espacio.component';
import { ListaSolicitudesEspaciosComponent } from '../lista-solicitudes-espacios/lista-solicitudes-espacios.component';
import { ProgramacionWeekComponent } from '../programacion-week/programacion-week.component';
import { HomeSiteComponent } from 'app/home-site/home-site.component';


export const routes: Routes = [
  {
    path: 'usuario/registro',
    component: ResgistroUsuarioComponent,
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
     path: '',
    component: PageContainerComponent,
    children: [
      {
        path: '',
        component: HomeSiteComponent,
        outlet:'site'
      },
    ]
  },
  {
    path: 'sesion',
    component: SideBarMenuComponent,
    canActivate: [ AuthorizatedGuard ],
    children: [
      {
        path: '',
        component: homesesionComponent,
        outlet:'homesesion'
      },
      {
        path: 'usuarios',
        component: ListaUsuarioComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet:'homesesion'
      },
      {
        path: 'registrousuario',
        component: ResgistroUsuarioComponent,
        outlet: 'homesesion'
      },
      {
        path: 'listaprogramas',
        component: ListaProgramasComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet:'homesesion',
      }, 
      {
        path: 'listasolicitudespacio',
        component: ListaSolicitudesEspaciosComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet:'homesesion',
      }, 
      {
        path: 'detalleusuario/:id',
        component: DetalleUsuarioComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },
      {
        path: 'detalleprograma/:id',
        component: DetalleProgramaComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },
      {
        path: 'actualizarusuario/:id',
        component: ActualizarUsuarioComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },
      {
        path: 'actualizarprograma/:id',
        component: ActualizarProgramaComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },
      {
        path: 'crearprograma',
        component: AgregarProgramaComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },  
      {
        path: 'solicitartarifas',
        component: AgregarSolicitudTarifasComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      }, 
      {
        path: 'solicitarpatrocinio',
        component: AgregarSolicitudPatrocinioComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },  
      {
        path: 'solicitudestarifas',
        component: ListaSolicitudesTarifasComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },  
      {
        path: 'solicitudespatrocinio',
        component: ListaSolicitudesPatrocinioComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      }, 
      {
        path: 'actualizarst/:id',
        component: ActualizarSolicitudTarifasComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },  
      {
        path: 'detallest/:id',
        component: DetalleSolicitudTarifasComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },  
      {
        path: 'detallesp/:id',
        component: DetalleSolicitudPautaComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      }, 
      {
        path: 'detallespa/:id',
        component: DetalleSolicitudPatrocinioComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      }, 
      {
        path: 'detallesolesp/:id',
        component: DetalleSolicitudEspacioComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      }, 
      {
        path: 'detallese/:id',
        component: DetalleSolicitudEntrevistaComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      }, 
      {
        path: 'solicitarpauta',
        component: AgregarSolicitudPautaComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },  
      {
        path: 'solicitarentrevista',
        component: AgregarSolicitudEntrevistaComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },  
      {
        path: 'solicitarespacio',
        component: AgregarSolicitudEspacioComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },  
      {
        path: 'solicitudespautas',
        component: ListaSolicitudesPautasComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },  
      {
        path: 'solicitudesentrevistas',
        component: ListaSolicitudesEntrevistaComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },  
      {
        path: 'actualizarsp/:id',
        component: ActualizarSolicitudPautaComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },
      {
        path: 'actualizarse/:id',
        component: ActualizarSolicitudEntrevistaComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },
      {
        path: 'actualizarsolesp/:id',
        component: ActualizarSolicitudEspacioComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },
      {
        path: 'actualizarspa/:id',
        component: ActualizarSolicitudPatrocinioComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },
      {
        path: 'registropago',
        component: AgregarPagoComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      }, 
      {
        path: 'detallepago/:id',
        component: DetallePagoComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },  
      {
        path: 'pagos',
        component: ListaPagosComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },  
      {
        path: 'actualizarpago/:id',
        component: ActualizarPagoComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },  
      {
        path: 'listaturnos',
        component: ListaTurnosComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },  
      {
        path: 'actualizarturno/:id',
        component: ActualizarTurnoComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },
       {
        path: 'detalleturno/:id',
        component: DetalleTurnoComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      }, 
       {
        path: 'registroturno',
        component: AgregarTurnoComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },
      {
        path: 'marcarsalida',
        component: AgregarSalidaComponent,
        canActivate: [ AuthorizatedGuard ],
        outlet: 'homesesion'
      },       
    ]
  },     
];

export const routes2: Routes = [
  {
   path: 'site',
   component: PageContainerComponent,
   children: [
     {
       path: '',
       component: HomeSiteComponent,
       outlet:'site'
     },
     {
       path: 'home',
       component: HomeSiteComponent,
       outlet:'site'
     },
     {
       path: 'programacion',
       component: ProgramacionComponent,
       outlet:'site'
     }, 
   ]
 },
];


@NgModule({
  imports: [
    CommonModule,  RouterModule.forRoot(routes, { useHash: true }), RouterModule.forChild(routes2)
  ],
  declarations: [],
  exports: [RouterModule],
  providers:[AuthorizatedGuard]
})

export class AppRoutingModule { }
import { Component, OnInit } from '@angular/core';
import { StorageService } from '../core';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { SolicitudPautasService } from '../http-services/solicitud-pauta.service';
import { SolicitudPauta } from '../modelos/modelo-solicitud-pauta';

@Component({
  selector: 'app-detalle-solicitud-pauta',
  templateUrl: './detalle-solicitud-pauta.component.html',
  styleUrls: ['./detalle-solicitud-pauta.component.css','../app.component.scss'],
  providers:[SolicitudPautasService, StorageService]
})

export class DetalleSolicitudPautaComponent implements OnInit {
  solicitud: SolicitudPauta;
  loading = false;
  sub:any;
  role: string;

  constructor(
    private storageService: StorageService,
    public route: ActivatedRoute,
    public router:Router,
    public solicituPautaService: SolicitudPautasService,
    private sideBar: SideBarMenuComponent
  ) { }

  ngOnInit() {
    this.sideBar.setTitle('Detalle de Solicitud de Pauta');
    this.loading = true;
    console.log(this.route.url);
    this.role = this.storageService.getCurrentUser().role;
    this.sub = this.route.params.subscribe(params => {     
      const solicitud = params['id'];
      this.solicituPautaService
        .getSolicitudPauta(solicitud, this.storageService.getCurrentToken())
        .subscribe(c => {
          this.loading = false;
          this.solicitud = c;
        });
    }, error => {
      this.loading = false;
      console.log(error);
    }
  );
  }

  public volver() : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['solicitudespautas']}}]);
  }

  public update(id: string) : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['actualizarsp', id]}}]);
  }

}




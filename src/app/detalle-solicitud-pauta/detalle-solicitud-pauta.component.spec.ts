import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleSolicitudPautaComponent } from './detalle-solicitud-pauta.component';

describe('DetalleSolicitudPautaComponent', () => {
  let component: DetalleSolicitudPautaComponent;
  let fixture: ComponentFixture<DetalleSolicitudPautaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleSolicitudPautaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleSolicitudPautaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

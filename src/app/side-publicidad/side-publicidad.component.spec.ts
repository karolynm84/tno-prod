import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidePublicidadComponent } from './side-publicidad.component';

describe('SidePublicidadComponent', () => {
  let component: SidePublicidadComponent;
  let fixture: ComponentFixture<SidePublicidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidePublicidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidePublicidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

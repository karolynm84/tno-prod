import { Component, OnInit } from '@angular/core';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation, NgxGalleryAction } from 'ngx-gallery';

@Component({
  selector: 'app-side-publicidad',
  templateUrl: './side-publicidad.component.html',
  styleUrls: ['./side-publicidad.component.css']
})
export class SidePublicidadComponent implements OnInit {
  innerWidth: number;
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  galleryActions:NgxGalleryAction;
  action = 
  new NgxGalleryAction({ disabled: false, icon: 'fa fa-remove', 
  titleText: 'Pro', onClick: () => 
  { this.actionButtonClicked } });
  galleryAction = <NgxGalleryAction>{
    onClick: this.actionButtonClicked
};
actionButtonClicked(i) {
  console.log(i);
  let attachment = this.galleryImages[i];
  this.galleryAction.onClick
}

  constructor() { }

  ngOnInit(): void {
    //this.galleryActions.push(this.galleryAction);
    this.innerWidth = window.innerWidth;
    this.galleryOptions = [
      {
          width: '100%',
          height: '220px',
          thumbnails: false,
          preview: false,
          imageAutoPlayPauseOnHover: true,
          imageAnimation: NgxGalleryAnimation.Fade,
          imageAutoPlay: true,
          imageAutoPlayInterval: 10000,
          actions: [this.action],
          imageArrowsAutoHide:true
          
         // actions: this.
      },
      // max-width 800
      {
          breakpoint: 800,
          width: '100%',
          height: '220px',
          imagePercent: 100
          
      },
      // max-width 400
      {
          breakpoint: 400,
          preview: false
      }
  ];

  if (this.innerWidth <=640) {
    this.galleryImages = [
        {
            small: 'assets/images/publicidad/protecnoresponsive.jpg',
            medium: 'assets/images/publicidad/protecnoresponsive.jpg',
            big: 'assets/images/publicidad/protecnoresponsive.jpg',
            url:'www.protecnonetworks.com'
        }, 
        {
            small: 'assets/images/publicidad/posadaamadamia_responsive.jpg',
            medium: 'assets/images/publicidad/posadaamadamia_responsive.jpg',
            big: 'assets/images/publicidad/posadaamadamia_responsive.jpg',
            url:'www.facebook.com/posadaamadamia'
        },
      ];
  }
  else {
    this.galleryImages = [
       {
           small: 'assets/images/publicidad/protecno.jpg',
           medium: 'assets/images/publicidad/protecno.jpg',
           big: 'assets/images/publicidad/protecno.jpg',
           url:'www.protecnonetworks.com'
       }, 
       {
           small: 'assets/images/publicidad/posadaamadamia.jpg',
           medium: 'assets/images/publicidad/posadaamadamia.jpg',
           big: 'assets/images/publicidad/posadaamadamia.jpg',
           url:'www.facebook.com/posadaamadamia'
       },
     ];

  }  
}

}

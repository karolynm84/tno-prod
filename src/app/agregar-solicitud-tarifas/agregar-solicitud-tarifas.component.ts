import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormsValidationService } from '../shared-components/forms-validation.service';
import { StorageService } from '../core';
import { User } from '../modelos/modelo-user';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { SolicitudTarifasService } from '../http-services/solicitud-tarifas.service';
import { SolicitudTarifa } from '../modelos/modelo-solicitud-tarifa';
import { formSolicitudTarifas } from '../forms/form-solicitud-tarifas';

@Component({
  selector: 'app-agregar-solicitud-tarifas',
  templateUrl: './agregar-solicitud-tarifas.component.html',
  styleUrls: ['./agregar-solicitud-tarifas.component.css', '../app.component.scss'],
  providers:[ FormsValidationService, SolicitudTarifasService, StorageService ]
})

export class AgregarSolicitudTarifasComponent {
    form_solicitud_tarifas: FormGroup;
    solicitud_tarifas: SolicitudTarifa;
    errMsg: any;
    loading = false;
    currentUser: User;    

  constructor(
    public router:Router,
        public formBuilder: FormBuilder,
        public toastr: ToastrService,
        public validation: FormsValidationService,
        public storageService: StorageService,
        public sideBar: SideBarMenuComponent,
        public solicitudTarifasService: SolicitudTarifasService
  ) { 
    this.sideBar.setTitle('Enviío de Solicitud de Tarifas');
    this.currentUser = this.storageService.getCurrentUser();   
    this.form_solicitud_tarifas = this.formBuilder.group({
        cliente: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],
        status: ['Pendiente'],        
    });
  }

   onSubmit({ value,  valid }: { value: formSolicitudTarifas, valid: boolean }) { 
       if(!this.form_solicitud_tarifas.valid){
            this.toastr.error('Problema en el formulario!', 'Error!');
        }else{ 
            this.solicitud_tarifas = new SolicitudTarifa();
            this.solicitud_tarifas.cliente = value.cliente;
            this.solicitud_tarifas.status = 'pendiente';
            this.solicitud_tarifas.user_id = this.currentUser.id.toString();
            console.log(this.solicitud_tarifas);
            this.loading = true;
            this.solicitudTarifasService
            .createSolicitudTarifas(this.solicitud_tarifas, this.storageService.getCurrentToken())
            .subscribe(
                (res) => {                  
                  console.log(res);
                  this.solicitudTarifasService
                  .sendEmailSolicitud(this.storageService.getCurrentToken())
                  .subscribe(
                      (resEmail) => {
                        this.loading = false;
                        console.log(resEmail);
                        this.toastr.success('Solicitud de tarifas enviada con éxito. Gracias por su tiempo.');
                      this.toastr.warning('Recibirá un correo con la información.');                        
                          this.volver();   
                      },errorEmail=>{
                        this.loading = false;
                        console.log(errorEmail);
                        this.toastr.error('Algo salió mal.'); 
                      }
                  );                              
                },error=>{
                    this.loading = false;     
                    console.log(error);        
                    this.toastr.error('Algo salió mal.');     
                }
            );
        }
    }    

    public volver() : void {
      this.router.navigate(['sesion', {outlets: {homesesion: ['sesion']}}]);
    }
}



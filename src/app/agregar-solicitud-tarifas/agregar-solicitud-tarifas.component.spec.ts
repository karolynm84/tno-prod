import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarSolicitudTarifasComponent } from './agregar-solicitud-tarifas.component';

describe('AgregarSolicitudTarifasComponent', () => {
  let component: AgregarSolicitudTarifasComponent;
  let fixture: ComponentFixture<AgregarSolicitudTarifasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarSolicitudTarifasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarSolicitudTarifasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component} from "@angular/core";
import { User } from "../modelos/modelo-user";
import { StorageService, AuthenticationService } from "../core";
import { Router } from "@angular/router";
import { SideBarMenuComponent } from "../side-bar-menu/side-bar-menu.component";
import { TurnoService } from "../http-services/turno.service";
import { Turno } from "../modelos/modelo-turno";



@Component({
  selector: 'app-home-session',
  templateUrl: './home-sesion.component.html',
  styleUrls:['./home-sesion.component.css'],
  providers: [TurnoService]
})
export class homesesionComponent {
  public user: User;
  loading = false;
  public turnos: Turno[];
  public operando: boolean = false;

  constructor(
    private storageService: StorageService,
    private authenticationService: AuthenticationService,
    public router:Router,
    public sideBar: SideBarMenuComponent,
    public turnoService: TurnoService
  ) { 
    this.user = this.storageService.getCurrentUser();
    console.log("---usuario sesión--- ",this.user);
    if (this.user.role == 'Operador' || this.user.role == 'Administrador' 
    || this.user.role == 'Otro Empleado' || this.user.role == 'Community Manager'
    || this.user.role == 'Social Media Manager') {
      this.operando=this.operadorEntro();   
    }
  }
  ngOnInit() {
    this.sideBar.setTitle('');    
  }
  public logout(): void{
    //this.storageService.removeCurrentSession();
    //this.router.navigate(['/login']);
    this.authenticationService.logout(this.storageService.getCurrentToken()).subscribe(
        response => {
          if(response) {
            this.storageService.logout();
          }
        },
        error=> {
          console.log(error);
        }
    );
  }

public operadorEntro (): boolean {
    var today = new Date();
    var todays;
    var dd = today.getDate();
    var yyyy = today.getFullYear();
    var mm = today.getMonth()+1; //January is 0!
    if (dd.toString().length<2) {
      if (mm.toString().length<2){
        todays =  yyyy.toString() + '-0' + mm.toString() + '-' + '0' + dd.toString();      
      }
      else {
        todays =  yyyy.toString() + '-' + mm.toString() + '-' + '0' + dd.toString();
      }
    }else {   
      if (mm.toString().length<2){
        todays =  yyyy.toString() + '-0' + mm.toString() + '-' + dd.toString();      
      }
      else {
        todays =  yyyy.toString() + '-' + mm.toString() + '-' + dd.toString();
      }
    }
    this.loading = true;
    this.turnoService
    .getTurnosCompletos(this.storageService.getCurrentToken())
    .subscribe(res => {
      this.loading = false;
      this.turnos = res;
      var id = this.user.id;
      var turnos = Array.from(this.turnos);
      /**
       * Obtengo los turnos del usuario.
       */
      var turnosUsuario = turnos.filter(s=>s.empleado_id == id);
      if (turnosUsuario.length == 0) {
        console.log("No tiene turnos");
        this.storageService.setCurrentTurno(null);
        this.sideBar.hayTurno = false;
        return false;
      }else {
        /**
         * Obtengo los turnos de hoy;
         */
        console.log("Tiene turnos en su historial");
        /*var turnosHoy = turnosUsuario.filter(function (turno) {   
          return turno.fecha === todays;
        });*/

        var turnosHoy = turnosUsuario.
        filter(s=> s.fecha.toString() == todays.toString());
        if (turnosHoy.length == 0){
          console.log("No tiene turnos hoy");
          this.storageService.setCurrentTurno(null);
          this.sideBar.hayTurno = false;
          return false;
        }else {
          /**
           * Obtengo los turnos de hoy sin marcar salida.
           */
          console.log("Tiene turnos hoy");
          var turnosAbiertos = turnosHoy.filter(function (turno) {   
            return turno.hora_entrada === turno.hora_salida;
          });
          if (turnosAbiertos.length == 0) {
            console.log("No tiene turnos en marcha");
            this.storageService.setCurrentTurno(null);
            this.sideBar.hayTurno = false;
            return false;
          } else {
            console.log("Tiene turno en marcha");
            var max = turnosAbiertos.reduce((a, b) => {
              return a.id > b.id ? a : b;
            } );
            console.log(max);
            this.storageService.setCurrentTurno(max);
            this.sideBar.hayTurno = true;
            return true;
          }          
        }
      }
    },
    error => {
      console.log(error);
      this.sideBar.hayTurno = false;
      return false;
    }
  );
    return false;
  }
}
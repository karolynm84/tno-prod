import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { homesesionComponent } from './home-sesion.component';

describe('homesesionComponent', () => {
  let component: homesesionComponent;
  let fixture: ComponentFixture<homesesionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ homesesionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(homesesionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

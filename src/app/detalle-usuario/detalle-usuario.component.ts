import { Component, OnInit } from '@angular/core';
import { User } from '../modelos/modelo-user';
import { StorageService } from '../core';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { UserService } from '../http-services/user.service';

@Component({
  selector: 'app-detalle-usuario',
  templateUrl: './detalle-usuario.component.html',
  styleUrls: ['./detalle-usuario.component.css','../app.component.scss'],
  providers: [UserService]
})
export class DetalleUsuarioComponent implements OnInit {
  user: User;
  loading=false;
  private sub: any;
  role: string;

  constructor(
    private storageService: StorageService,
    private userService: UserService,
    public route: ActivatedRoute,
    public router:Router,
    private sideBar: SideBarMenuComponent
  ) { }

  ngOnInit() {
    this.loading = false;
    this.sideBar.setTitle('Detalle de Usuario');   
    this.loading = true;
    console.log(this.route.url);
    this.role = this.storageService.getCurrentUser().role;
    this.sub = this.route.params.subscribe(params => {     
      const user = params['id'];
      this.userService
        .getUser(user, this.storageService.getCurrentToken())
        .subscribe(c => {
          this.loading = false;
          this.user = c;
        });
    }, error => {
      this.loading = false;
      console.log(error);
    }
  );
  }

  public volver() : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['usuarios']}}]);
  }

  public update(id:string) : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['actualizarusuario', id]}}]);
  }

}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '../../../node_modules/@angular/forms';
import { Router, ActivatedRoute } from '../../../node_modules/@angular/router';
import { StorageService, ProgramaService } from '../core';
import { ToastrService } from '../../../node_modules/ngx-toastr';
import { FormsValidationService } from '../shared-components/forms-validation.service';
import { DataFormatService } from '../shared-services/data-format.service';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { User } from '../modelos/modelo-user';
import { SolicitudPautasService } from '../http-services/solicitud-pauta.service';
import { Programa } from '../modelos/modelo-programa';
import { SolicitudPauta } from '../modelos/modelo-solicitud-pauta';
import { formSolicitudPauta } from '../forms/form-solicitud-pauta';

@Component({
  selector: 'app-actualizar-solicitud-pauta',
  templateUrl: './actualizar-solicitud-pauta.component.html',
  styleUrls: ['./actualizar-solicitud-pauta.component.css', '../app.component.scss'],
  providers: [ FormsValidationService, SolicitudPautasService ,
    StorageService, DataFormatService, ProgramaService ]
})

export class ActualizarSolicitudPautaComponent implements OnInit {

  statusList = [
    'pendiente',
    'enviada',
    'eliminada'
  ]

  form_agregar_solicitud: FormGroup;
  pauta_nueva: SolicitudPauta;
  errMsg: any;
  loading = false;
  currentUser: User;    
  programas: Programa[] = [];
  role:string;
  formSolicitudPauta: formSolicitudPauta;
  sub: any;
  
  constructor (
    public router: Router,
    public formBuilder: FormBuilder,
    public solicitudPautaService: SolicitudPautasService,
    public toastr: ToastrService,
    public validation: FormsValidationService,
    public storageService: StorageService,
    public route: ActivatedRoute,
    public dataFromatService: DataFormatService,
    public sideBar: SideBarMenuComponent,
    public programaService : ProgramaService
  ) {}

  ngOnInit() {
    this.sideBar.setTitle('Actualizar Solicitud de Pauta de Grabación');
    this.currentUser = this.storageService.getCurrentUser();
    this.role = this.currentUser.role;
    this.loadProgramas();        
    this.form_agregar_solicitud = this.formBuilder.group({
      id:[null,],
      programa_id: ['', ],
      status: ['Pendiente', Validators.compose([Validators.maxLength(60)])],
      hora_inicio: ['', Validators.compose([Validators.required, Validators.maxLength(50), this.validation.notNullValidator])],
      hora_fin: ['', Validators.compose([Validators.required, Validators.maxLength(50), this.validation.notNullValidator])],
      user_id: [null],
      fecha:['',  Validators.compose([Validators.required])],
    }, {validator: Validators.compose([
        this.validation.dateLessThan('hora_inicio', 'hora_fin', { 'hora_inicio': true })
    ])}
);
    this.sub = this.route.params.subscribe(params => {     
      const solicitud = params['id'];
      this.loading = true;
      this.solicitudPautaService
        .getSolicitudPauta(solicitud, this.storageService.getCurrentToken())
        .subscribe(c => {
          this.loading = false;
          this.pauta_nueva = c;
          this.pauta_nueva.status = c.status;
          let form = this.formSolicitudPauta;            
          form = {
            "fecha" : this.pauta_nueva.fecha,
            "status" : this.pauta_nueva.status,
            "id" : this.pauta_nueva.id.toString(),
            "hora_inicio" : this.pauta_nueva.hora_inicio,
            "hora_fin" : this.pauta_nueva.hora_fin,
            "programa_id" : this.pauta_nueva.programa_id,
            "user_id" : this.currentUser.id.toString()
          }         
         this.form_agregar_solicitud.setValue(form);        
        });
    }, error => {
      this.loading = false;
      console.log(error);
    }
  );
  }

  /**
   * When user press submit button.
   */
  onSubmit({ value, valid }: { value: formSolicitudPauta, valid: boolean }) { 
    if(!this.form_agregar_solicitud.valid){
        this.toastr.error('Debe llenar todos los campos obligatorios!', 'Error!');
    }else {
      this.loading = true;            
      this.solicitudPautaService
      .updateSolicitudPauta(value, this.storageService.getCurrentToken())
      .subscribe(
          (res) => {
            this.loading = false;  
            console.log(res);    
            this.toastr.success('Solicitud de Pauta actualizada con éxito!');
            this.router.navigate(['sesion', {outlets: {homesesion: ['solicitudespautas']}}]);                                 
          },error=>{
            this.loading = false;           
            console.log(error);             
            this.toastr.error('Algo salió mal. Comunicarse con soporte.'); 
          }
       );          
    } 
  }

  public volver() : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['solicitudespautas']}}]);
  }

/**
* Loads Cortes into a SelectList.
*/
public loadProgramas() {
console.info("---loadProgramas()---");
this.programaService.getProgramas().subscribe
(programas => { 
    this.programas = programas;
});
}

}

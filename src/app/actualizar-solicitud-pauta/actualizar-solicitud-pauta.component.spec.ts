import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarSolicitudPautaComponent } from './actualizar-solicitud-pauta.component';

describe('ActualizarSolicitudPautaComponent', () => {
  let component: ActualizarSolicitudPautaComponent;
  let fixture: ComponentFixture<ActualizarSolicitudPautaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarSolicitudPautaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarSolicitudPautaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

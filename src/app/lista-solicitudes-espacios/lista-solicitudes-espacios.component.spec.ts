import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaSolicitudesEspaciosComponent } from './lista-solicitudes-espacios.component';

describe('ListaSolicitudesEspaciosComponent', () => {
  let component: ListaSolicitudesEspaciosComponent;
  let fixture: ComponentFixture<ListaSolicitudesEspaciosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaSolicitudesEspaciosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaSolicitudesEspaciosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

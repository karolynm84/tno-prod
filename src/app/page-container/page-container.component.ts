import { Component, OnInit } from '@angular/core';
import { MenuOpenedService } from '../menu-opened.service';
import { DomSanitizer  } from '@angular/platform-browser';


@Component({
  selector: 'app-page-container',
  templateUrl: './page-container.component.html',
  styleUrls: ['./page-container.component.css', '../app.component.scss']
})
export class PageContainerComponent implements OnInit {
  _isOpened: boolean;
  innerWidth: any;
  style: string;

  constructor(private data: MenuOpenedService, private sanitizer: DomSanitizer) { }
 

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    this.data._isOpened.subscribe(menuIsOpened => {      
      this._isOpened = menuIsOpened;
    });
  }

  getSideBarStyle() {
    if(this._isOpened) {
      if (this.innerWidth >= 1200) {
        this.style = 'margin-left:-25px;  width:16%;';
      }
      else {
        this.style = 'margin-left:-10px; width:16%;';
      }
      return this.sanitizer.bypassSecurityTrustStyle(this.style);
     
    } else {
      this.style = '';
      this.sanitizer.bypassSecurityTrustStyle(this.style);
     
    }
  }

  getCol11Style() {
    if(this._isOpened) {
      if (this.innerWidth >= 1200) {
        this.style = ' margin-left:-7%;';
      }
      else {
        this.style = ' margin-left:-8%;';
      }
      return this.sanitizer.bypassSecurityTrustStyle(this.style);
     
    } 
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuOpenedService } from '../menu-opened.service';
import { StorageService, AuthenticationService } from '../core';
import { DataSharingService } from '../shared-services/data-sharing.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
  providers: [StorageService, AuthenticationService]
})
export class FooterComponent implements OnInit {
  isUserLoggedIn: boolean = false;
  private localStorageService;

  constructor(
    private router: Router,
    private data: MenuOpenedService, 
    private storageService: StorageService,
    private authenticationService: AuthenticationService,
    private dataSharingService: DataSharingService
  ) {
      this.localStorageService = localStorage;
      this.dataSharingService.isUserLoggedIn.subscribe( value => {
      this.isUserLoggedIn = value;
      console.log("--constructor--");
      console.log(this.isUserLoggedIn);
      console.log(this.localStorageService.getItem('currentUser'));
  });
   }

  ngOnInit() {
    if (this.localStorageService.getItem('currentUser')=='null' || this.localStorageService.getItem('currentUser')==null){
      console.info("---not logged---");     
      this.isUserLoggedIn=false;
    }
    else {
      console.log("---logged---");
       this.dataSharingService.isUserLoggedIn.next(true);
      this.isUserLoggedIn=true;
    }
  }

  public micuenta():void {
    console.log("---micuenta()---");
    this.router.navigate(['/sesion']);
  }

  public login(): void {
    this.dataSharingService.isUserLoggedIn.next(true);
    this.router.navigate(['/login']);    
  }

}

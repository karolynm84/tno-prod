import { Component, OnInit } from '@angular/core';
import { MenuOpenedService } from '../menu-opened.service';
import { DomSanitizer  } from '@angular/platform-browser';
import { StorageService, AuthenticationService } from '../core';
import { DataSharingService } from '../shared-services/data-sharing.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css','../app.component.scss'],
  providers: [StorageService, AuthenticationService]
})

export class SideBarComponent implements OnInit {

  _opened: boolean = false;
  isUserLoggedIn: boolean = false;
  private localStorageService;
 
  _toggleSidebar() {
    this._opened = !this._opened;
    this.data.openClose(this._opened);
  }

  constructor(
    private router: Router,
    private data: MenuOpenedService, 
    private sanitizer: DomSanitizer,
    private dataSharingService: DataSharingService
  ) {
      this.localStorageService = localStorage;
      this.dataSharingService.isUserLoggedIn.subscribe( value => {
      this.isUserLoggedIn = value;
      console.log("--constructor--");
      console.log(this.isUserLoggedIn);
      console.log(this.localStorageService.getItem('currentUser'));
  });
   }

  ngOnInit() {
    this.data._isOpened.subscribe(menuIsOpened => this._opened = menuIsOpened);
    console.log(this.localStorageService.getItem('currentUser'));
    if (this.localStorageService.getItem('currentUser')=='null' || this.localStorageService.getItem('currentUser')==null){
      console.info("---not logged---");     
      this.isUserLoggedIn=false;
    }
    else {
      console.log("---logged---");
       this.dataSharingService.isUserLoggedIn.next(true);
      this.isUserLoggedIn=true;
    }
  }

  public micuenta():void {
    console.log("---micuenta()---");
    this.router.navigate(['/sesion']);
  }

  public programacion():void {
    console.log("---programacion()---");
    this.router.navigateByUrl('site/(site:programacion)');
  }

  getHamburguerStyle() {
    if(this._opened) {
      const style =
      'margin-left: 185% !important;';
      return this.sanitizer.bypassSecurityTrustStyle(style);
     
    } else {
      const style = '';
      this.sanitizer.bypassSecurityTrustStyle(style);     
    }
  }

  public login(): void {
    this.dataSharingService.isUserLoggedIn.next(true);
    this.router.navigate(['/login']);    
  }
}

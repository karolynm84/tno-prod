import { Component, OnInit } from '@angular/core';
import { StorageService } from '../core';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { SolicitudEspacioService } from '../http-services/solicitud-espacios.service';
import { SolicitudEspacio } from '../modelos/modelo-solicitud-espacio';
import { UserService } from 'app/http-services/user.service';
import { User } from 'app/modelos/modelo-user';

@Component({
  selector: 'app-detalle-solicitud-espacio',
  templateUrl: './detalle-solicitud-espacio.component.html',
  styleUrls: ['./detalle-solicitud-espacio.component.css','../app.component.scss'],
  providers:[SolicitudEspacioService, StorageService, UserService]
})

export class DetalleSolicitudEspacioComponent implements OnInit {
  solicitud: SolicitudEspacio;
  loading = false;
  sub:any;
  role:string;
  solicitante: User;

  constructor(
    private storageService: StorageService,
    public route: ActivatedRoute,
    public router:Router,
    public solicituEspacioService: SolicitudEspacioService,
    private sideBar: SideBarMenuComponent,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.sideBar.setTitle('Detalle de Solicitud de Espacio');
    this.loading = true;
    this.role = this.storageService.getCurrentUser().role;
    console.log(this.route.url);    
    this.sub = this.route.params.subscribe(params => {     
      const solicitud = params['id'];
      this.solicituEspacioService
        .getSolicitudEspacio(solicitud, this.storageService.getCurrentToken())
        .subscribe(c => {
          //this.loading = false;
          this.solicitud = c;
          console.log(this.solicitud);         
          this.userService
          .getUser(this.solicitud.user_id, 
            this.storageService.getCurrentToken())
            .subscribe(u=>{
              this.loading = false;
              this.solicitante = u;

          },error =>{
            this.loading = false;
            console.log(error);
          });
        });
    }, error => {
      this.loading = false;
      console.log(error);
    }
  );
  }

  public volver() : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['listasolicitudesespacio']}}]);
  }

  public update(id: string) : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['actualizarsolesp', id]}}]);
  }

}




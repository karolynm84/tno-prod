import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleSolicitudEspacioComponent } from './detalle-solicitud-espacio.component';

describe('DetalleSolicitudEspacioComponent', () => {
  let component: DetalleSolicitudEspacioComponent;
  let fixture: ComponentFixture<DetalleSolicitudEspacioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleSolicitudEspacioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleSolicitudEspacioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

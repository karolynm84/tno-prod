import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '../../../node_modules/@angular/forms';
import { Programa } from '../modelos/modelo-programa';
import { Router, ActivatedRoute } from '../../../node_modules/@angular/router';
import { ProgramaService, StorageService } from '../core';
import { ToastrService } from '../../../node_modules/ngx-toastr';
import { FormsValidationService } from '../shared-components/forms-validation.service';
import { formPrograma } from '../forms/form-programa';
import { DataFormatService } from '../shared-services/data-format.service';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { CorteProgramaService } from '../http-services/corte-programa.service';
import { CortePrograma } from '../modelos/modelo-corte-programa';
import { User } from '../modelos/modelo-user';

@Component({
  selector: 'app-actualizar-programa',
  templateUrl: './actualizar-programa.component.html',
  styleUrls: ['./actualizar-programa.component.css','../app.component.scss'],
  providers: [ FormsValidationService, ProgramaService, 
              StorageService, DataFormatService, CorteProgramaService ]
})

export class ActualizarProgramaComponent implements OnInit {
  hora = [
    '06:00',
    '07:00',
    '08:00',
    '09:00',
    '10:00',
    '11:00',
    '12:00',
    '13:00',
    '14:00',
    '15:00',
    '16:00',
    '17:00',
    '18:00',
    '19:00',
    '20:00',
    '21:00',
    '22:00',
    '23:00',
  ];

dias = [ 
    'lunes', 
    'martes', 
    'miércoles', 
    'jueves', 
    'viernes',  
    'sábado',  
    'domingo'
  ];

  clasificaciones = [ 
    'Todo Público', 
    'Supervisado', 
    'Adulto'
];

tipos = [
    'Cultural y Educativo',
    'Informativo',
    'Mixto',
    'Recreacional o Deportivo',
    'Opinión'
];

  form_agregar_programa: FormGroup;
  form_locutores: FormGroup
  programa_nuevo: Programa;
  errMsg: any;
  isChecked: boolean = false;
  diasCheck: string[] = [];
  loading = false;
  formPrograma: formPrograma;
  sub: any;
  cortes: CortePrograma[] = [];
  corte_:string;
  currentUser: User;
  programaEnabled: boolean;
  
  constructor (
    public router: Router,
        public formBuilder: FormBuilder,
        public programaService: ProgramaService,
        public toastr: ToastrService,
        public validation: FormsValidationService,
        public storageService: StorageService,
        public route: ActivatedRoute,
        public dataFromatService: DataFormatService,
        public sideBar: SideBarMenuComponent,
        private corteService: CorteProgramaService
      ) {}

  ngOnInit() {
      this.sideBar.setTitle('Actualizar Programa');
      this.currentUser = this.storageService.getCurrentUser();
      this.loading = true;
      this.loadCortes();      
      this.form_agregar_programa = this.formBuilder.group({
      id:[null],
      pro_nombre: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],
      pro_sinopsis: ['', Validators.compose([Validators.required, Validators.maxLength(500), this.validation.notNullValidator])],
      pro_hora_inicio: ['', Validators.compose([Validators.required, this.validation.notNullValidator])],
      pro_hora_fin: ['', Validators.compose([Validators.required, this.validation.notNullValidator])],
      pro_email: ['', Validators.compose([Validators.required, this.validation.emailValidator, Validators.maxLength(40)])],
      pro_dia_semana: [new FormArray([])/*,  this.validation.minSelectedCheckboxes(1)*/],
      pro_fecha_origen: ['',  Validators.compose([Validators.required])],
      pro_locutor_1: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],
      pro_locutor_2: ['', Validators.compose([Validators.maxLength(60)])],
      pro_locutor_3: ['', Validators.compose([Validators.maxLength(60)])],
      pro_locutor_4: ['', Validators.compose([Validators.maxLength(60)])],
      pro_facebook: ['', Validators.compose([Validators.maxLength(60)])],
      pro_twitter: ['', Validators.compose([ Validators.maxLength(60)])],
      pro_instagram: ['', Validators.compose([ Validators.maxLength(60)])],
      pro_snapchat: ['', Validators.compose([ Validators.maxLength(60)])],
      pro_productor: ['', Validators.compose([Validators.required, Validators.maxLength(60)])],
      habilitado: [false,],
      pro_corte: ['', Validators.compose([Validators.required, this.validation.notNullValidator])],
      pro_tipo: ['', Validators.compose([Validators.required, this.validation.notNullValidator])],
      pro_clasificacion: ['', Validators.compose([Validators.required, this.validation.notNullValidator])]
    });
    
    this.sub = this.route.params.subscribe(params => {     
      const programa = params['id'];
      this.programaService
        .getPrograma(programa, this.storageService.getCurrentToken())
        .subscribe(c => {
          this.loading = false;
          this.programa_nuevo = c;
          //console.log(c);
          this.corte_ = this.programa_nuevo.pro_corte;
          this.programaEnabled = this.programa_nuevo.habilitado;
          this.diasCheck = this.programa_nuevo.pro_dia_semana.split(',');
          let form = this.formPrograma;            
          form = {
            "id" : this.programa_nuevo.id,
            "pro_nombre" : this.programa_nuevo.pro_nombre,
            "pro_sinopsis" : this.programa_nuevo.pro_sinopsis,
            "pro_hora_inicio" : this.programa_nuevo.pro_hora_inicio,
            "pro_hora_fin" : this.programa_nuevo.pro_hora_fin,
            "pro_email" : this.programa_nuevo.pro_email,
            "pro_dia_semana" : this.programa_nuevo.pro_dia_semana,
            "pro_fecha_origen" : this.programa_nuevo.pro_fecha_origen,
            "pro_locutor_1" : this.programa_nuevo.pro_locutor_1,
            "pro_locutor_4" : this.programa_nuevo.pro_locutor_4,
            "pro_locutor_2" : this.programa_nuevo.pro_locutor_2,
            "pro_locutor_3" : this.programa_nuevo.pro_locutor_3,
            "pro_facebook" : this.programa_nuevo.pro_facebook,
            "pro_twitter" : this.programa_nuevo.pro_twitter,
            "pro_instagram" : this.programa_nuevo.pro_instagram,
            "pro_snapchat" : this.programa_nuevo.pro_snapchat, 
            "pro_productor" : this.programa_nuevo.pro_productor,
            "habilitado": this.programa_nuevo.habilitado,
            "pro_corte": this.programa_nuevo.pro_corte,
            "pro_clasificacion": this.programa_nuevo.pro_clasificacion,
            "pro_tipo": this.programa_nuevo.pro_tipo
          }      
          //console.log(form);  
          //console.log(this.programaEnabled); 
          //console.log(this.programa_nuevo.habilitado); 
         this.form_agregar_programa.setValue(form);        
        });
    }, error => {
      this.loading = false;
      console.log(error.status);
      console.log(error);
    }
  );
  }

  /**
   * When user press submit button.
   */
  onSubmit({ value, valid }: { value: formPrograma, valid: boolean }) { 
    if(!this.form_agregar_programa.valid){
        this.toastr.error('Debe llenar todos los campos obligatorios!', 'Error!');
    }else {
      value.pro_dia_semana = this.diasCheck.toString();
      this.loading = true;  
      value.habilitado = this.programaEnabled;  
      console.log(value);    
      this.programaService
      .updatePrograma(value, this.storageService.getCurrentToken())
      .subscribe(
          (res) => {
            this.loading = false;  
            console.log(res);    
            this.toastr.success('Programa actualizado con éxito!');
            this.router.navigate(['sesion', {outlets: {homesesion: ['listaprogramas']}}]);                                 
          },error=>{
            this.loading = false;           
            console.log(error);             
            this.toastr.error('Algo salió mal. Comunicarse con soporte.'); 
          }
       );          
    } 
  }

  onHabilitadoChange(isEnabled: boolean){
    if(isEnabled==true) {
      this.programaEnabled = true;
    } else {
        this.programaEnabled = false;
    }
    //console.log(this.programaEnabled);    
  }

  isHabilitado(){
    if (this.programaEnabled==true)
      return true;
    else
    return false;
  }

  onCheckChange(dia: string, isChecked: boolean) {
    if(isChecked) {
        this.diasCheck.push(dia);
    } else {
        this.diasCheck = this.diasCheck.filter(item => item !== dia);
    }
    //console.log(JSON.stringify(this.diasCheck));
  }

  isSelected(dia:string):boolean {
    if (this.diasCheck.includes(dia)) {
      return true;
    }
    else {
      return false;
    }
  }

   /**
   * Loads Cortes into a SelectList.
   */
  public loadCortes() {
    console.info("---loadCortes()---");
    this.corteService.getCortes().subscribe
    (cortes => { 
        this.cortes = cortes;
    });
  }

  public volver() : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['listaprogramas']}}]);
  }
}
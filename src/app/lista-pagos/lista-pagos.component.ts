import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { StorageService } from '../core';
import { User } from '../modelos/modelo-user';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { PagoService } from '../http-services/pago.service';
import { Pago } from '../modelos/modelo-pago';

@Component({
  selector: 'app-lista-pagos',
  templateUrl: './lista-pagos.component.html',
  styleUrls: ['./lista-pagos.component.css'],
  providers: [ PagoService ]
})
export class ListaPagosComponent implements OnInit {

  pagos: Pago[] = [];
  errMsg: any;
  clientid: number;
  loading = false;
  p: number = 1;
  currenUser: User;
  id_: string;
  role_:string;

   /**
   * PagosListComponent constructor.
   */
  constructor(
    public pagosService: PagoService,
    public toastr: ToastrService,
    public router:Router,
    public storageService: StorageService,
    public sideBar: SideBarMenuComponent
  ) { }

/**
 * Gets list of pagos from data base.
 */
  getPagos() {
    console.info("--getPagos()---");
    this.loading = true;
    this.currenUser = this.storageService.getCurrentUser();
    console.log(this.currenUser);
    this.role_ = this.currenUser.role;
    this.id_ = this.currenUser.id.toString();
    this.pagosService.getPagos(this.storageService.getCurrentToken())
      .subscribe(pagos => {  
        this.loading = false;      
        this.pagos = pagos;
        var id = this.currenUser.id.toString();
        if (this.currenUser.role!='Administrador'){
          var pagos = Array.from(pagos);
          this.pagos = pagos.filter(s=>s.user_id == id);
        }             
      },error=>{
        this.loading = false;
        console.error(error);             
        this.toastr.info('Algo salió mal.');     
      }
    );    
  }

 /**
  * Initialization of component.
  */
  ngOnInit(): void {
    this.sideBar.setTitle('Lista de Pagos');
    this.getPagos();
  }

  setClient(clientid: number) {
    this.clientid = clientid;
  }

  /**
   * Deletes a client from pagos list.
   * @param id client id.
   */
  delete(id: string) {
    console.info("---delete()---");
    this.pagosService.deletePago(id, this.storageService.getCurrentToken()).subscribe(
      (res) => {
        this.toastr.success('¡Pago eliminado!');
        this.pagos.forEach((t, i) => {
          if (t.id === id) { 
            this.pagos.splice(i, 1); 
          }
        });
      }
    );
  }

  public detallePago(id: string): void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['detallepago', id]}}]);
  }

  public actualizarPago(id: string): void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['actualizarpago', id]}}]);
  }
}


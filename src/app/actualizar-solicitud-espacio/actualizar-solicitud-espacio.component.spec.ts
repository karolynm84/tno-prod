import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarSolicitudEspacioComponent } from './actualizar-solicitud-espacio.component';

describe('ActualizarSolicitudEspacioComponent', () => {
  let component: ActualizarSolicitudEspacioComponent;
  let fixture: ComponentFixture<ActualizarSolicitudEspacioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarSolicitudEspacioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarSolicitudEspacioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

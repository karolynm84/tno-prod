import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarSolicitudTarifasComponent } from './actualizar-solicitud-tarifas.component';

describe('ActualizarSolicitudTarifasComponent', () => {
  let component: ActualizarSolicitudTarifasComponent;
  let fixture: ComponentFixture<ActualizarSolicitudTarifasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarSolicitudTarifasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarSolicitudTarifasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

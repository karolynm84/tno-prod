import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '../../../node_modules/@angular/forms';
import { Router, ActivatedRoute } from '../../../node_modules/@angular/router';
import { StorageService } from '../core';
import { ToastrService } from '../../../node_modules/ngx-toastr';
import { FormsValidationService } from '../shared-components/forms-validation.service';
import { DataFormatService } from '../shared-services/data-format.service';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { User } from '../modelos/modelo-user';
import { SolicitudTarifa } from '../modelos/modelo-solicitud-tarifa';
import { formSolicitudTarifas } from '../forms/form-solicitud-tarifas';
import { SolicitudTarifasService } from '../http-services/solicitud-tarifas.service';

@Component({
  selector: 'app-actualizar-solicitud-tarifas',
  templateUrl: './actualizar-solicitud-tarifas.component.html',
  styleUrls: ['./actualizar-solicitud-tarifas.component.css', '../app.component.scss'],
  providers: [ FormsValidationService,  SolicitudTarifasService,
    StorageService, DataFormatService ]
})
export class ActualizarSolicitudTarifasComponent implements OnInit {

  status = [
    'pendiente',
    'enviada',
    'eliminada'
  ]

  form_solicitud_tarifas: FormGroup;
  form_locutores: FormGroup
  solicitud_nueva: SolicitudTarifa;
  errMsg: any;
  isChecked: boolean = false;
  diasCheck: string[] = [];
  loading = false;
  formSolicitudTarifa: formSolicitudTarifas;
  sub: any;
  currentUser: User;
  
  constructor (
    public router: Router,
        public formBuilder: FormBuilder,
        public solicitudTarifaService: SolicitudTarifasService,
        public toastr: ToastrService,
        public validation: FormsValidationService,
        public storageService: StorageService,
        public route: ActivatedRoute,
        public dataFromatService: DataFormatService,
        public sideBar: SideBarMenuComponent
      ) {}

  ngOnInit() {
      this.sideBar.setTitle('Actualizar Solicitud de Tarifas');
      this.currentUser = this.storageService.getCurrentUser();
      this.loading = true;   
      this.form_solicitud_tarifas = this.formBuilder.group({
      id:[null],
      cliente: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],
      status: ['', Validators.compose([Validators.required, this.validation.notNullValidator])],
    });
    this.form_solicitud_tarifas.get('cliente').disable();
    this.loading = true;
    this.sub = this.route.params.subscribe(params => {     
      const solicitud = params['id'];
      this.solicitudTarifaService
        .getSolicitudTarifa(solicitud, this.storageService.getCurrentToken())
        .subscribe(c => {
          this.loading = false;
          this.solicitud_nueva = c;
          this.solicitud_nueva.status = c.status;
          let form = this.formSolicitudTarifa;            
          form = {
            "cliente" : this.solicitud_nueva.cliente,
            "status" : this.solicitud_nueva.status,
            "id" : this.solicitud_nueva.id
          }         
         this.form_solicitud_tarifas.setValue(form);        
        });
    }, error => {
      this.loading = false;
      console.log(error);
    }
  );
  }

  /**
   * When user press submit button.
   */
  onSubmit({ value, valid }: { value: formSolicitudTarifas, valid: boolean }) { 
    if(!this.form_solicitud_tarifas.valid){
        this.toastr.error('Debe llenar todos los campos obligatorios!', 'Error!');
    }else {
      this.loading = true;            
      this.solicitudTarifaService
      .updateSolicitudTarifa(value, this.storageService.getCurrentToken())
      .subscribe(
          (res) => {
            this.loading = false;  
            console.log(res);    
            this.toastr.success('SolicitudTarifa actualizado con éxito!');
            this.router.navigate(['sesion', {outlets: {homesesion: ['solicitudestarifas']}}]);                                 
          },error=>{
            this.loading = false;           
            console.log(error);             
            this.toastr.error('Algo salió mal. Comunicarse con soporte.'); 
          }
       );          
    } 
  }

  public volver() : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['solicitudestarifas']}}]);
  }
}

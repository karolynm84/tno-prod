import { Component, OnInit } from '@angular/core';
import { Programa } from '../modelos/modelo-Programa';
import { StorageService, ProgramaService } from '../core';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';

@Component({
  selector: 'app-detalle-Programa',
  templateUrl: './detalle-Programa.component.html',
  styleUrls: ['./detalle-Programa.component.css','../app.component.scss'],
  providers:[ProgramaService, StorageService]
})
export class DetalleProgramaComponent implements OnInit {
  programa: Programa;
  loading = false;
  private sub: any;
  role:string;

  constructor(
    private storageService: StorageService,
    public route: ActivatedRoute,
    public router:Router,
    public programaService: ProgramaService,
    private sideBar: SideBarMenuComponent
  ) { }

  ngOnInit() {
    this.sideBar.setTitle('Detalle de Programa');
    this.role = this.storageService.getCurrentUser().role;
    this.loading = true;
    this.sub = this.route.params.subscribe(params => {     
      const programa = params['id'];
      this.programaService
        .getPrograma(programa, this.storageService.getCurrentToken())
        .subscribe(c => {
          this.loading = false;
          this.programa = c;
        });
    }, error => {
      this.loading = false;
      console.log(error);
    }
  );
  }

  public volver() : void {
    this.router.navigate([['sesion', {outlets: {homesesion: ['listaprogramas']}}]]);
  }

  public update(id: string) : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['actualizarprograma', id]}}]);
  }

}


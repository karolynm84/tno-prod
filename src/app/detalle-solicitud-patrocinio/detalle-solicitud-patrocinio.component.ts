import { Component, OnInit } from '@angular/core';
import { StorageService } from '../core';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { SolicitudPatrocinio } from '../modelos/modelo-solicitud-patrocinio';
import { SolicitudPatrocinioService } from '../http-services/solicitud-patrocinio.service';

@Component({
  selector: 'app-detalle-solicitud-patrocinio',
  templateUrl: './detalle-solicitud-patrocinio.component.html',
  styleUrls: ['./detalle-solicitud-patrocinio.component.css','../app.component.scss'],
  providers:[SolicitudPatrocinioService, StorageService]
})

export class DetalleSolicitudPatrocinioComponent implements OnInit {
  solicitud: SolicitudPatrocinio;
  loading = false;
  sub:any;
  role: string;

  constructor(
    private storageService: StorageService,
    public route: ActivatedRoute,
    public router:Router,
    public solicituPatrocinioService: SolicitudPatrocinioService,
    private sideBar: SideBarMenuComponent
  ) { }

  ngOnInit() {
    this.sideBar.setTitle('Detalle de Solicitud de Patrocinio');
    this.loading = true;
    this.role = this.storageService.getCurrentUser().role;
    console.log(this.route.url);
    this.sub = this.route.params.subscribe(params => {     
      const solicitud = params['id'];
      this.solicituPatrocinioService
        .getSolicitudPatrocinio(solicitud, this.storageService.getCurrentToken())
        .subscribe(c => {
          this.loading = false;
          this.solicitud = c;
        });
    }, error => {
      this.loading = false;
      console.log(error);
    }
  );
  }

  public volver() : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['solicitudespatrocinio']}}]);
  }

  public update(id: string) : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['actualizarspa', id]}}]);
  }

}




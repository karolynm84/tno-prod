import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleSolicitudPatrocinioComponent } from './detalle-solicitud-patrocinio.component';

describe('DetalleSolicitudPatrocinioComponent', () => {
  let component: DetalleSolicitudPatrocinioComponent;
  let fixture: ComponentFixture<DetalleSolicitudPatrocinioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleSolicitudPatrocinioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleSolicitudPatrocinioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

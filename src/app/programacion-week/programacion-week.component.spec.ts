import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramacionWeekComponent } from './programacion-week.component';

describe('ProgramacionWeekComponent', () => {
  let component: ProgramacionWeekComponent;
  let fixture: ComponentFixture<ProgramacionWeekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramacionWeekComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramacionWeekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

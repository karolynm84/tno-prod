import { Component, OnInit, Input } from '@angular/core';
import { Programa } from 'app/modelos/modelo-programa';
import { StorageService } from 'app/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { DataSharingService } from 'app/shared-services/data-sharing.service';

@Component({
  selector: 'app-programacion-week',
  templateUrl: './programacion-week.component.html',
  styleUrls: ['./programacion-week.component.scss']
})
export class ProgramacionWeekComponent implements OnInit {

  programas: Programa[] = [];
  programasDia: Programa[] = [];
  loading = true;
  dia:string = "LUNES";
  
  constructor(
    public toastr: ToastrService,
    public router: Router,
    public storageService: StorageService,
    private dataSharingService: DataSharingService
  ) { 
    console.log("-- ProgramacionWeekComponent Constructor--");
    //this.loading = true;
    this.dataSharingService.programas$.subscribe(data => {
      this.loading = false;
      this.programas = data;
      this.programasDia = this.programas.filter(s => s.pro_dia_semana.includes('lunes'));
    }); 
   }

  ngOnInit() {
    console.log("--ProgramacionWeekComponent Init---");
  }
     

  getLista(dia:string){
    this.dia = dia.toUpperCase();
    console.log('--ProgramacionWeekComponent getLista()--');
    const result = this.programas.filter(s => s.pro_dia_semana.includes(dia));
    this.programasDia = result;  
  }
}

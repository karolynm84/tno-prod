import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleSolicitudEntrevistaComponent } from './detalle-solicitud-entrevista.component';

describe('DetalleSolicitudEntrevistaComponent', () => {
  let component: DetalleSolicitudEntrevistaComponent;
  let fixture: ComponentFixture<DetalleSolicitudEntrevistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleSolicitudEntrevistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleSolicitudEntrevistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

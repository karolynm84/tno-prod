import { Component, OnInit } from '@angular/core';
import { StorageService } from '../core';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';
import { SolicitudEntrevistaService } from '../http-services/solicitud-entrevista.service';
import { SolicitudEntrevista } from '../modelos/modelo-solicitud-entrevista';

@Component({
  selector: 'app-detalle-solicitud-entrevista',
  templateUrl: './detalle-solicitud-entrevista.component.html',
  styleUrls: ['./detalle-solicitud-entrevista.component.css','../app.component.scss'],
  providers:[SolicitudEntrevistaService, StorageService]
})

export class DetalleSolicitudEntrevistaComponent implements OnInit {
  solicitud: SolicitudEntrevista;
  loading = false;
  sub:any;
  role:string;

  constructor(
    private storageService: StorageService,
    public route: ActivatedRoute,
    public router:Router,
    public solicituEntrevistaService: SolicitudEntrevistaService,
    private sideBar: SideBarMenuComponent
  ) { }

  ngOnInit() {
    this.sideBar.setTitle('Detalle de Solicitud de Entrevista');
    this.loading = true;
    this.role = this.storageService.getCurrentUser().role;
    console.log(this.route.url);
    this.sub = this.route.params.subscribe(params => {     
      const solicitud = params['id'];
      this.solicituEntrevistaService
        .getSolicitudEntrevista(solicitud, this.storageService.getCurrentToken())
        .subscribe(c => {
          this.loading = false;
          this.solicitud = c;
        });
    }, error => {
      this.loading = false;
      console.log(error);
    }
  );
  }

  public volver() : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['solicitudesentrevistas']}}]);
  }

  public update(id: string) : void {
    this.router.navigate(['sesion', {outlets: {homesesion: ['actualizarse', id]}}]);
  }

}




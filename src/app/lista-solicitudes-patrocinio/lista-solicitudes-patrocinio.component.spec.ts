import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaSolicitudesPatrocinioComponent } from './lista-solicitudes-patrocinio.component';

describe('ListaSolicitudesPatrocinioComponent', () => {
  let component: ListaSolicitudesPatrocinioComponent;
  let fixture: ComponentFixture<ListaSolicitudesPatrocinioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaSolicitudesPatrocinioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaSolicitudesPatrocinioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

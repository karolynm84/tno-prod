import { Area } from "./modelo-area";
import { User } from "./modelo-user";

export class Turno {
    id:string;
    hora_entrada: string;
    hora_salida: string;
    fecha: string;
    area_id: number;
    empleado_id: number;
    actividad: string;
    area:Area;
    empleado:User
}
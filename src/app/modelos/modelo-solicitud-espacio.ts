export class SolicitudEspacio {
    public id: number;
    public demo: boolean;
    public user_id: number;
    public demo_link: string;
    public corte: string;
    public seg_objetivo: string;
    public dias: string;
    public experiencia_medios: boolean;
    public experiencia_medios_online: boolean;
    public vinculo_otros_medios: boolean;
    public comentarios: string;
    public tipo_experiencia: string;
    public conductor_1: string;
}
export class SolicitudPatrocinio {
    public id:string;
    public fecha: string;
    public lugar: string;
    public nombre_responsable: string;
    public nombre_productora: string;
    public objetivos_evento: string;
    public tipo_evento: string;
    public user_id: string;
    public status: string;
}
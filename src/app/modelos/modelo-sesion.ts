import { User } from "./modelo-user";

export class Sesion {
    public access_token: string;
    public user: User;
  }
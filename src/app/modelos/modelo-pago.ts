export class Pago {
    public referencia: string;
    public id: string;
    public programa_id: string;
    public status: string;
    public hora_transaccion: string;
    public fecha_transaccion: string;
    public concepto: string;
    public banco_origen: string;
    public banco_destino: string;
    public user_id: string;
    public created_at: string;
    public monto: number;
    public efectivo: boolean;
}
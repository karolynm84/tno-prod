export class SolicitudTarifa {
    public cliente: string;
    public id: string;
    public status: string;
    public user_id: string;
    public created_at: Date;
  }
export class User {
    public id: number;
    public name: string;
    public lastname: string;
    public email: string;
    public password?: string;
    public password_confirmation?: string;
    public role: string;
    public us_id: string;
    public c_locucion: string;
    public telephone: string;
    public birthdate: Date;
    public instagram : string;
    public twitter: string;
    public facebook: string;
    public pni: string;
    public bio: string;
    public enabled:boolean;
}
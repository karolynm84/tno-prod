export class Programa {
    id: string;
    pro_nombre: string;
    pro_sinopsis: string;
    pro_email: string;
    pro_tipo: string;
    pro_clasificacion: string;
    pro_twitter:string;
    pro_facebook:string;
    pro_instagram:string;
    pro_snapchat:string;
    pro_hora_inicio: string;    
    pro_hora_fin: string;
    pro_fecha_origen: string;
    pro_dia_semana: string;
    pro_locutor_1: string;
    pro_locutor_2: string;
    pro_locutor_3: string;
    pro_locutor_4: string;
    pro_productor: string;
    habilitado: boolean;
    fk_user_last_modifier: number;
    user_id: number;
    autorizado?:boolean;
    pro_corte: string;
}
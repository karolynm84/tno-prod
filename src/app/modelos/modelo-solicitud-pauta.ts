export class SolicitudPauta {
    public id: string;
    public fecha: string;
    public programa_id: string;
    public user_id: string;    
    public status: string;
    public hora_inicio: string;
    public hora_fin: string;
}
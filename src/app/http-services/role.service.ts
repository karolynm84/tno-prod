import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpClientHelper } from './definiciones';
import { Observable } from 'rxjs';
import { Role } from '../modelos/modelo-rol';
//import {Observable} from 'rxjs/Observable';
 
@Injectable()
export class RoleService {
    roles: Role[];

    private readonly URL = HttpClientHelper.produccionURL;

    constructor(
        private http:HttpClient
    ) {}
 
    // Uses http.get() to load data from a single API endpoint
    getRoles():  Observable<Role[]> {
        return this.http.get<Role[]>(this.URL + "roles",HttpClientHelper.httpOptions);                    
    }

    createUser(role) {
        console.log(role);
        let body = JSON.stringify(role);
        return this.http.post(this.URL + 'role', body, HttpClientHelper.httpOptions);
    }

    updateRole(role) {
        let body = JSON.stringify(role);
        return this.http.put('/api/roles/' + role.id, body, HttpClientHelper.httpOptions);
    }

    deleteRole(role) {
        return this.http.delete('/api/roles/' + role.id);
    }
}
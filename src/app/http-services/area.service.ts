import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpClientHelper } from './definiciones';
import { Observable } from 'rxjs';
import { Area } from '../modelos/modelo-area';
import { map, catchError } from 'rxjs/operators';
 
@Injectable()
export class AreaService {
    areas: Area[];

    private readonly URL = HttpClientHelper.produccionURL;

    constructor(
        private http:HttpClient
    ) {}
 
    // Uses http.get() to load data from a single API endpoint
    getAreas():  Observable<Area[]> {
        return this.http.get<Area[]>(this.URL + "areas", HttpClientHelper.httpOptions);                    
    }

    createArea(area, token) {
        console.log(area);
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        let body = JSON.stringify(area);
        return this.http.post(this.URL + 'area', body, httpOptions);
    }

    updateArea(area, token) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        let body = JSON.stringify(area);
        return this.http.put('/api/areas/' + area.id, body, httpOptions);
    }

    deleteArea(area, token) {
        return this.http.delete('/api/areas/' + area.id);
    }

     // Devuelve un statusArea específico
    // @params: id
    getStatusArea(id: string, token: string): Observable<any> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        const statusArea = this.http.get(this.URL + 'area/status/' + id, httpOptions)
        .pipe(map(result => result ? { areaOcupada: true } : null),
         catchError(() => null));
         const otro =this.http.get(this.URL + 'area/status/' + id, httpOptions);
         console.log(otro);
        return statusArea;
    }


    public getHeaders (token: string) : HttpHeaders {
        return new HttpHeaders({ 'Content-Type': 'application/json', 
        'Accept': 'application/json',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
        'Access-Control-Allow-Origin':'https://localhost:4200, https://www.tnoradio.com',
        'client-id':'1',
        'client_secret':'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh',
        'Authorization': 'Bearer ' + token
      })  
    }
}
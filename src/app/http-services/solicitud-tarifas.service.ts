import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpClientHelper } from './definiciones';
import { Observable } from 'rxjs';
import { SolicitudTarifa } from '../modelos/modelo-solicitud-tarifa';

@Injectable()
export class SolicitudTarifasService {

    solicitudestarifas: SolicitudTarifa[];

    private readonly URL = HttpClientHelper.produccionURL + "tarifas";

    constructor(
        private http:HttpClient
    ) {}
 
    // Uses http.get() to load data from a single API endpoint
    getSolicitudesTarifas(token):  Observable<SolicitudTarifa[]> {
      const httpOptions = {
        headers:  this.getHeaders(token)
      };
      return this.http.get<SolicitudTarifa[]>(this.URL+'/solicitudes', httpOptions);                    
    }

    createSolicitudTarifas(solicitudTarifas, token) {
        console.log(solicitudTarifas);
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        let body = JSON.stringify(solicitudTarifas);
        return this.http.post(this.URL + '/solicitud', body, httpOptions);
    }

    updateSolicitudTarifa(solicitudTarifas, token) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        let body = JSON.stringify(solicitudTarifas);
        return this.http.put(this.URL + '/solicitud/' + solicitudTarifas.id, body, httpOptions);
    }

    deleteSolicitudTarifa(solicitudTarifas, token) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.delete(this.URL + '/solicitud/' + solicitudTarifas, httpOptions);
    }
     // Devuelve una solicitudTarifas específica
    // @params: id
    getSolicitudTarifa(id: string, token: string): Observable<SolicitudTarifa> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        console.log(this.URL + '/solicitud/' + id);
    const solicitudTarifas = this.http.get<SolicitudTarifa>(this.URL + '/solicitud/' + id, httpOptions);
    return solicitudTarifas;
    }

    sendEmailSolicitud(token) {
      //const httpOptions = {
        //  headers:  this.getHeaders(token)
      //};
      console.log(this.URL + '/solicitud/' + 'sendemailsolicitud');
      return this.http.get<any>(this.URL + '/solicitud/' + 'sendemailsolicitud', HttpClientHelper.httpOptions);                      
  }

    public getHeaders (token: string) : HttpHeaders {
        return new HttpHeaders({ 'Content-Type': 'application/json', 
        'Accept': 'application/json',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
        'Access-Control-Allow-Origin':'https://localhost:4200, https://www.tnoradio.com',
        'client-id':'1',
        'client_secret':'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh',
        'Authorization': 'Bearer ' + token
      })  
    }
}
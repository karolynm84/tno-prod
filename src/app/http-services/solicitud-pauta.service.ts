import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpClientHelper } from './definiciones';
import { Observable } from 'rxjs';
import { SolicitudPauta } from '../modelos/modelo-solicitud-pauta';

@Injectable()
export class SolicitudPautasService {

    solicitudespautas: SolicitudPauta[];

    private readonly URL = HttpClientHelper.produccionURL + "pautas";

    constructor(
        private http:HttpClient
    ) {}
 
    // Uses http.get() to load data from a single API endpoint
    getSolicitudesPautas(token):  Observable<SolicitudPauta[]> {
      const httpOptions = {
        headers:  this.getHeaders(token)
      };
      return this.http.get<SolicitudPauta[]>(this.URL+'/solicitudes', httpOptions);                    
    }

    createSolicitudPautas(solicitudPautas, token) {
        console.log(solicitudPautas);
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        let body = JSON.stringify(solicitudPautas);
        return this.http.post(this.URL + '/solicitud', body, httpOptions);
    }

    updateSolicitudPauta(solicitudPautas, token) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        let body = JSON.stringify(solicitudPautas);
        return this.http.put(this.URL + '/solicitud/' + solicitudPautas.id, body, httpOptions);
    }

    deleteSolicitudPauta(solicitudPautas, token) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.delete(this.URL + '/solicitud/' + solicitudPautas, httpOptions);
    }
     // Devuelve una solicitudPautas específica
    // @params: id
    getSolicitudPauta(id: string, token: string): Observable<SolicitudPauta> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        console.log(this.URL + '/solicitud/' + id);
    const solicitudPautas = this.http.get<SolicitudPauta>(this.URL + '/solicitud/' + id, httpOptions);
    return solicitudPautas;
    }

    sendEmailSolicitud() {
      console.log(this.URL + '/solicitud/' + 'sendemailsolicitud');
      return this.http.get<any>(this.URL + '/solicitud/' + 'sendemailsolicitud', HttpClientHelper.httpOptions);                      
  }

    public getHeaders (token: string) : HttpHeaders {
        return new HttpHeaders({ 'Content-Type': 'application/json', 
        'Accept': 'application/json',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
        'Access-Control-Allow-Origin':'https://localhost:4200, https://www.tnoradio.com',
        'client-id':'1',
        'client_secret':'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh',
        'Authorization': 'Bearer ' + token
      })  
    }
}
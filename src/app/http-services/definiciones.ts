import { HttpHeaders } from "@angular/common/http";

export class HttpClientHelper{
    private static _baseURL: string = 'https://tnoradio.com/api/public/api/';
    private static _produccionURL: string = 'https://tnoradio.com/api/public/api/';
    private us_email: string;
    private us_contrasena;
    static client_id:'1';
    static client_secret:'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh';

    static httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                   'Accept': 'application/json',
                                   'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
                                   "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
                                   'Access-Control-Allow-Origin':'https://www.tnoradio.com/, https://localhost:4200/, https://tnoradio.com/',
                                   'client-id':'1',
                                   'client_secret':'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh',
                                 })   
    };

 

    public static get produccionURL(): string {
        return HttpClientHelper._produccionURL;
    }
    public static set produccionURL(value: string) {
        HttpClientHelper._produccionURL = value;
    }
    
    static get baseURL(): string {
        return HttpClientHelper._baseURL;
    }
    static set baseURL(value: string) {
        HttpClientHelper._baseURL = value;
    }

    public static get postData(): string {
        return HttpClientHelper.postData;
    }

    /* data to get Passport authentication */
    setUserData (email: string, contrasena: string){          
            this.us_email = email; // an User in Laravel database
            this.us_contrasena = contrasena; // the user's password
    }

}
import {Injectable} from "@angular/core";
import { Router } from '@angular/router';
import { Sesion } from "../modelos/modelo-sesion";
import { User } from "../modelos/modelo-user";
import { Turno } from "../modelos/modelo-turno";
import { Programa } from "app/modelos/modelo-programa";

@Injectable()
export class StorageService {
  private localStorageService: any;
  private currentSession : Sesion = null;
  turno: Turno;

  constructor(private router: Router) {
    console.log("storageService constructor()");
    this.localStorageService = localStorage;
    this.currentSession = this.loadSessionData();
  }
  setCurrentSession(sesion: Sesion): void {
    console.log("---setCurrentSession---");
    this.currentSession = sesion;
    //console.log("--session--");
    //console.log(sesion);
    this.localStorageService.setItem('currentUser', JSON.stringify(sesion));
    this.localStorageService.setItem('token',sesion.access_token);
  }
  loadSessionData(): Sesion{
    console.log("--loadSessionData---");
    var sessionStr = this.localStorageService.getItem('currentUser');
    return (sessionStr) ? <Sesion> JSON.parse(sessionStr) : null;
  }
  getCurrentSession(): Sesion {
    console.log("---get Current Session---");
    var sessionStr = this.localStorageService.getItem('currentUser');
    return JSON.parse(sessionStr);
  }
  removeCurrentSession(): void {
    console.info("---removeCurrentSession---");
    this.localStorageService.removeItem('currentUser');
    this.currentSession = null;
    this.localStorageService.setItem('currentUser', null);
    this.localStorageService.removeItem('token');
  }
  getCurrentUser(): User {
    console.log("---getCurrentUser---");
    var sesion: Sesion = this.getCurrentSession();
    return (sesion && sesion.user) ? sesion.user : null;
  };
  isAuthenticated(): boolean {
    console.log("--isAuthenticated---");
    return (this.getCurrentToken() != null) ? true : false;
  };
  getCurrentToken(): string {
    console.log("---getCurrentToken---");
    var sesion = this.getCurrentSession();
    return (sesion && sesion.access_token) ? sesion.access_token : null;
  };
  logout(): void {
    console.log("--logout()--");
    this.removeCurrentSession();
    this.localStorageService.setItem('currentUser', null);
    this.localStorageService.setItem('currentTurno', null);
    this.router.navigate(['/login']);
  }

  setCurrentTurno(turno: Turno){
    console.log("---setCurrentTurno()---");
    this.localStorageService.setItem('currentTurno', JSON.stringify(turno));
  }

  getCurrentTurno(user: string): Turno {
    console.log("---getCurrentTurno()---");  
    if(this.localStorageService.getItem('currentTurno')!=null) {
      this.turno= <Turno>JSON.parse(this.localStorageService.getItem('currentTurno'));
      if (this.turno!=null && this.turno.empleado_id.toString()==user){
        console.log("turno pertenece al usuario");
        return JSON.parse(this.localStorageService.getItem('currentTurno'));
      }
      else
        return null;      
    }
    else
      return null;
  }

  setWeekDayProgramsList(programas: Programa[]) {
    console.log("---setWeekDayProgramsList---");
    this.localStorageService.setItem('programasDia', JSON.stringify(programas));
  }

  getWeekDayProgramsList(): Programa[] {
    console.log("---getWeekDayProgramsList()---");
    console.log(this.localStorageService.getItem('programasDia'));
    return JSON.parse(this.localStorageService.getItem('programasDia'));
  }

 }
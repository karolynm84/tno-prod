import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpClientHelper } from './definiciones';
import { Observable } from 'rxjs';
import { Pago } from '../modelos/modelo-pago';
//import {Observable} from 'rxjs/Observable';
 

@Injectable()
export class PagoService {
    pagos: Pago[];

    private readonly URL = HttpClientHelper.produccionURL + "pagos";

    constructor(
        private http:HttpClient
    ) {}
 
    // Uses http.get() to load data from a single API endpoint
    getPagos(token):  Observable<Pago[]> {
       const httpOptions = {
          headers:  this.getHeaders(token)
        };
        return this.http.get<Pago[]>(this.URL, httpOptions);                    
    }

    sendEmailPago() {       
        console.log(this.URL+'sendemailpago');
        return this.http.get<any>(this.URL+'/sendemailpago', HttpClientHelper.httpOptions);                      
    }

    createPago(pago, token) {
        console.log(pago);
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        let body = JSON.stringify(pago);
        return this.http.post(this.URL, body, httpOptions);
    }

    updatePago(pago, token) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        let body = JSON.stringify(pago);
        return this.http.put(this.URL + '/' +pago.id, body, httpOptions);
    }

    deletePago(pago, token) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.delete(this.URL + '/' +pago, httpOptions);
    }
     // Devuelve un pago específico
    // @params: id
    getPago(id: string, token: string): Observable<Pago> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
    const pago = this.http.get<Pago>(this.URL + '/' + id, httpOptions);
    return pago;
    }

    public getHeaders (token: string) : HttpHeaders {
        return new HttpHeaders({ 'Content-Type': 'application/json', 
        'Accept': 'application/json',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
        'Access-Control-Allow-Origin':'https://localhost:4200, https://www.tnoradio.com',
        'client-id':'1',
        'client_secret':'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh',
        'Authorization': 'Bearer ' + token
      })  
    }
}
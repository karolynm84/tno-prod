import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpClientHelper } from './definiciones';
import { Observable } from 'rxjs';
import { CortePrograma } from '../modelos/modelo-corte-programa';
 
@Injectable()
export class CorteProgramaService {
    cortes: CortePrograma[];

    private readonly URL = HttpClientHelper.produccionURL;

    constructor(
        private http:HttpClient
    ) {}
 
    // Uses http.get() to load data from a single API endpoint
    getCortes():  Observable<CortePrograma[]> {
        return this.http.get<CortePrograma[]>(this.URL + "cortes", HttpClientHelper.httpOptions);                    
    }

    createUser(corte) {
        console.log(corte);
        let body = JSON.stringify(corte);
        return this.http.post(this.URL + 'corte', body, HttpClientHelper.httpOptions);
    }

    updateCortePrograma(corte) {
        let body = JSON.stringify(corte);
        return this.http.put('/api/cortes/' + corte.id, body, HttpClientHelper.httpOptions);
    }

    deleteCortePrograma(corte) {
        return this.http.delete('/api/cortes/' + corte.id);
    }
}
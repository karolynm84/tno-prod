import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpClientHelper } from './definiciones';
import { Observable } from 'rxjs';
import { Programa } from '../modelos/modelo-programa';
//import {Observable} from 'rxjs/Observable';
 
@Injectable()
export class ProgramaService {
    programas: Programa[];

    private readonly URL = HttpClientHelper.produccionURL + "programas";

    constructor(
        private http:HttpClient
    ) {}
 
    // Uses http.get() to load data from a single API endpoint
    getProgramas():  Observable<Programa[]> {
        return this.http.get<Programa[]>(this.URL, HttpClientHelper.httpOptions);                    
    }

    getProgramasAutorizados(userId, token):  Observable<Programa[]> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.get<Programa[]>(this.URL + '/' + userId, httpOptions);                    
    }

    sendEmailAprobación(token) {
        //const httpOptions = {
          //  headers:  this.getHeaders(token)
        //};
        console.log(this.URL+'sendemailaprobacion');
        return this.http.get<any>(this.URL+'sendemailaprobacion', HttpClientHelper.httpOptions);                      
    }

    createPrograma(programa, token) {
        console.log(programa);
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        let body = JSON.stringify(programa);
        return this.http.post(this.URL, body, httpOptions);
    }

    updatePrograma(programa, token) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        let body = JSON.stringify(programa);
        return this.http.put(this.URL + '/'+ programa.id, body, httpOptions);
    }

    deletePrograma(programa, token) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.delete(this.URL + '/' +programa, httpOptions);
    }
     // Devuelve un programa específico
    // @params: id
    getPrograma(id: string, token: string): Observable<Programa> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
    const programa = this.http.get<Programa>(this.URL +'/'+ id, httpOptions);
    return programa;
    }

    public getHeaders (token: string) : HttpHeaders {
        return new HttpHeaders({ 'Content-Type': 'application/json', 
        'Accept': 'application/json',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
        'Access-Control-Allow-Origin':'https://localhost:4200/, https://www.tnoradio.com/',
        'client-id':'1',
        'client_secret':'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh',
        'Authorization': 'Bearer ' + token
      })  
    }
}
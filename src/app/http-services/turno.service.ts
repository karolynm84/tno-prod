import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpClientHelper } from './definiciones';
import { Observable } from 'rxjs';
import { Turno } from '../modelos/modelo-turno';
//import {Observable} from 'rxjs/Observable';
 
@Injectable()
export class TurnoService {
    turnos: Turno[];

    private readonly URL = HttpClientHelper.produccionURL;

    constructor(
        private http:HttpClient
    ) {}

     // Devuelve una solicitudPautas específica
    // @params: id
    getTurno(id: string, token: string): Observable<Turno> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        const turno = this.http.get<Turno>(this.URL + 'turno/' + id, httpOptions);
        return turno;
    }
    // Uses http.get() to load data from a single API endpoint
    getTurnos(token):  Observable<Turno[]> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.get<Turno[]>(this.URL + "turnos", httpOptions);                    
    }

    createTurno(turno, token) : Observable<Turno> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        let body = JSON.stringify(turno);
        return this.http.post<Turno>(this.URL + 'turno', body, httpOptions);
    }

    updateTurno(turno: Turno, token: string) :  Observable<Turno> {
        let body = JSON.stringify(turno);
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.put<Turno>(this.URL + 'turno/' + turno.id, body, httpOptions);
    }

    deleteTurno(turno, token) : Observable<Turno> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.delete<Turno>(this.URL + 'turno/' + turno, httpOptions);
    }

    getTurnosCompletos(token):  Observable<any[]> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.get<any[]>(this.URL+'turnos/completos', httpOptions);                    
    }

    public getHeaders (token: string) : HttpHeaders {
        return new HttpHeaders({ 'Content-Type': 'application/json', 
        'Accept': 'application/json',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
        'Access-Control-Allow-Origin':'https://localhost:4200, https://www.tnoradio.com',
        'client-id':'1',
        'client_secret':'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh',
        'Authorization': 'Bearer ' + token
      })  
    }
}
import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpClientHelper } from './definiciones';
import { Observable } from 'rxjs';
import { SolicitudEntrevista } from '../modelos/modelo-solicitud-entrevista';

@Injectable()
export class SolicitudEntrevistaService {

    solicitudesentrevistas: SolicitudEntrevista[];

    private readonly URL = HttpClientHelper.produccionURL + "entrevistas";

    constructor(
        private http:HttpClient
    ) {}
 
    // Uses http.get() to load data from a single API endpoint
    getSolicitudesEntrevista(token):  Observable<SolicitudEntrevista[]> {
      const httpOptions = {
        headers:  this.getHeaders(token)
      };
      return this.http.get<SolicitudEntrevista[]>(this.URL+'/solicitudes', httpOptions);                    
    }

    createSolicitudEntrevista(solicitudEntrevista, token) {
        console.log(solicitudEntrevista);
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        let body = JSON.stringify(solicitudEntrevista);
        return this.http.post(this.URL + '/solicitud', body, httpOptions);
    }

    updateSolicitudEntrevista(solicitudEntrevista, token) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        let body = JSON.stringify(solicitudEntrevista);
        return this.http.put(this.URL + '/solicitud/' + solicitudEntrevista.id, body, httpOptions);
    }

    deleteSolicitudEntrevista(solicitudEntrevista, token) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.delete(this.URL + '/solicitud/' + solicitudEntrevista, httpOptions);
    }
     // Devuelve una solicitudEntrevista específica
    // @params: id
    getSolicitudEntrevista(id: string, token: string): Observable<SolicitudEntrevista> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        console.log(this.URL + '/solicitud/' + id);
    const solicitudEntrevista = this.http.get<SolicitudEntrevista>(this.URL + 'solicitud/' + id, httpOptions);
    return solicitudEntrevista;
    }

    sendEmailSolicitud() {
      console.log(this.URL + '/solicitud/' + 'sendemailsolicitud');
      return this.http.get<any>(this.URL + '/solicitud/' + 'sendemailsolicitud', HttpClientHelper.httpOptions);                      
  }

    public getHeaders (token: string) : HttpHeaders {
        return new HttpHeaders({ 'Content-Type': 'application/json', 
        'Accept': 'application/json',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
        'Access-Control-Allow-Origin':'https://localhost:4200, https://www.tnoradio.com',
        'client-id':'1',
        'client_secret':'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh',
        'Authorization': 'Bearer ' + token
      })  
    }
}
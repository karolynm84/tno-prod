import {Injectable} from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";
import { LoginObject } from "../modelos/modelo-login-object";
import { Sesion } from "../modelos/modelo-sesion";
import { HttpClientHelper } from "./definiciones";

@Injectable()
export class AuthenticationService {
    private URL = HttpClientHelper.produccionURL;
    private readonly httpOptions = HttpClientHelper.httpOptions;
    private accessToken = []; // variable to store the access token

    constructor(private http: HttpClient) {}

    login (loginObj: LoginObject): Observable<Sesion> {
        console.info("---login---");
        const httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                       'Accept': 'application/json',
                                       'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
                                       "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
                                       'Access-Control-Allow-Origin':'https://localhost:4200',
                                       'client-id':'1',
                                       'client_secret':'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh'
                                     })   
        };
        let body = JSON.stringify(loginObj);
        console.log(body);
        return this.http.post<Sesion>(this.URL + 'login', body, httpOptions);
    }

    logout(token:string): Observable<Boolean> {
        const httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                       'Accept': 'application/json',
                                       'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
                                       "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
                                       'Access-Control-Allow-Origin':'https://localhost:4200, https://www.tnoradio.com',
                                       'client-id':'1',
                                       'client_secret':'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh',
                                       'Authorization': 'Bearer ' +token
                                     })   
        };
        
        return this.http.get<Boolean>(this.URL + 'logout', httpOptions);
    }
    private extractData(res: Response) {
        let body = res.json();
        return body;
    }

    getToken() {
        return this.http.post(this.URL, this.httpOptions);                        
    }
    
    setToken(token) {
    this.accessToken = token;  // save the access_token
    }

    public getHeaders (token: string) : HttpHeaders {
        return new HttpHeaders({ 'Content-Type': 'application/json', 
        'Accept': 'application/json',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
        'Access-Control-Allow-Origin':'https://localhost:4200, https://www.tnoradio.com',
        'client-id':'1',
        'client_secret':'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh',
        'Authorization': 'Bearer ' + token
      })  
    }
}
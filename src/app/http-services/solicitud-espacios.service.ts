import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpClientHelper } from './definiciones';
import { Observable } from 'rxjs';
import { SolicitudEspacio } from '../modelos/modelo-solicitud-espacio';

@Injectable()
export class SolicitudEspacioService {

    solicitudesespacios: SolicitudEspacio[];

    private readonly URL = HttpClientHelper.produccionURL + "espacios";

    constructor(
        private http:HttpClient
    ) {}
 
    // Uses http.get() to load data from a single API endpoint
    getSolicitudesEspacios(token):  Observable<SolicitudEspacio[]> {
      const httpOptions = {
        headers:  this.getHeaders(token)
      };
      return this.http.get<SolicitudEspacio[]>(this.URL+'/solicitudes', httpOptions);                    
    }

    createSolicitudEspacios(solicitudEspacios, token) {
        console.log(solicitudEspacios);
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        let body = JSON.stringify(solicitudEspacios);
        return this.http.post(this.URL + '/solicitud', body, httpOptions);
    }

    updateSolicitudEspacio(solicitudEspacios, token) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        let body = JSON.stringify(solicitudEspacios);
        return this.http.put(this.URL + '/solicitud/' + solicitudEspacios.id, body, httpOptions);
    }

    deleteSolicitudEspacio(solicitudEspacios, token) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.delete(this.URL + '/solicitud/' + solicitudEspacios, httpOptions);
    }
     // Devuelve una solicitudEspacios específica
    // @params: id
    getSolicitudEspacio(id: string, token: string): Observable<SolicitudEspacio> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        console.log(this.URL + '/solicitud/' + id);
    const solicitudEspacios = this.http.get<SolicitudEspacio>(this.URL + '/solicitud/' + id, httpOptions);
    return solicitudEspacios;
    }

    sendEmailSolicitud() {
      console.log(this.URL + '/solicitud/' + 'sendemailsolicitud');
      return this.http.get<any>(this.URL + '/solicitud/' + 'sendemailsolicitud', HttpClientHelper.httpOptions);                      
  }

    public getHeaders (token: string) : HttpHeaders {
        return new HttpHeaders({ 'Content-Type': 'application/json', 
        'Accept': 'application/json',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
        'Access-Control-Allow-Origin':'https://localhost:4200, https://www.tnoradio.com',
        'client-id':'1',
        'client_secret':'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh',
        'Authorization': 'Bearer ' + token
      })  
    }
}
import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpClientHelper } from './definiciones';
import { Observable } from 'rxjs';
import { SolicitudPatrocinio } from '../modelos/modelo-solicitud-patrocinio';

@Injectable()
export class SolicitudPatrocinioService {

    solicitudespatrocinios: SolicitudPatrocinio[];

    private readonly URL = HttpClientHelper.produccionURL + "patrocinios";

    constructor(
        private http:HttpClient
    ) {}
 
    // Uses http.get() to load data from a single API endpoint
    getSolicitudesPatrocinios(token):  Observable<SolicitudPatrocinio[]> {
      const httpOptions = {
        headers:  this.getHeaders(token)
      };
      return this.http.get<SolicitudPatrocinio[]>(this.URL+'/solicitudes', httpOptions);                    
    }

    createSolicitudPatrocinios(solicitudPatrocinios, token) {
        console.log(solicitudPatrocinios);
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        let body = JSON.stringify(solicitudPatrocinios);
        return this.http.post(this.URL + '/solicitud', body, httpOptions);
    }

    updateSolicitudPatrocinio(solicitudPatrocinios, token) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        let body = JSON.stringify(solicitudPatrocinios);
        return this.http.put(this.URL + '/solicitud/' + solicitudPatrocinios.id, body, httpOptions);
    }

    deleteSolicitudPatrocinio(solicitudPatrocinios, token) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.delete(this.URL + '/solicitud/' + solicitudPatrocinios, httpOptions);
    }
     // Devuelve una solicitudPatrocinios específica
    // @params: id
    getSolicitudPatrocinio(id: string, token: string): Observable<SolicitudPatrocinio> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        console.log(this.URL + '/solicitud/' + id);
    const solicitudPatrocinios = this.http.get<SolicitudPatrocinio>(this.URL + '/solicitud/' + id, httpOptions);
    return solicitudPatrocinios;
    }

    sendEmailSolicitud() {
      console.log(this.URL + '/solicitud/' + 'sendemailsolicitud');
      return this.http.get<any>(this.URL + '/solicitud/' + 'sendemailsolicitud', HttpClientHelper.httpOptions);                      
  }

    public getHeaders (token: string) : HttpHeaders {
        return new HttpHeaders({ 'Content-Type': 'application/json', 
        'Accept': 'application/json',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
        'Access-Control-Allow-Origin':'https://localhost:4200, https://www.tnoradio.com',
        'client-id':'1',
        'client_secret':'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh',
        'Authorization': 'Bearer ' + token
      })  
    }
}
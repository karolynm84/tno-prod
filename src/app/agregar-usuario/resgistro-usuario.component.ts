import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ProgramaService, StorageService, AuthenticationService } from '../core';
import { UserService } from '../http-services/user.service';
import { RoleService } from '../http-services/role.service';
import { FormsValidationService } from '../shared-components/forms-validation.service';
import { ErrorService } from '../shared-services/error.service';
import { formUser } from '../forms/form-user';
import { formRegistroUsuario } from '../forms/form-registro-usuario';
import { Role } from '../modelos/modelo-rol';
import { Programa } from '../modelos/modelo-programa';
import { Sesion } from '../modelos/modelo-sesion';
import { LoginObject } from '../modelos/modelo-login-object';
import { DataSharingService } from '../shared-services/data-sharing.service';
import { DatePipe } from '@angular/common';
import { SideBarMenuComponent } from '../side-bar-menu/side-bar-menu.component';

interface Send {
  email: string;
  password:string;
}

@Component({
  selector: 'app-resgistro-usuario',
  templateUrl: './resgistro-usuario.component.html',
  styleUrls: ['./resgistro-usuario.component.css','../app.component.scss'],
  providers: [UserService, ProgramaService, 
              RoleService, FormsValidationService, 
              StorageService, AuthenticationService, 
              ErrorService, SideBarMenuComponent]
})

export class ResgistroUsuarioComponent implements OnInit {
    form_registro_usuario: FormGroup;
    roles: Role[];
    programas: Programa[];
    data: Sesion = new Sesion(); 
    loading = false;   
    send: Send;
    date: Date = new Date(); 
    pipe: DatePipe = new DatePipe('en-US'); // Use your own locale
    path: string;
    loginObject: LoginObject 

  constructor(
    public router:Router,
    public formBuilder: FormBuilder,
    public validation: FormsValidationService,
    public userService: UserService,
    public roleService: RoleService,     
    public programaService: ProgramaService,
    public toastr: ToastrService,
    private storageService: StorageService,
    private authenticationService: AuthenticationService,
    private dataSharingService: DataSharingService,
    private errorService: ErrorService,
    private sideBar: SideBarMenuComponent
) {}

  ngOnInit() {
    this.sideBar.setTitle('Registro de Usuario');
    this.form_registro_usuario = this.formBuilder.group({
        name: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator, this.validation.justLettersValidator, this.validation.tooManyWordsValidator])],
        lastname: ['', Validators.compose([Validators.required, Validators.maxLength(60), this.validation.notNullValidator])],
        email: ['', Validators.compose([Validators.required, this.validation.emailValidator,Validators.maxLength(40)])],
        instagram: ['', Validators.compose([Validators.maxLength(60)])],
        twitter:  ['', Validators.compose([Validators.maxLength(60)])],
        facebook:  ['', Validators.compose([Validators.maxLength(60)])],
        birthdate:['', Validators.compose([Validators.required, this.validation.isValidBirthDate])],
        password:['5555'],
        password_confirmation: ['5555'],
        role: ['Audiencia'],
        enabled: [true]
    });
    console.log(this.form_registro_usuario.valid);
    this.path = this.router.url;
    console.log(this.path);  // to print only path eg:"/login"
  }

  /**
   * When user press submit button.
   */
  onSubmit({ value, valid }: { value: formRegistroUsuario, valid: boolean }) { 
    var pass = this.generatePass();
    value.password = pass;
    value.password_confirmation = pass;
    value.role = 'Audiencia';
    value.enabled = true;

    //value.birthdate = this.pipe.transform(value.birthdate,'YYYY-MM-DD');
    console.log(value);

    this.loginObject = new LoginObject(value);

    if(!value.password || value.password === '') {
        delete value.password;
        delete value.password_confirmation;
    }
    if( (value.password || '') !== (value.password_confirmation || '')) {
        this.toastr.warning('Las contraseñas no coinciden.');
        //return;
    }
    if(!this.form_registro_usuario.valid){
        this.toastr.error('Debe llenar todos los campos obligatorios!', 'Error!');
    } else {
      this.loading = true;            
      this.userService
      .createUser(value)
      .subscribe(
        (res) => {
          this.loading = false;  
          console.log(res);
          this.userService.sendEmailBienvenida(this.loginObject).subscribe(
            (res) => {
              console.log(res);
            },
            (err) => {
              console.log(err);
            }
          );

          if (this.storageService.getCurrentUser()!=null) {
            console.log("Logeado");
            //if (this.storageService.getCurrentUser().role=='Administrador') {
              this.toastr.success('Usuario creado con éxito!');
              this.router.navigate([{outlets: {homesesion: 'usuarios'}}]);
           // }
          }else {
            console.log("No Logeado");
            this.toastr.success('Usuario creado con éxito!.');
            this.toastr.warning('Sus credenciales han sido enviadas al correo registrado.');
            this.storageService.removeCurrentSession();
            this.login(value);     
          }                              
        },error=>{
          this.loading = false;         
          console.log(error);    
          //var emailError = this.errors.emailTakenError(error);        
          //if (emailError == 0) {                   
            //  this.toastr.info('Something went wrong.');              
          //}          
        }
      );          
    } 
  }

   
  /**
   * Loads Roles into a SelectList.
   */
  public loadRoles() {
    console.info("---loadRoles()---");
    this.roleService.getRoles().subscribe
      (roles => { 
        this.roles = roles;
      });
  }

    public login (value: formRegistroUsuario) {
      this.loading = true;
      this.authenticationService
      .login(new LoginObject(value))
      .subscribe( 
        data => {     
         this.loading=false;  
         console.log(data);  
         this.correctLogin(data);
        },
        error => {
          this.loading = false;
          console.error("Error");
          this.toastr.error("Usuario o clave inválido.");
          this.storageService.removeCurrentSession();
          this.router.navigate(['/login']);
        }
      )
    }

    public correctLogin(data: Sesion){
      console.log("---correctLogin---");    
      this.storageService.setCurrentSession(data);   
      this.dataSharingService.isUserLoggedIn.next(true);
      this.router.navigate(['/sesion']);
    }

    public generatePass() : string {
      var randomstring = Math.random().toString(36).slice(-8);
      console.log(randomstring);
      return randomstring;
    }
    public volver() : void {
      this.router.navigate(['sesion', {outlets: {homesesion: ['usuarios']}}]);
    }
}


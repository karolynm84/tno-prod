import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { StorageService } from '../http-services/storage.service';

@Injectable()
export class AuthorizatedGuard implements CanActivate {
    
  constructor(private router: Router,
              private storageService: StorageService) { }
              
  canActivate() {
    console.log("---canActivate---");
    if (this.storageService.isAuthenticated()) {
      // logged in so return true
      console.log("logged");
      return true;
    }
    // not logged in so redirect to login page
    this.router.navigate(['/login']);
    console.log("not logged");
    return false;
  }
}
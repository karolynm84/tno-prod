import { Injectable, Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Programa } from 'app/modelos/modelo-programa';
import { StorageService, ProgramaService } from 'app/core';
import { ToastrService } from 'ngx-toastr';


@Injectable({
    providedIn: 'root'
})
export class DataSharingService {
    
    public isUserLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    private programas = new BehaviorSubject<any>(null);
    public programas$ = this.programas.asObservable();


    constructor(
        public storageService: StorageService,
        public servicioPrograma: ProgramaService,
        public toastr: ToastrService
    ) {}

    // here we set/change value of the observable
    setData(programas: Programa[]) { 
        console.log("DataSharingService setData");
        this.storageService.setWeekDayProgramsList(programas);
        this.programas.next(programas);
        
    }

/**
 * Gets list of programas from data base.
 */
getProgramas() {
    console.info("--DataSharingService getProgramas()---");
    return this.servicioPrograma.getProgramas();
                    
     
 
  }
}

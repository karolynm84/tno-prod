import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class ErrorService {

  constructor(
    public toastr: ToastrService) {
   }

   emailTakenError(error: any) : any {        
        var json = JSON.parse(error._body);  
        console.log(json);              
        if (json.errors.email == "Este email ya ha sido registrado en el sistema.") {
            this.toastr.error('Este email ya ha sido registrado en el sistema.');
            return 1;
        }
        else {
            return 0;
        }
    }   
}